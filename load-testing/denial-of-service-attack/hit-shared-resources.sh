#!/bin/bash

playbackURL="$1"
keepRunningMarker="$2"

cd ${MATTERHORN_HOME}/shared-resources

while [[ true ]]; do
    echo "Process $$ is re-running $0"
    for relativePath in $(find * -type f -print)
    do
        if [ ! -f ${keepRunningMarker} ]; then
            exit 0;
        fi
        wget -qO- ${playbackURL}/${relativePath} &> /dev/null
    done
done

exit 0;
