#!/bin/bash

##
# Bombard Matterhorn with HTTP requests and other traffic which will overwhelm its
# HTTP request handling and internal I/O.
##
if [ ! ${QA_HOME} ] ; then
    echo; echo "You must set env var QA_HOME=/opt/mh/ucb/qa (i.e., location of git clone)"
    echo; exit 1;
fi

if [ ! ${MATTERHORN_HOME} ] || [ ! -d "${MATTERHORN_HOME}" ] ; then
    echo; echo "You must git clone Matterhorn code and set env variable MATTERHORN_HOME to directory of checkout"
    echo; exit 1;
fi

keepRunningMarker="${QA_HOME}/load-testing/denial-of-service-attack/keep-running-marker"

if [ ! -f "${keepRunningMarker}" ] ; then
    echo; echo "[ERROR] You must create marker file to run this script:"
    echo; echo "    ${keepRunningMarker}"
    echo; echo "Next, start script. Terminate the script by removing the marker file."
    echo; exit 1;
fi

echo; echo "Remove the following file when you want this script to exit:"
echo "    ${keepRunningMarker}"

playbackURL="http://playback-qa.ets.berkeley.edu"

hit-shared-resources.sh "${playbackURL}" "${keepRunningMarker}" &
hit-shared-resources.sh "${playbackURL}" "${keepRunningMarker}" &

hit-playback-endpoints.sh "${playbackURL}" "${keepRunningMarker}" &
hit-playback-endpoints.sh "${playbackURL}" "${keepRunningMarker}" &

hit-404s-and-unauthorized.sh "${playbackURL}" "${keepRunningMarker}" &
hit-404s-and-unauthorized.sh "${playbackURL}" "${keepRunningMarker}" &

wait

echo "Done"; echo

exit 0
