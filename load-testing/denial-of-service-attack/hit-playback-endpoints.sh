#!/bin/bash

playbackURL="$1"
keepRunningMarker="$2"

restEndPoints=( "/info/me.json" "/search/paellaEpisodeListing.json?sid=930d206e-15ac-47d4-9e1e-b4728c6a4bf3&limit=100000&offset=0" \
    "/usertracking/actions.json" "/search/episode.json?id=b891a157-c4cc-4a5f-a7e3-1c5d5b8632e6" \
    "/search/episode.json?id=bfc5308c-dc76-4e00-8bfe-8514e70c2485" \
    "/search/episode.json?id=38507f2e-9d3a-4c0e-bd54-097e4eab6a2c" \
    "/search/episode.json?id=441e9e78-fbc8-47bc-b116-5132bb22e34f" )

feeds=( "/feeds/atom/1.0/latest" "/feeds/rss/2.0/series/48f07503-b598-452c-ae5a-090aba4d0460" )

miscellaneous=( "/login.html" "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/44b1fe2c-90f0-4107-a909-f443db4e9ec9/screen.mp4"
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/14290fd8-f92f-4fc2-9087-cc3614b61117/screen.jpg"
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/548bbdf8-f47c-4eb3-a568-6a8031358ea6/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/01ca2e32-8380-4bd2-9d26-902421afb00a/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/055d3559-0609-4656-b857-0b522dfae78c/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/05a2b0cc-e30d-4248-9376-3eda609ee54b/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/1712e590-6847-4017-ab08-448edaf62c61/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/190eb250-8e11-43b3-bcd8-1f405489d5cb/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/1ec48a68-13ef-4ee4-a912-04e5f1d2c75a/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/2a609573-7efa-4c53-8dfd-a97aa949808d/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/2e5e39aa-459e-4d2b-99d0-18ab516c391a/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/2fde98ca-84d8-46f2-9de7-dcf3386465e8/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/3173d4b5-7b3f-48b2-8b8f-6e7eaefaf869/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/3a83dfd2-3d40-4054-a67a-6a7d9f2f7e2a/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/3c128b82-0284-477f-babd-d4be8eb53816/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/3fd50bfb-8483-424e-a411-f8b97bb43e05/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/577ee04a-2e45-4787-b0a3-c92484257d20/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/64a365c4-2e3f-4469-a706-c69fea371089/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/7e37f7a0-7cd8-4e17-84bb-e0fc5d59ad09/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/8313a81c-d1fc-404f-8593-645b9443c43c/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/84793009-6d03-49f2-a546-07dd50228597/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/93af8cf8-e758-42be-9b17-947f63f3c86a/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/9b6adbdb-d428-4d9a-bbaa-b41eb9c8d8db/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/9e1d1f2b-ed1e-4d7b-abb2-3f4ad3ebb205/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/9ed2cab2-9c89-4b8e-83b7-e89cb655d39f/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/ac1faf5d-d6d4-4d87-a8b8-91f1ce2c829d/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/attachment-34/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/attachment-35/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/attachment-36/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/b4076349-ec3c-4c71-b689-e23e0ecd574b/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/baed4b73-3d73-4bb6-8f3a-a78e598eddb0/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/cbb4b971-d1d7-4b92-944e-bb2939ca0e90/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/da9a107a-7268-4001-a472-006dd66cccd5/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/e0fcd4eb-cfbd-492c-9d8f-8f98a5037d9d/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/e2ba5791-9b19-458e-a929-be40e268ce07/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/f83bf55e-6455-45c2-b7e6-381dfeffdeeb/screen.jpg" \
    "/static/engage-player/84fc19c7-d96a-41f3-b92f-e5793f751cec/security-policy/xacml.xml" )

arrayOfArrays=( "${restEndPoints[@]}" "${feeds[@]}" "${miscellaneous[@]}" )

while [[ true ]]; do
    echo "Process $$ is re-running $0"
    for arry in "${arrayOfArrays[@]}"
    do
        for relativePath in "${arry[@]}"
        do
            wget -qO- ${playbackURL}${relativePath} &> /dev/null
        done
    done
    if [ ! -f ${keepRunningMarker} ]; then
        exit 0;
    fi
done

exit 0;
