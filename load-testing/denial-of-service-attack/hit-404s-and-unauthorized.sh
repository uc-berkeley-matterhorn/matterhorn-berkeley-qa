#!/bin/bash

playbackURL="$1"
keepRunningMarker="$2"

notFoundURLs=( "/does/not/exist" "/css/doesNotExist.css" "/js/doesNotExist.css" )
unauthorizedURLs=( "/welcome.html" "/engage/ui/index.html" "/org/all.json" )

arrayOfArrays=( "${notFoundURLs[@]}" "${unauthorizedURLs[@]}" )

while [[ true ]]; do
    echo "Process $$ is re-running $0"
    for arry in "${arrayOfArrays[@]}"
    do
        for relativePath in "${arry[@]}"
        do
            wget -qO- ${playbackURL}${relativePath} &> /dev/null
        done
    done
    if [ ! -f ${keepRunningMarker} ]; then
        exit 0;
    fi
done

exit 0;
