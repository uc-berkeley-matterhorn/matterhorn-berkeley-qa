package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.matterhorn.SignUpPage;
import util.AuthenticationUtils;

/**
 * @author Paul Farestveit
 */

public class CalNetAuthPage extends Pages {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @FindBy(id = "username") WebElement username;

    @FindBy(id = "password") WebElement password;

    @FindBy(xpath = "//input[@value='Sign In']") WebElement signInButton;

    public CalNetAuthPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Authenticates user with specified CAS credentials
     *
     * @param driver   WebDriver instance
     * @param username CalNet username
     * @param password CalNet password
     * @return "logged in" version of same page
     */
    public CalNetAuthPage logIn(WebDriver driver, String username, String password) throws Exception {
        if (username.contains("secret")) {
            waitForTitleNotContains(driver, "Central Authentication Service", 120000);
        } else {
            clearAndType(driver, this.username, username);
            clearAndType(driver, this.password, password);
            signInButton.click();
        }
        Thread.sleep(1000);
        return this;
    }

    public CalNetAuthPage logInAdminTool(WebDriver driver) throws Exception {
        logger.info("Logging in admin using CalNet credentials");
        logIn(driver, AuthenticationUtils.getCalnetAdminUsername(), AuthenticationUtils.getCalnetAdminPassword());
        return this;
    }

    public SignUpPage logInSignUpForm(WebDriver driver, String username, String password) throws Exception {
        logger.info("Logging into CalNet, expect redirect to sign up form");
        logIn(driver, username, password);
        waitForPageLoad(driver).until(ExpectedConditions.titleIs("Course Capture (Webcast) | Educational Technology Services, " +
                "University of California Berkeley"));
        return new SignUpPage(driver);
    }

}
