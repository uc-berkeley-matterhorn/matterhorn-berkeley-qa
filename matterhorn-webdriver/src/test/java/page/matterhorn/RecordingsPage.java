package page.matterhorn;

import domain.Series;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.CalNetAuthPage;
import page.Pages;
import util.*;

import java.io.IOException;
import java.util.Date;

import static org.junit.Assert.fail;

/**
 * @author Paul Farestveit
 */
public class RecordingsPage extends Pages {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @FindBy(linkText = "Log out")
    public WebElement logoutLink;

    @FindBy(xpath = "//span[@class='searchbox-search-icon ui-icon ui-icon-search']/following-sibling::select")
    WebElement searchSelect;

    public RecordingsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public RecordingsPage loadPage(WebDriver driver) throws Exception {
        driver.get(AuthenticationUtils.getMatterhornAdminTool());
        return this;
    }

    public WebElement recordingTitle(WebDriver driver, int rowNumber) {
        return driver.findElements(By.xpath("//table[@id='recordingsTable']/tbody/tr/td[2]")).get(rowNumber - 1);
    }

    public WebElement recordingPresenter(WebDriver driver, int rowNumber) {
        return driver.findElements(By.xpath("//table[@id='recordingsTable']/tbody/tr/td[3]")).get(rowNumber - 1);
    }

    public WebElement recordingSeries(WebDriver driver, int rowNumber) {
        return driver.findElements(By.xpath("//table[@id='recordingsTable']/tbody/tr/td[4]")).get(rowNumber - 1);
    }

    public WebElement recordingCaptureAgent(WebDriver driver, int rowNumber) {
        return driver.findElements(By.xpath("//table[@id='recordingsTable']/tbody/tr/td[5]")).get(rowNumber - 1);
    }

    public WebElement recordingDateTime(WebDriver driver, int rowNumber) {
        return driver.findElements(By.xpath("//table[@id='recordingsTable']/tbody/tr/td[6]")).get(rowNumber - 1);
    }

    private String searchButtonXpath() { return "//span[@class='searchbox-search-icon ui-icon ui-icon-search']"; }

    private String bulkActionLinkText() { return "Bulk Action"; }

    private String bulkActionButtonId() { return "i18n_button_apply_bulk_action"; }

    private String upcomingTabXpath() { return "//span[contains(text(),'Upcoming')]"; }

    private String recordingStatusXpathOnRow(int rowNumber) { return "//table[@id='recordingsTable']/tbody/tr[" + String.valueOf(rowNumber) + "]/td[7]"; }

    private String archiveTabId() { return "i18n_tab_episode"; }

    private String scheduleRecordingsButtonId() { return "scheduleButton"; }

    private String uploadRecordingButtonId() { return "uploadButton"; }

    /**
     * Clicks the homepage link.  After trimming a recording, it's necessary to navigate away from the recordings page
     * and back again in order to redisplay the recordings table.
     *
     * @param driver the current browser
     * @return the homepage
     * @throws Exception
     */
    public HomePage clickHomepageLink(WebDriver driver) throws Exception {
        logger.info("Clicking homepage link");
        for (int attempts = 1; attempts <= 2; attempts++) {
            if (attempts > 2)
                fail("Gave up trying to click link to admin tools on homepage after " + attempts + " attempts");
            try {
                waitForPageLoad(driver).until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@title='Matterhorn Home Page']")));
                if (driver.findElement(By.xpath("//img[@title='Matterhorn Home Page']")).isDisplayed()) {
                    break;
                }
            } catch (Exception e) {
                continue;
            }
            Thread.sleep(1000);
        }
        waitForElementAndClick(driver, driver.findElement(By.xpath("//img[@title='Matterhorn Home Page']")));
        Thread.sleep(TimeoutUtils.getPageLoadWaitMs());
        return new HomePage(driver);
    }

    /**
     * Logs out of Matterhorn and CalNet
     *
     * @param driver the current browser
     * @return the CalNet page
     * @throws IOException
     */
    public CalNetAuthPage clickLogoutLink(WebDriver driver) throws IOException {
        logger.info("Logging out of Matterhorn");
        logoutLink.click();
        waitForPageLoad(driver).until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//h3[@class='login-message']"), "Logout Successful"));
        return new CalNetAuthPage(driver);
    }

    /**
     * Clicks the archives tab
     *
     * @param driver the current browser
     * @return the archives page
     * @throws IOException
     */
    public ArchivesPage clickArchivesTab(WebDriver driver) throws Exception {
        logger.info("Clicking archives tab");
        waitForElementAndClick(driver, driver.findElement(By.id(archiveTabId())));
        return new ArchivesPage(driver);
    }

    /**
     * Clicks the "Schedule Recording" button
     *
     * @param driver WebDriver instance
     * @return Schedule Recording page
     */
    public RecordingsSchedulerPage clickScheduleRecordingButton(WebDriver driver) throws Exception {
        logger.info("Clicking schedule recording button");
        waitForElementAndClick(driver, driver.findElement(By.id(scheduleRecordingsButtonId())));
        return new RecordingsSchedulerPage(driver);
    }

    /**
     * Clicks the "Upload Recording" button
     *
     * @param driver WebDriver instance
     * @return Recording Upload Page
     */
    public RecordingUploadPage clickUploadRecordingButton(WebDriver driver) throws Exception {
        logger.info("Clicking upload recording button");
        waitForElementAndClick(driver, driver.findElement(By.id(uploadRecordingButtonId())));
        return new RecordingUploadPage(driver);
    }

    /**
     * Clicks the edit link on a designated row number in the recordings table.
     *
     * @param driver    the current browser
     * @param rowNumber the row containing the episode to be edited
     * @return the recording edit page
     * @throws Exception
     */
    public RecordingEditPage clickEditLinkOnRow(WebDriver driver, int rowNumber) throws Exception {
        logger.info("Clicking edit link on row " + String.valueOf(rowNumber));
        String xpath = "//table[@id='recordingsTable']/tbody/tr[" + String.valueOf(rowNumber) + "]/td[8]/a[text()='Edit']";
        waitForElementXpathAndClick(driver, xpath);
        return new RecordingEditPage(driver);
    }

    /**
     * Clicks the review/trim link on a designated row number in the recordings table.
     *
     * @param driver    the current browser
     * @param rowNumber the row containing the episode to be trimmed
     * @return the review/trim page
     * @throws Exception
     */
    public ReviewTrimPage clickReviewTrimLinkOnRow(WebDriver driver, int rowNumber) throws Exception {
        logger.info("Clicking review-trim link on row " + String.valueOf(rowNumber));
        String xpath = "//table[@id='recordingsTable']/tbody/tr[" + String.valueOf(rowNumber) + "]/td[8]/a[text()='Review / Trim']";
        waitForElementXpathAndClick(driver, xpath);
        return new ReviewTrimPage(driver);
    }

    /**
     * Clicks the cancel publish delay link on a designated row number in the recordings table.  Also accepts the browser alert.
     *
     * @param driver    the current browser
     * @param rowNumber the row containing the episode to be published immediately
     * @return the same page
     * @throws Exception
     */
    public RecordingsPage clickCancelPublishDelayLinkOnRow(WebDriver driver, int rowNumber) throws Exception {
        logger.info("Clicking cancel publish delay link on row " + String.valueOf(rowNumber));
        String xpath = "//table[@id='recordingsTable']/tbody/tr[" + String.valueOf(rowNumber) + "]/td[8]/a[text()='Cancel Publish Delay']";
        waitForElementXpathAndClick(driver, xpath);
        Thread.sleep(3000);
        return this;
    }

    /**
     * Bulk deletes all upcoming recordings.  Used to clear out leftover test data from previous tests.
     *
     * @param driver the current browser
     * @return the same page
     * @throws Exception
     */
    public RecordingsPage deleteUpcomingRecordings(WebDriver driver) throws Exception {
        logger.info("Clicking Upcoming tab and checking for existing scheduled recordings");
        loadPage(driver);
        waitForElementAndClick(driver, driver.findElement(By.xpath(upcomingTabXpath())));
        Thread.sleep(2000);
        if (driver.findElement(By.xpath("//table[@id='recordingsTable']/tbody/tr/td")).getText().contains("No Recordings found")) {
            logger.info("No upcoming recordings, no need to delete anything");
        } else {
            logger.info("Found existing recordings, so deleting them");
            waitForElementAndClick(driver, driver.findElement(By.linkText(bulkActionLinkText())));
            waitForElementAndSelect(driver, driver.findElement(By.id("bulkActionSelect")), "Delete Recordings");
            Thread.sleep(1000);
            waitForElementAndClick(driver, driver.findElement(By.id("selectAllRecordings")));
            if (!driver.findElement(By.id("selectAllRecordings")).isSelected()) {
                driver.findElement(By.id("selectAllRecordings")).click();
            }
            waitForElementAndClick(driver, driver.findElement(By.id(bulkActionButtonId())));
            swallowAlert(driver);
            waitForPageUpdate(driver).until(ExpectedConditions.textToBePresentInElementLocated((By.xpath("//table[@id='recordingsTable']/tbody/tr/td")),
                    "No Recordings found"));
            logger.info("Waiting for " + TimeoutUtils.getCaptureSchedWaitSec() + " seconds so that capture agents are notified.");
            Thread.sleep(TimeoutUtils.getCaptureSchedWaitMs());
        }
        return this;
    }

    /**
     * Uses the unique test ID of a series to search for all recordings in the series.  The expected number of results
     * is the number of recordings in the series.  Because of frequent DOM updates that can cause stale element exceptions,
     * the method catches errors and continues.  It will retry the search five times before giving up and failing the test.
     *
     * @param driver               the current browser
     * @param series               the series to be searched
     * @param expectedResultsCount the number of episodes in he series
     * @return the same page
     * @throws Exception
     */
    public RecordingsPage searchForSeriesRecordings(WebDriver driver, Series series, int expectedResultsCount) throws Exception {
        for (int attempts = 1; attempts <= 5; attempts++) {
            if (attempts > 5) fail("Gave up searching after " + attempts + " attempts");
            try {
                clearAndType(driver, driver.findElement(By.xpath("//input[@class='searchbox-text-input ui-corner-all']")), series.getSeriesTestId());
                waitForElementAndSelect(driver, searchSelect, "Title");
                waitForElementAndClick(driver, driver.findElement(By.xpath(searchButtonXpath())));
                if (driver.findElement(By.id("filterRecordingCount")).getText().matches(String.valueOf(expectedResultsCount) + " found")) {
                    break;
                }
            } catch (Exception e) {
                logger.info("Error when searching");
                e.getMessage();
                continue;
            }
            Thread.sleep(TimeoutUtils.getWorkflowRetryIntervalMs());
        }
        return this;
    }

    public void waitForRecordingOnRow(WebDriver driver, String recordingTitle, int row, int wait) throws InterruptedException {
        logger.info("Waiting for \"" + recordingTitle + "\" to appear ");
        for (int second = 0; second <= wait; second++) {
            Thread.sleep(1000);
            if (second == wait)
                fail("Timed out after waiting for " + wait + " seconds.  The text I found was " + recordingTitle(driver, row).getText());
            try {
                if (recordingTitle(driver, row).getText().contains(recordingTitle)) {
                    logger.debug("Found! at " + DateFormatUtils.getHourMinute(new Date()));
                    break;
                }
            } catch (StaleElementReferenceException ignored) {
            }
        }
    }

    /**
     * Waits for a series episode on a specific row to achieve a designated workflow status.  For example, wait for 'On Hold' or 'Finished'.
     * Because of frequent DOM updates that can cause stale element exceptions, the method catches errors and continues.
     *
     * @param driver             the current browser
     * @param series             the series to be searched
     * @param recordingsTableRow the row that should contain the episode being processed by Matterhorn
     * @param status             the workflow status for which the test is waiting
     * @return the same page
     * @throws Exception
     */
    public RecordingsPage waitForRecordingStatusOnRow(WebDriver driver, Series series, int recordingsTableRow, String status) throws Exception {
        logger.info("Waiting for recording to reach '" + status + "'");
        int attempts = TimeoutUtils.getMaxWorkflowRetries();
        for (int i = 0; i <= attempts; i++) {
            if (i == attempts) fail("Timed out before the recording status was updated");
            loadPage(driver);
            try {
                searchForSeriesRecordings(driver, series, recordingsTableRow);
                waitForPageLoad(driver).until(ExpectedConditions.presenceOfElementLocated(By.id("recordingsTable")));
                if (driver.findElement(By.xpath(recordingStatusXpathOnRow(recordingsTableRow))).getText().contains(status)) {
                    logger.debug("Found! at " + DateFormatUtils.getHourMinute(new Date()));
                    break;
                } else if (driver.findElement(By.xpath(recordingStatusXpathOnRow(recordingsTableRow))).getText().contains("Failed")
                        ) {
                    logger.error("Workflow failed at " + DateFormatUtils.getHourMinute(new Date()));
                    fail();
                }
            } catch (StaleElementReferenceException ignored) {
            }
            Thread.sleep(TimeoutUtils.getWorkflowRetryIntervalMs());
        }
        return this;
    }

    /**
     * For use in sign-up form tests, verifies the metadata for the two recordings scheduled by the scripts
     *
     */
    public void verifyTwoScheduledRecordings(WebDriver driver, StringBuffer verificationErrors, Series series, Date[] recordingDates) throws Exception {
        // Recording Two
        waitForRecordingOnRow(driver, series.getSeriesName() + " - " + DateFormatUtils.getDateYYYY_MM_DD(recordingDates[1]), 1, TimeoutUtils.getPageLoadWaitSec());
        TestUtils.verifyEquals(verificationErrors, StringUtils.join(series.getInstructors(), ", "), recordingPresenter(driver, 1).getText());
        TestUtils.verifyEquals(verificationErrors, series.getSeriesName() + " - " + SalesforceUtils.getCurrentTerm() + " " + SalesforceUtils.getCurrentYear(),
                recordingSeries(driver, 1).getText());
        TestUtils.verifyEquals(verificationErrors, series.getCaptureAgent().getCaptureAgentName(), recordingCaptureAgent(driver, 1).getText());
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getDayAndDate(recordingDates[1]) + " - 18:37\nDuration: 00:26:00",
                recordingDateTime(driver, 1).getText());
        // Recording One
        TestUtils.verifyEquals(verificationErrors, series.getSeriesName() + " - " + DateFormatUtils.getDateYYYY_MM_DD(recordingDates[0]), recordingTitle(driver, 2).getText());
        TestUtils.verifyEquals(verificationErrors, StringUtils.join(series.getInstructors(), ", "), recordingPresenter(driver, 2).getText());
        TestUtils.verifyEquals(verificationErrors, series.getSeriesName() + " - " + SalesforceUtils.getCurrentTerm() + " " + SalesforceUtils.getCurrentYear(),
                recordingSeries(driver, 2).getText());
        TestUtils.verifyEquals(verificationErrors, series.getCaptureAgent().getCaptureAgentName(), recordingCaptureAgent(driver, 2).getText());
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getDayAndDate(recordingDates[0]) + " - 18:37\nDuration: 00:26:00",
                recordingDateTime(driver, 2).getText());

    }

}
