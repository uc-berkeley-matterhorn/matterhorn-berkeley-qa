package page.matterhorn;

import domain.Series;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.Pages;
import util.AuthenticationUtils;
import util.SalesforceUtils;
import util.TestUtils;
import util.TimeoutUtils;

/**
 * @author Paul Farestveit
 */

public class SeriesInfoPage extends Pages {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @FindBy(id = "id") public WebElement seriesId;

    @FindBy(id = "title") public WebElement seriesTitle;

    @FindBy(id = "contributor") public WebElement seriesContributor;

    @FindBy(id = "accessRights") public WebElement seriesAvailability;

    @FindBy(id = "description") public WebElement seriesDescription;

    public SeriesInfoPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Hits the series info page for a given series
     *
     * @param driver the current browser
     * @param series the series
     * @return the series info page
     * @throws Exception
     */
    public SeriesInfoPage loadPage(WebDriver driver, Series series) throws Exception {
        logger.info("Loading series info page");
        driver.get(AuthenticationUtils.getMatterhornAdminTool() + "/admin/index.html#/viewseries?seriesId=" + series.getSeriesId());
        waitForVisible(driver, driver.findElement(By.id("id")), TimeoutUtils.getPageLoadWaitSec());
        return this;
    }

    /**
     * Clicks the additional description link to reveal additional series info
     *
     * @param driver the current browser
     * @return the same page
     */
    public SeriesInfoPage clickAddlDesc(WebDriver driver) throws Exception {
        logger.info("Clicking additional info link");
        waitForElementAndClick(driver, driver.findElement(By.id("i18n_additional")));
        return this;
    }

    public void verifySeriesInfo(WebDriver driver, StringBuffer verificationErrors, Series series) throws Exception {
        loadPage(driver, series);
        waitForText(seriesId, series.getSeriesId(), TimeoutUtils.getPageLoadWaitSec());
        TestUtils.verifyEquals(verificationErrors, series.getSeriesName() + " - " + SalesforceUtils.getCurrentTerm() + " " + SalesforceUtils.getCurrentYear(),
                seriesTitle.getText());
        waitForText(seriesContributor, StringUtils.join(series.getInstructors(), ", "), TimeoutUtils.getAjaxWaitSec());
        clickAddlDesc(driver);
        TestUtils.verifyEquals(verificationErrors, series.getCourseTitle() + " - " + StringUtils.join(series.getInstructors(), ", "), seriesDescription.getText());

    }

}
