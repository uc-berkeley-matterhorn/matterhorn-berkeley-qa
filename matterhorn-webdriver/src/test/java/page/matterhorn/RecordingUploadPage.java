package page.matterhorn;

import domain.Recording;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.Pages;
import util.*;

/**
 * @author Paul Farestveit
 */

public class RecordingUploadPage extends Pages {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @FindBy(id = "heading-metadata") public WebElement uploadConfirmation;

    @FindBy(id = "field-title") public WebElement uploadedTitle;

    @FindBy(id = "field-creator") public WebElement uploadedPresenter;

    @FindBy(id = "field-license") public WebElement uploadedLicense;

    @FindBy(id = "field-filename1") public WebElement uploadedSegmentableFile;

    @FindBy(id = "field-filename2") public WebElement uploadedNonSegmentableFile;

    @FindBy(id = "field-filename3") public WebElement uploadedAudioFile;

    public RecordingUploadPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Clicks the homepage image
     *
     * @param driver WebDriver instance
     * @return homepage object
     */
    public HomePage clickHomepageLink(WebDriver driver) throws Exception {
        logger.info("Clicking homepage link");
        waitForElementAndClick(driver, driver.findElement(By.xpath("//img[@title='Matterhorn Home Page']")));
        return new HomePage(driver);
    }

    /**
     * Clicks the "back to recordings" link
     *
     * @param driver WebDriver instance
     * @return recordings page with list of recordings
     */
    public RecordingsPage clickBackToRecordingsLink(WebDriver driver) throws Exception {
        logger.info("Clicking the back-to-recordings link on the recording upload page");
        waitForElementAndClick(driver, driver.findElement(By.linkText("<< Back to Recordings")));
        return new RecordingsPage(driver);
    }

    /**
     * Inputs recording metadata in the primary (top) form
     *
     * @param driver WebDriver instance
     * @param recording existing recording object
     * @return same page, fields populated with designated input
     */
    public RecordingUploadPage inputRecordingMetadata(WebDriver driver, Recording recording, boolean seriesAlreadyExists) throws Exception {
        logger.info("Recording title is " + recording.getTitle());
        waitForElementPresent(driver.findElement(By.id("uploadForm")), TimeoutUtils.getPageLoadWaitSec());
        clearAndType(driver, driver.findElement(By.id("title")), recording.getTitle());
        // For some reason, the title field sometimes doesn't get populated, hence the repetition below
        clearAndType(driver, driver.findElement(By.id("title")), recording.getTitle());

        if (recording.getInstructors() != null) {
            logger.info("Recording presenter is " + StringUtils.join(recording.getInstructors(), ", "));
            clearAndType(driver, driver.findElement(By.id("creator")), StringUtils.join(recording.getInstructors(), ", "));
        }

        if (recording.getSeries() != null) {
            logger.info("Recording series is " + recording.getSeriesTitle());
            clearAndType(driver, driver.findElement(By.id("seriesSelect")), recording.getSeries().getSeriesTestId());
            if (seriesAlreadyExists) {
                waitForPageUpdate(driver).until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//ul[@class='ui-autocomplete ui-menu " +
                        "ui-widget ui-widget-content ui-corner-all']/li/a"), recording.getSeriesTitle()));
                driver.findElement(By.xpath("//ul[@class='ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all']/li/a")).click();
            }
        }

        logger.info("Recording license is " + recording.getLicenseDescription());
        waitForElementAndSelect(driver, driver.findElement(By.id("license")), recording.getLicenseDescription());

        logger.info("Recording date is " + DateFormatUtils.getDateYYYY_MM_DD(recording.getRecordingDate()) + ", " +
                DateFormatUtils.getHour(recording.getRecordingDate()) + ":" + DateFormatUtils.getMinute(recording.getRecordingDate()));
        clearAndType(driver, driver.findElement(By.id("recordDate")), DateFormatUtils.getDateYYYY_MM_DD(recording.getRecordingDate()));
        waitForElementAndClick(driver, driver.findElement(By.id("title")));
        waitForElementAndSelect(driver, driver.findElement(By.id("startTimeHour")), DateFormatUtils.getHour(recording.getRecordingDate()));
        waitForElementAndSelect(driver, driver.findElement(By.id("startTimeMin")), DateFormatUtils.getMinute(recording.getRecordingDate()));
        return this;
    }

    /**
     * Selects "single file" for upload, enters filepath for the upload
     *
     * @param driver WebDriver instance
     * @param filepath file path from file system (located in resources/sample-recordings)
     * @param containsSlides whether or not the single file contains slides (is a presentation)
     * @return same page, with file to be uploaded
     * @throws Exception
     */
    public RecordingUploadPage inputSingleFile(WebDriver driver, String filepath, boolean containsSlides) throws Exception {
        logger.info("Uploading a single file for the recording");
        waitForElementAndClick(driver, driver.findElement(By.id("singleUploadRadio")));
        switchToIframe(driver, driver.findElement(By.xpath("//div[@id='uploadContainerSingle']//li[2]/iframe")));
        waitForVisible(driver, driver.findElement(By.id("file")), TimeoutUtils.getAjaxWaitSec());
        logger.info("The file is " + filepath);
        FileUploadUtils.uploadFile(driver, driver.findElement(By.id("file")), filepath);
        if (containsSlides && !driver.findElement(By.id("containsSlides")).isSelected()) {
            driver.findElement(By.id("containsSlides")).click();
        }
        driver.switchTo().defaultContent();
        return this;
    }

    /**
     * Selects "multiple files" for upload, enters filepath for the uploads
     *
     * @param driver WebDriver instance
     * @param filepathSegmentable filename for the segmentable file in the file system (located in resources/sample-recordings)
     * @param filepathNonSegmentable filename for the non-segmentable file in the file system (located in resources/sample-recordings)
     * @param filepathAudio filename for the audio file in the file system (located in resources/sample-recordings)
     * @return same page, with files to be uploaded
     * @throws Exception
     */
    public RecordingUploadPage inputMultipleFiles(WebDriver driver, String filepathSegmentable, String filepathNonSegmentable,
                                                   String filepathAudio) throws Exception {
        logger.info("Uploading multiple files for the recording");
        waitForPageUpdate(driver).until(ExpectedConditions.elementToBeClickable(By.id("multiUploadRadio")));
        if (!driver.findElement(By.id("multiUploadRadio")).isSelected()) {
            driver.findElement(By.id("multiUploadRadio")).click();
        }
        waitForPageUpdate(driver).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='uploadContainerMulti']/fieldset[1]//li[2]/iframe")));
        if (filepathSegmentable != null) {
            logger.info("The segmentable file is " + filepathSegmentable);
            driver.switchTo().frame(driver.findElement(By.xpath("//div[@id='uploadContainerMulti']/fieldset[1]//li[2]/iframe")));
            waitForPageUpdate(driver).until(ExpectedConditions.visibilityOfElementLocated(By.id("file")));
            FileUploadUtils.uploadFile(driver, driver.findElement(By.id("file")), filepathSegmentable);
            driver.switchTo().defaultContent();
        }
        else {
            logger.info("There is no segmentable file");
        }
        if (filepathNonSegmentable != null) {
            logger.info("The non-segmentable file is " + filepathNonSegmentable);
            driver.switchTo().frame(driver.findElement(By.xpath("//div[@id='uploadContainerMulti']/fieldset[2]//li[2]/iframe")));
            waitForPageUpdate(driver).until(ExpectedConditions.visibilityOfElementLocated(By.id("file")));
            FileUploadUtils.uploadFile(driver, driver.findElement(By.id("file")), filepathNonSegmentable);
            driver.switchTo().defaultContent();
        }
        else {
            logger.info("There is no non-segmentable file");
        }
        if (filepathAudio !=null) {
            logger.info("The audio file is " + filepathAudio);
            driver.switchTo().frame(driver.findElement(By.xpath("//div[@id='uploadContainerMulti']/fieldset[3]//li[2]/iframe")));
            waitForPageUpdate(driver).until(ExpectedConditions.visibilityOfElementLocated(By.id("file")));
            FileUploadUtils.uploadFile(driver, driver.findElement(By.id("file")), filepathAudio);
            driver.switchTo().defaultContent();
        }
        else {
            logger.info("There is no audio file");
        }
        return this;
    }

    public RecordingUploadPage chooseReviewTrim(WebDriver driver) throws Exception {
        logger.info("Selecting review / trim");
        waitForElementAndClick(driver, driver.findElement(By.id("trimHold")));
        waitForPageUpdate(driver).until(ExpectedConditions.elementToBeSelected(By.id("trimHold")));
        return this;
    }

    /**
     * Clicks the "submit" button at the bottom of the page and waits for the upload confirmation.
     *
     * @param driver WebDriver instance
     * @return same page with upload confirmation
     */
    public RecordingUploadPage submitUpload(WebDriver driver) throws Exception {
        logger.info("Clicking the submit button");
        waitForElementAndClick(driver, driver.findElement(By.id("submitButton")));
        waitForFileUpload(driver).until(ExpectedConditions.visibilityOfElementLocated(By.id("heading-metadata")));
        waitForText(uploadConfirmation, "A Recording with the following information has been uploaded:",
                TimeoutUtils.getPageLoadWaitSec());
        return this;
    }

}
