package page.matterhorn;

import domain.Inputs;
import domain.PublishDelay;
import domain.Series;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import page.CalNetAuthPage;
import page.Pages;
import util.*;
import java.util.Date;

import java.io.IOException;

/**
 * @author Paul Farestveit
 */

public class SignUpPage extends Pages {

    final Logger logger = WebDriverUtils.getLogger(this.getClass());

    @FindBy(xpath = "//h1") public WebElement pageHeading;

    @FindBy(xpath = "//h2[@class='course-name']") public WebElement courseTitle;

    @FindBy(xpath = "//h3") public WebElement locationAndTime;

    @FindBy(id = "instructorList") public WebElement instructorListText;

    @FindBy(linkText = "Log out") public WebElement logOutLink;

    // Input selection controls

    @FindBy(id = "audioOnlyOption") public WebElement audioOnly;

    @FindBy(id = "screencastOption") public WebElement screencast;

    @FindBy(id = "videoOnlyOption") public WebElement videoOnly;

    @FindBy(id = "videoAndScreencastOption") public WebElement videoAndScreencast;

    // Availability controls

    @FindBy(name = "recordingAvailability") public WebElement recordingAvailability;

    // Publish delay controls

    @FindBy(id = "noPublishDelay") public WebElement noPublishDelayRadio;

    @FindBy(id = "publishDelay") public WebElement publishDelayRadio;

    @FindBy(id = "publishDelayDays") WebElement publishDelaySelect;

    // Agreement, sign up button

    @FindBy(id = "agreeToTerms") public WebElement agreementConfirmationCbx;

    @FindBy(id = "signUpButton") public WebElement signUpButton;

    // Messaging

    @FindBy(id = "globalErrors") public WebElement adminSignUpWarningMessage;

    @FindBy(xpath = "//p[@class='instructional-text']") public WebElement instructionalText;

    @FindBy(id = "instructionalText") public WebElement instructionalTextSignAgreeOnly;

    @FindBy(id = "recordingTypeDiv") public WebElement recordingTypeConfirmation;

    @FindBy(id = "recordingTimeDiv") public WebElement recordingTimeConfirmation;

    @FindBy(id = "availabilityDiv") public WebElement recordingAvailabilityConfirmation;

    @FindBy(id = "publishDelayDiv") public WebElement publishDelayConfirmation;

    public SignUpPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    // Informational text

    public String headingText = "Sign Up for Course Capture (Webcast)";

    public String confText = "The following settings have been selected for capture, and recordings have been scheduled. Please note that there will " +
            "be a delay of approximately one hour before other systems such as bCourses and CalCentral can be updated to reflect that your Course Capture " +
            "recordings have been scheduled.";
    public String reconfText = "You have already signed up with the following settings. Please note that there will be a delay of approximately one hour " +
            "before other systems such as bCourses and CalCentral can be updated to reflect that your Course Capture recordings have been scheduled.";
    public String authorizeText = "Recordings have been scheduled with the following preferences but we need you to agree to the terms described below. " +
            "Please note that there will be a delay of approximately one hour before other systems such as bCourses and CalCentral can be updated to reflect that your " +
            "Course Capture recordings have been scheduled.";

    public String coInstructorConfText = "You have submitted the preferences below. Recordings will be scheduled when all instructors in your course have signed up.";
    public String coInstructorWarnText = "Your co-instructor(s) signed up with the following settings. Change these only if you believe your co-instructor(s) will agree.";

    public String nonAuthText = "Sorry, you are not authorized to access this sign up form because you are not listed as an instructor of the course.";

    /**
     * Hits the sign-up page for a given series
     *
     * @param driver the current browser
     * @param series the series
     * @return the sign-up page
     * @throws IOException
     */
    public SignUpPage loadPage(WebDriver driver, Series series) throws IOException {
        logger.info("Loading sign up form at " + SalesforceUtils.getSignUpFormUrl(series));
        driver.get(SalesforceUtils.getSignUpFormUrl(series));
        waitForPageLoad(driver).until(ExpectedConditions.titleIs("Course Capture (Webcast) | Educational Technology Services, University of California Berkeley"));
        return new SignUpPage(driver);
    }

    public SignUpPage logIn(WebDriver driver, CalNetAuthPage calNetAuthPage, Series series, String username, String password) throws Exception
    {
        logger.info("Logging into sign up form at " + SalesforceUtils.getSignUpFormUrl(series));
        driver.get(SalesforceUtils.getSignUpFormUrl(series));
        calNetAuthPage.logInSignUpForm(driver, username, password);
        return this;
    }

    /**
     * Hits the sign-up page for a given series while the user is not authenticated in CalNet.  Expects that the CalNet
     * page will load instead of the sign-up page.
     *
     * @param driver the current browser
     * @param series the series
     * @return the CalNet page
     * @throws IOException
     */
    public CalNetAuthPage calNetRedirect(WebDriver driver, Series series) throws IOException {
        logger.info("Loading sign up form at " + SalesforceUtils.getSignUpFormUrl(series) + ", expecting CAS redirect");
        driver.get(SalesforceUtils.getSignUpFormUrl(series));
        waitForPageLoad(driver).until(ExpectedConditions.titleIs("CalNet Central Authentication Service - Single Sign-on"));
        return new CalNetAuthPage(driver);
    }

    public Select getPublishDelaySelect() {
        return new Select(publishDelaySelect);
    }

    /**
     * Makes recording input selections
     *
     * @param series the series
     * @return the same page
     */
    public SignUpPage chooseInputs(Series series) {
        logger.info("Recording input selection is " + series.getInputs().getDescription());
        if (series.getInputs() == Inputs.AUDIO_ONLY) { audioOnly.click(); }
        else if (series.getInputs() == Inputs.SCREENCAST) { screencast.click(); }
        else if (series.getInputs() == Inputs.VIDEO_ONLY) { videoOnly.click(); }
        else if (series.getInputs() == Inputs.VIDEO_SCREEN) { videoAndScreencast.click(); }
        return this;
    }

    /**
     * Selects the publish delay for the recordings
     *
     * @param series the series
     * @return the same page
     */
    public SignUpPage setPublishDelay(Series series) {
        logger.info("Publish delay is " + series.getPublishDelay().getDelayDesc());
        if (series.getPublishDelay() == PublishDelay.ZERO_DAYS) { noPublishDelayRadio.click(); }
        else {
            publishDelayRadio.click();
            getPublishDelaySelect().selectByVisibleText(series.getPublishDelay().getDelayDaysString());
        }
        return this;
    }

    /**
     * Clicks the "agree" checkbox
     *
     * @return the same page
     * @throws Exception
     */
    public SignUpPage agreeToTerms() throws Exception {
        logger.info("Agreeing to T&C");
        agreementConfirmationCbx.click();
        Thread.sleep(1000);
        return this;
    }

    /**
     * Submits the sign-up form and waits the configured page load timeout
     *
     * @return the same page
     * @throws Exception
     */
    public SignUpPage submitForm() throws Exception {
        logger.info("Clicking the sign up button");
        signUpButton.click();
        Thread.sleep(TimeoutUtils.getPageLoadWaitMs());
        return this;
    }

    /**
     * Clicks the logout link on the page, which redirects the user to the CalNet logout confirmation page
     *
     * @param driver the current browser
     * @return the CalNet page
     * @throws IOException
     */
    public CalNetAuthPage clickLogOut(WebDriver driver) throws IOException {
        logger.info("Clicking the log out link");
        logOutLink.click();
        waitForPageLoad(driver).until(ExpectedConditions.titleIs("Logout Successful - CAS – Central Authentication Service"));
        return new CalNetAuthPage(driver);
    }

    public SignUpPage loadCompleteSubmit(WebDriver driver, Series series) throws Exception {
        loadPage(driver, series);
        chooseInputs(series);
        setPublishDelay(series);
        submitForm();
        return this;
    }

    public void verifyStaticConfirmMsg(StringBuffer verificationErrors, Series series, Date recordingOneDate) {
        TestUtils.verifyEquals(verificationErrors, "Sign Up for Course Capture (Webcast)", pageHeading.getText());
        TestUtils.verifyEquals(verificationErrors, series.getSeriesName(), courseTitle.getText());
        TestUtils.verifyEquals(verificationErrors, series.getCaptureAgent().getRoom() + "  |  " + DateFormatUtils.getDayOfWeek(recordingOneDate) + " 6:30pm-7:00pm",
                locationAndTime.getText());
        TestUtils.verifyEquals(verificationErrors, "Recording Type: " + series.getInputs().getDescription(), recordingTypeConfirmation.getText());
        TestUtils.verifyEquals(verificationErrors, "Recording Time: " + DateFormatUtils.getDayOfWeek(recordingOneDate) + " 6:37pm-7:03pm", recordingTimeConfirmation.getText());
        TestUtils.verifyFalse(verificationErrors, isElementPresent(recordingAvailabilityConfirmation));
        if (series.getPublishDelay() == PublishDelay.ZERO_DAYS) {
            TestUtils.verifyEquals(verificationErrors, "Publish Delay: " + series.getPublishDelay().getDelayDesc(), publishDelayConfirmation.getText());
        } else {
            TestUtils.verifyEquals(verificationErrors, "Publish Delay: " + series.getPublishDelay().getDelayDesc() + " after capture", publishDelayConfirmation.getText());
        }



    }

}
