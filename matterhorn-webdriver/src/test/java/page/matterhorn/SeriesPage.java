package page.matterhorn;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.Pages;

import java.io.IOException;

/**
 * @author Paul Farestveit
 */
public class SeriesPage extends Pages {

    final Logger logger = LoggerFactory.getLogger(getClass());

    public SeriesPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Clicks the homepage link.
     *
     * @param driver the current browser
     * @return the homepage
     */
    public HomePage clickHomepageLink(WebDriver driver) throws Exception {
        logger.info("Clicking the homepage link");
        waitForElementAndClick(driver, driver.findElement(By.xpath("//img[@title='Matterhorn Home Page']")));
        return new HomePage(driver);
    }

    /**
     * Clicks the recordings tab on the series page
     *
     * @param driver the current browser
     * @return the recordings page
     */
    public RecordingsPage clickRecordingsTab(WebDriver driver) throws Exception {
        logger.info("Clicking the recordings tab");
        waitForElementAndClick(driver, driver.findElement(By.id("i18n_tab_recording")));
        // Sometimes Matterhorn doesn't respond to the first click, so just in case click again
        waitForElementAndClick(driver, driver.findElement(By.id("i18n_tab_recording")));
        return new RecordingsPage(driver);
    }

    /**
     * Clicks the archives tab on the series page
     *
     * @param driver the current browser
     * @return the archives page
     * @throws IOException
     */
    public ArchivesPage clickArchivesTab(WebDriver driver) throws Exception {
        logger.info("Clicking archives tab");
        waitForElementAndClick(driver, driver.findElement(By.id("i18n_tab_episode")));
        return new ArchivesPage(driver);
    }

}
