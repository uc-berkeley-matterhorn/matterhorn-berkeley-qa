package page.matterhorn;

import domain.Recording;
import org.apache.commons.lang3.time.DateUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.Pages;
import util.DateFormatUtils;

import java.io.IOException;
import java.util.Date;

/**
 * @author Paul Farestveit
 */
public class ReviewTrimPage extends Pages {

    final Logger logger = LoggerFactory.getLogger(getClass());

    // TRIM CONTROLS

    @FindBy(id = "inPoint") public WebElement inPoint;

    // EDIT METADATA INPUTS

    @FindBy(id = "titleField") public WebElement recordingTitle;

    @FindBy(id = "creator") public WebElement recordingCreator;

    private String titleFieldId() { return "titleField"; }

    public ReviewTrimPage(WebDriver driver) throws Exception {
        super(driver);
        PageFactory.initElements(driver, this);
        switchToIframe(driver, driver.findElement(By.id("holdActionUI")));
    }

    /**
     * Moves the start point of the video forward by a designated number of clicks
     *
     * @param driver the current browser
     * @param secondsToAdvance how many clicks
     * @return the same page
     * @throws Exception
     */
    public ReviewTrimPage moveInpointForward(WebDriver driver, int secondsToAdvance) throws Exception {
        logger.info("Moving the in-point forward by " + secondsToAdvance + " seconds");
        // Line below adds 3 clicks to the desired seconds, since the first 2 never register in Matterhorn's UI
        for (int clicks = 0; clicks < (secondsToAdvance + 2); clicks++) {
            waitForElementAndClick(driver, driver.findElement(By.id("step-in-forward")));
            Thread.sleep(1000);
        }
        return this;
    }

    /**
     * Moves the end point of the video backward by a designated number of clicks
     *
     * @param driver the current browser
     * @param secondsToReverse how many clicks
     * @return the same page
     * @throws Exception
     */
    public ReviewTrimPage moveOutpointBack(WebDriver driver, int secondsToReverse) throws Exception {
        logger.info("Moving out-point back by " + secondsToReverse + " seconds");
        for (int clicks = 0; clicks < secondsToReverse; clicks++) {
            waitForElementAndClick(driver, driver.findElement(By.id("step-out-backward")));
            Thread.sleep(1000);
        }
        return this;
    }

    /**
     * Sets the episode title to the recording's current title
     *
     * @param driver the current browser
     * @param recording the episode being trimmed
     * @return the same page
     * @throws IOException
     */
    public ReviewTrimPage editTitleMetadata(WebDriver driver, Recording recording) throws Exception {
        logger.info("Changing recording title to " + recording.getTitle());
        waitForElementAndClick(driver, driver.findElement(By.id(titleFieldId())));
        clearAndType(driver, driver.findElement(By.id(titleFieldId())), recording.getTitle());
        return this;
    }

    /**
     * Sets the episode recording date to the recording's current date
     *
     * @param driver the current browser
     * @param recording the episode being trimmed
     * @return the same page
     */
    public ReviewTrimPage editDateTimeMetadata(WebDriver driver, Recording recording) throws Exception {
        logger.info("Changing recording date and time to " + DateFormatUtils.getDayAndDate(recording.getRecordingDate())
                + " " + DateFormatUtils.getHourMinute(recording.getRecordingDate()));
        clearAndType(driver, driver.findElement(By.id("recordDate")), DateFormatUtils.getDateYYYY_MM_DD(recording.getRecordingDate()));
        waitForElementAndSelect(driver, driver.findElement(By.id("startTimeHour")), DateFormatUtils.getHour(recording.getRecordingDate()));
        waitForElementAndSelect(driver, driver.findElement(By.id("startTimeMin")), DateFormatUtils.getMinute(recording.getRecordingDate()));
        return this;
    }

    /**
     * Sets the episode publish delay to the recording's current delay
     *
     * @param driver the current browser
     * @param recording the episode being trimmed
     * @return the same page
     * @throws IOException
     */
    public ReviewTrimPage editPublishDelay(WebDriver driver, Recording recording) throws Exception {
        logger.info("Changing publish delay to " + recording.getDelay());
        String publishDate = DateFormatUtils.getDateMDYYYY(DateUtils.addDays(new Date(), recording.getDelay().getDelayDaysNumber()));
        clearAndType(driver, driver.findElement(By.id("episodePublishDate")), publishDate);
        return this;
    }

    /**
     * Clicks continue to save the edits and trims.  The hardcoded pause is required, since the subsequent recordings page is
     * always blank, so there's no way to verify arrival at the updated recordings page.
     *
     * @param driver the current browser
     * @return the recordings page
     * @throws Exception
     */
    public RecordingsPage clickContinue(WebDriver driver) throws Exception {
        logger.info("Clicking continue");
        waitForElementAndClick(driver, driver.findElement(By.id(titleFieldId())));
        waitForElementAndClick(driver, driver.findElement(By.id("continueBtn")));
        Thread.sleep(5000);
        return new RecordingsPage(driver);
    }

}
