package page.matterhorn;

import domain.Recording;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import page.Pages;

import java.io.IOException;

/**
 * @author Paul Farestveit
 */
public class RecordingEditPage extends Pages {

    final Logger logger = LoggerFactory.getLogger(this.getClass());

    @FindBy(id = "title") public WebElement titleInput;

    @FindBy(id = "submitButton") public WebElement updateButton;

    @FindBy(xpath = "//li[@id='field-title']/span[@class='fieldValue']") public WebElement titleField;

    public RecordingEditPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Sets the recording title on the edit page.
     *
     * @param driver the current browser
     * @param recording the recording to be edited
     * @return the same page
     * @throws IOException
     */
    public RecordingEditPage editRecordingTitle(WebDriver driver, Recording recording) throws Exception {
        logger.info("Changing recording title to " + recording.getTitle());
        clearAndType(driver, titleInput, recording.getTitle());
        if (!titleInput.getText().matches(recording.getTitle())) {
            clearAndType(driver, titleInput, recording.getTitle());
        }
        return this;
    }

    /**
     * Saves edits on the edit page
     *
     * @param driver the current browser
     * @return the same page
     * @throws IOException
     */
    public RecordingsPage saveRecordingEdits(WebDriver driver) throws Exception {
        logger.info("Saving recording edits");
        waitForElementAndClick(driver, updateButton);
        return new RecordingsPage(driver);
    }

}
