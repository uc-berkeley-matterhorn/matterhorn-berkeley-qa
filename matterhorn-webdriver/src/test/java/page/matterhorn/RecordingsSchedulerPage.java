package page.matterhorn;

import domain.*;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.Pages;
import util.*;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.fail;

/**
 * @author Paul Farestveit
 */
public class RecordingsSchedulerPage extends Pages {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @FindBy(id = "title") public WebElement recordingTitle;

    RecordingsSchedulerPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    private String singleRecordingCheckboxId() { return "singleRecording"; }

    private String multipleRecordingsCheckboxId() { return "multipleRecordings"; }

    private String singleRecordingStartHourId() { return "startTimeHour"; }

    private String singleRecordingStartMinId() { return "startTimeMin"; }

    private String singleRecordingDurationHourId() { return "durationHour"; }

    private String singleRecordingDurationMinId() { return "durationMin"; }

    private String captureAgentId() { return "agent"; }

    private String recurCaptureAgentId() { return "recurAgent"; }

    private String recurringRecordingStartHourId() { return "recurStartTimeHour"; }

    private String recurringRecordingStartMinId() { return "recurStartTimeMin"; }

    private String recurringRecordingDurationHourId() { return "recurDurationHour"; }

    private String recurringRecordingDurationMinId() { return "recurDurationMin"; }

    private String publishDelayId() { return "delayPublishByDaysDays"; }

    private String trimHoldId() { return "trimHold"; }

    private String submitButtonId() { return "submitButton"; }

    /**
     * Clicks the homepage link
     *
     * @param driver the current browser
     * @return the homepage
     */
    public HomePage clickHomepageLink(WebDriver driver) {
        logger.info("Clicking homepage link");
        driver.findElement(By.xpath("//img[@title='Matterhorn Home Page']")).click();
        return new HomePage(driver);
    }

    /**
     * Selects the single recording radio button and waits for a single recording input field to appear before moving on
     *
     * @param driver WebDriver instance
     * @return same page
     */
    public RecordingsSchedulerPage selectSingleRecording(WebDriver driver) throws IOException {
        logger.info("Selecting the single recording radio button");
        if (!driver.findElement(By.id(singleRecordingCheckboxId())).isSelected()) {
            driver.findElement(By.id(singleRecordingCheckboxId())).click();
        }
        waitForPageUpdate(driver).until(ExpectedConditions.presenceOfElementLocated(By.id("startDate")));
        return this;
    }

    /**
     * Selects the recurring recording radio button and waits for a recurring recording input field to appear before moving on.
     * Occasionally, the publish delay select is not visible by the time a test needs to interact with it, so this method
     * tries to wait for it.
     *
     * @param driver WebDriver instance
     * @return same page
     */
    public RecordingsSchedulerPage selectMultipleRecordings(WebDriver driver) throws Exception {
        logger.info("Selecting the group of recordings radio button");
        waitForElementAndClick(driver, driver.findElement(By.id(multipleRecordingsCheckboxId())));
        if (!driver.findElement(By.id(multipleRecordingsCheckboxId())).isSelected()) {
            driver.findElement(By.id(multipleRecordingsCheckboxId())).click();
        }
        int attempts = 3;
        for (int i = 0; i <= attempts; i++) {
            if (i == attempts) fail("Gave up trying to find the publish delay drop-down");
            try {
                waitForPageUpdate(driver).until(ExpectedConditions.visibilityOfElementLocated(By.id("delayPublishByDaysDays")));
                break;
                }
            catch (Exception e) {
                logger.info("Can't find it, gonna try again");
                waitForElementAndClick(driver, driver.findElement(By.id(singleRecordingCheckboxId())));
                Thread.sleep(TimeoutUtils.getAjaxWaitMs());
                waitForElementAndClick(driver, driver.findElement(By.id(multipleRecordingsCheckboxId())));
            }
        }
        return this;
    }

    /**
     * Inputs the basic recording metadata including title, presenter, series, and licensing.  Use the same method for
     * single or recurring recordings, as the fields are the same.
     *
     * @param driver    the current browser
     * @param recording the recording(s) being scheduled
     * @return same page
     */
    public RecordingsSchedulerPage inputRecordingMetadata(WebDriver driver, Recording recording) throws Exception {
        logger.info("Recording title is " + recording.getTitle());
        clearAndType(driver, driver.findElement(By.id("title")), recording.getTitle());
        if (recording.getInstructors() != null) {
            clearAndType(driver, driver.findElement(By.id("creator")), StringUtils.join(recording.getInstructors(), ", "));
        }
        logger.info("Recording license is " + recording.getLicenseDescription());
        waitForElementAndSelect(driver, driver.findElement(By.id("license")), recording.getLicenseDescription());
        if (recording.getSeries() != null) {
            logger.info("Recording series is " + recording.getSeriesTitle());
            clearAndType(driver, driver.findElement(By.id("seriesSelect")), recording.getSeries().getSeriesTestId());
            waitForPageUpdate(driver).until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//ul[@class='ui-autocomplete ui-menu " +
                    "ui-widget ui-widget-content ui-corner-all']/li/a"), recording.getSeriesTitle()));
            Thread.sleep(1000);
            driver.findElement(By.linkText(recording.getSeriesTitle())).click();
        }
        return this;
    }

    /**
     * Completes the scheduling information for a single recording
     *
     * @param driver    the current browser
     * @param recording the recording(s) being scheduled
     * @return same page
     */
    public RecordingsSchedulerPage scheduleSingleCapture(WebDriver driver, Recording recording) throws Exception {
        logger.info("Start date and time is " + DateFormatUtils.getDateYYYY_MM_DD(recording.getRecordingDate()) + " "
                + DateFormatUtils.getHour(recording.getRecordingDate()) + "h " + DateFormatUtils.getMinute(recording.getRecordingDate()) + "m");
        clearAndType(driver, driver.findElement(By.id("startDate")), DateFormatUtils.getDateYYYY_MM_DD(recording.getRecordingDate()));
        Thread.sleep(2000);
        waitForElementAndClick(driver, driver.findElement(By.id("title")));
        waitForElementAndSelect(driver, driver.findElement(By.id(singleRecordingStartHourId())), DateFormatUtils.getHour(recording.getRecordingDate()));
        waitForElementAndSelect(driver, driver.findElement(By.id(singleRecordingStartMinId())), DateFormatUtils.getMinute(recording.getRecordingDate()));

        logger.info("Duration is " + RecordingUtils.getDurationHours() + "h " + RecordingUtils.getDurationMinutes() + "m");
        waitForElementAndSelect(driver, driver.findElement(By.id(singleRecordingDurationHourId())), RecordingUtils.getDurationHours());
        waitForElementAndSelect(driver, driver.findElement(By.id(singleRecordingDurationMinId())), RecordingUtils.getDurationMinutes());

        logger.info("Capture agent is " + recording.getSeries().getCaptureAgent());
        waitForElementAndSelect(driver, driver.findElement(By.id(captureAgentId())), recording.getSeries().getCaptureAgent().getCaptureAgentName());

        waitForPageUpdate(driver).until(ExpectedConditions.presenceOfElementLocated(By.id("inputList")));
        if (recording.getSeries().getCaptureAgent().agentHasScreen()
                && (recording.getSeries().getInputs() == Inputs.SCREENCAST || recording.getSeries().getInputs() == Inputs.VIDEO_SCREEN)
                && !driver.findElement(By.id("screen")).isSelected()) {
            driver.findElement(By.id("screen")).click();
        } else if (recording.getSeries().getCaptureAgent().agentHasScreen()
                && (recording.getSeries().getInputs() == Inputs.AUDIO_ONLY || recording.getSeries().getInputs() == Inputs.VIDEO_ONLY)
                && driver.findElement(By.id("screen")).isSelected()) {
            driver.findElement(By.id("screen")).click();
        }
        if (recording.getSeries().getCaptureAgent().agentHasVideo()
                && (recording.getSeries().getInputs() == Inputs.VIDEO_ONLY || recording.getSeries().getInputs() == Inputs.VIDEO_SCREEN)
                && !driver.findElement(By.id("video")).isSelected()) {
            driver.findElement(By.id("video")).click();
        } else if (recording.getSeries().getCaptureAgent().agentHasVideo()
                && (recording.getSeries().getInputs() == Inputs.AUDIO_ONLY || recording.getSeries().getInputs() == Inputs.SCREENCAST)
                && driver.findElement(By.id("video")).isSelected()) {
            driver.findElement(By.id("video")).click();
        }
        return this;
    }

    /**
     * Completes the scheduling information for a recurring recording
     *
     * @param driver    WebDriver instance
     * @param recording the recording object associated with the recurring recordings
     * @return same page
     */
    public RecordingsSchedulerPage scheduleRecurringCaptures(WebDriver driver, Recording recording) throws Exception {
        String[] recurrence = {DateFormatUtils.getDayOfWeek(recording.getSeries().getSeriesStartDate())};
        if (Arrays.asList(recurrence).contains("Sunday")) { driver.findElement(By.id("repeatSun")).click(); }
        if (Arrays.asList(recurrence).contains("Monday")) { driver.findElement(By.id("repeatMon")).click(); }
        if (Arrays.asList(recurrence).contains("Tuesday")) { driver.findElement(By.id("repeatTue")).click(); }
        if (Arrays.asList(recurrence).contains("Wednesday")) { driver.findElement(By.id("repeatWed")).click(); }
        if (Arrays.asList(recurrence).contains("Thursday")) { driver.findElement(By.id("repeatThu")).click(); }
        if (Arrays.asList(recurrence).contains("Friday")) { driver.findElement(By.id("repeatFri")).click(); }
        if (Arrays.asList(recurrence).contains("Saturday")) { driver.findElement(By.id("repeatSat")).click(); }

        logger.info("Start date and time is " + DateFormatUtils.getDateYYYY_MM_DD(recording.getSeries().getSeriesStartDate()) + " "
                + DateFormatUtils.getHour(recording.getRecordingDate()) + "h " + DateFormatUtils.getMinute(recording.getRecordingDate()) + "m");
        logger.info("Duration is " + RecordingUtils.getDurationHours() + "h " + RecordingUtils.getDurationMinutes() + "m");
        clearAndType(driver, driver.findElement(By.id("recurStart")), DateFormatUtils.getDateYYYY_MM_DD(recording.getSeries().getSeriesStartDate()));
        waitForElementAndClick(driver, driver.findElement(By.id("title")));
        clearAndType(driver, driver.findElement(By.id("recurEnd")), DateFormatUtils.getDateYYYY_MM_DD(recording.getSeries().getSeriesEndDate()));
        waitForElementAndClick(driver, driver.findElement(By.id("title")));
        waitForElementAndSelect(driver, driver.findElement(By.id(recurringRecordingStartHourId())), DateFormatUtils.getHour(recording.getRecordingDate()));
        waitForElementAndSelect(driver, driver.findElement(By.id(recurringRecordingStartMinId())), DateFormatUtils.getMinute(recording.getRecordingDate()));
        waitForElementAndSelect(driver, driver.findElement(By.id(recurringRecordingDurationHourId())), RecordingUtils.getDurationHours());
        waitForElementAndSelect(driver, driver.findElement(By.id(recurringRecordingDurationMinId())), RecordingUtils.getDurationMinutes());

        logger.info("Waiting for the capture agent inputs for agent " + recording.getSeries().getCaptureAgent().getCaptureAgentName());
        waitForElementAndSelect(driver, driver.findElement(By.id(recurCaptureAgentId())), recording.getSeries().getCaptureAgent().getCaptureAgentName());
        waitForPageUpdate(driver).until(ExpectedConditions.presenceOfElementLocated(By.id("recurInputList")));
        if (recording.getSeries().getCaptureAgent().agentHasScreen()
                && (recording.getSeries().getInputs() == Inputs.SCREENCAST || recording.getSeries().getInputs() == Inputs.VIDEO_SCREEN)
                && !driver.findElement(By.id("screen")).isSelected()) {
            driver.findElement(By.id("screen")).click();
        } else if (recording.getSeries().getCaptureAgent().agentHasScreen()
                && (recording.getSeries().getInputs() == Inputs.AUDIO_ONLY || recording.getSeries().getInputs() == Inputs.VIDEO_ONLY)
                && driver.findElement(By.id("screen")).isSelected()) {
            driver.findElement(By.id("screen")).click();
        }
        if (recording.getSeries().getCaptureAgent().agentHasVideo()
                && (recording.getSeries().getInputs() == Inputs.VIDEO_ONLY || recording.getSeries().getInputs() == Inputs.VIDEO_SCREEN)
                && !driver.findElement(By.id("video")).isSelected()) {
            driver.findElement(By.id("video")).click();
        } else if (recording.getSeries().getCaptureAgent().agentHasVideo()
                && (recording.getSeries().getInputs() == Inputs.AUDIO_ONLY || recording.getSeries().getInputs() == Inputs.SCREENCAST)
                && driver.findElement(By.id("video")).isSelected()) {
            driver.findElement(By.id("video")).click();
        }
        return this;
    }

    /**
     * Enters a publish delay for multiple recordings
     *
     * @param driver the current browser
     * @param series the series
     * @return the same page
     * @throws IOException foo
     */
    public RecordingsSchedulerPage addPublishDelayMultiRecordings(WebDriver driver, Series series) throws Exception {
        logger.info("Adding publish delay of " + series.getPublishDelay().getDelayDesc());
        if (series.getPublishDelay() != null) {
            if (series.getPublishDelay() == PublishDelay.ZERO_DAYS) {
                waitForElementAndSelect(driver, driver.findElement(By.id(publishDelayId())), "-- none --");
            } else {
                waitForElementAndSelect(driver, driver.findElement(By.id(publishDelayId())), series.getPublishDelay().getDelayDaysString());
            }
        }
        return this;
    }

    /**
     * Clicks the "Review / Trim" checkbox
     *
     * @param driver the current browser
     * @return same page
     */
    public RecordingsSchedulerPage chooseReviewTrim(WebDriver driver) throws Exception {
        logger.info("Checking the review/trim box");
        waitForElementAndClick(driver, driver.findElement(By.id(trimHoldId())));
        waitForPageUpdate(driver).until(ExpectedConditions.elementToBeSelected(By.id(trimHoldId())));
        return this;
    }

    /**
     * Clicks the button to schedule the recording(s)
     *
     * @param driver the current browser
     * @return same page with confirmation of metadata
     */
    public RecordingsSchedulerPage submitScheduledRecording(WebDriver driver) throws Exception {
        logger.info("Clicking submit button to schedule recording(s)");
        waitForElementAndClick(driver, driver.findElement(By.id(submitButtonId())));
        Thread.sleep(3000);
        return this;
    }

}
