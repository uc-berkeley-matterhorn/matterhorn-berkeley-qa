package page.matterhorn;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.CalNetAuthPage;
import page.Pages;
import util.AuthenticationUtils;

/**
 * @author Paul Farestveit
 */
public class HomePage extends Pages {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @FindBy(id = "adminlink")
    public WebElement adminLink;

    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public RecordingsPage logIn(WebDriver driver, CalNetAuthPage calNetAuthPage) throws Exception {
        driver.get(AuthenticationUtils.getMatterhornAdminTool());
        calNetAuthPage.logInAdminTool(driver);
        return new RecordingsPage(driver);
    }

    /**
     * Clicks admin link on the Matterhorn homepage
     * @param driver WebDriver instance
     * @return the page listing all recordings
     */
    public RecordingsPage clickAdminLink(WebDriver driver) throws Exception {
        logger.info("Clicking admin link on homepage");
        adminLink.click();
        Thread.sleep(1000);
        if (isLinkPresentByText(driver, "Go to Admin Tools")) {
            adminLink.click();
        }
        return new RecordingsPage(driver);
    }

}
