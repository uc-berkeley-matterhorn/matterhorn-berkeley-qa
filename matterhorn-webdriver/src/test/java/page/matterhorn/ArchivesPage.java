package page.matterhorn;

import domain.Recording;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.Pages;
import util.DateFormatUtils;
import util.TimeoutUtils;

import java.io.IOException;

import static org.junit.Assert.fail;

/**
 * @author Paul Farestveit
 */
public class ArchivesPage extends Pages {

    final Logger logger = LoggerFactory.getLogger(this.getClass());

    @FindBy(xpath = "//span[@class='searchbox-search-icon ui-icon ui-icon-search']/following-sibling::select") WebElement searchSelect;

    public ArchivesPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String submitButtonId() { return "awf-submit"; }

    private String rowCheckboxXpath(int rowNumber) { return "//table[@id='episodesTable']/tbody/tr[" + String.valueOf(rowNumber) + "]/td/input"; }

    private String workflowSelectId() { return "awf-select"; }

    private String trimHoldCheckboxId() { return "trimHold"; }

    private String workflowStartButtonId() { return "awf-start"; }

    /**
     * Click homepage link
     *
     * @param driver existing driver instance
     * @return homepage
     */
    public HomePage clickHomepageLink(WebDriver driver) throws Exception {
        logger.info("Clicking homepage link");
        waitForElementAndClick(driver, driver.findElement(By.xpath("//img[@title='Matterhorn Home Page']")));
        return new HomePage(driver);
    }

    /**
     * Click recordings tab
     *
     * @param driver existing driver instance
     * @return recordings page
     */
    public RecordingsPage clickRecordingsTab(WebDriver driver) throws Exception {
        logger.debug("Clicking recordings tab");
        waitForElementAndClick(driver, driver.findElement(By.id("i18n_tab_recording")));
        return new RecordingsPage(driver);
    }

    public ArchivesPage clickSubmitButton(WebDriver driver) throws Exception {
        logger.info("Clicking submit button");
        waitForPageUpdate(driver).until(ExpectedConditions.elementToBeClickable(By.id(submitButtonId())));
        Thread.sleep(1000);
        driver.findElement(By.id(submitButtonId())).click();
        if (driver.findElement(By.id(submitButtonId())).isDisplayed()) {
            driver.findElement(By.id(submitButtonId())).click();
        }
        // Let Matterhorn think about things for a few seconds or you'll hit a JavaScript error pop-up when you try to leave the page
        Thread.sleep(5000);
        return this;
    }

    // ************** SEARCH **************

    /**
     * Search for an existing recording.  Need to specify number of expected results (since this is important for some tests).  Also
     * need to specify the number of search attempts to make.  A hack to deal with Matterhorn's unpredictable UI.
     *
     * @param driver               existing driver instance
     * @param recording            the recording object
     * @param expectedResultsCount number of results expected (e.g., the "2" in "2 found")
     * @return same page
     * @throws Exception
     */
    public ArchivesPage searchForRecording(WebDriver driver, Recording recording, int expectedResultsCount) throws Exception {
        waitForElementPresent(driver.findElement(By.id("episodesTable")), TimeoutUtils.getPageLoadWaitSec());
        logger.info("Searching for recording by title " + recording.getRecordingTestId());
        int maxAttempts = 5;
        for (int attempts = 1; attempts <= maxAttempts; attempts++) {
            if (attempts == maxAttempts) {
                logger.error("Unable to find recording");
                fail();
            }
            else {
                try {
                    waitForElementAndClick(driver, driver.findElement(By.xpath("//input[@class='searchbox-text-input ui-corner-all']")));
                    clearAndType(driver, driver.findElement(By.xpath("//input[@class='searchbox-text-input ui-corner-all']")), recording.getRecordingTestId());
                    waitForElementAndSelect(driver, searchSelect, "Title");
                    waitForElementAndClick(driver, driver.findElement(By.xpath("//span[@class='searchbox-search-icon ui-icon ui-icon-search']")));
                    if (driver.findElement(By.id("filterRecordingCount")).getText().matches(String.valueOf(expectedResultsCount) + " found")) {
                        break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Thread.sleep(1000);
            }
        }
        return this;
    }

    // ************** RETRACT **************

    /**
     * Clicks the checkbox to select the recording on designated row, chooses the retract option on the drop-down, and then hits "Apply" button.
     * Enters the processing instructions for the retraction in the pop-up.
     *
     * @param driver existing driver instance
     * @param rowNumber row node
     * @return same page
     */
    public ArchivesPage retractRecording(WebDriver driver, int rowNumber) throws Exception {
        logger.info("Retracting recording on row " + String.valueOf(rowNumber));
        waitForElementAndSelect(driver, driver.findElement(By.id(workflowSelectId())), "Retract media package");
        waitForElementAndClick(driver, driver.findElement(By.xpath(rowCheckboxXpath(rowNumber))));
        waitForElementAndClick(driver, driver.findElement(By.id(workflowStartButtonId())));
        logger.info("Checking retraction instructions");
        if (!driver.findElement(By.id("youtube")).isSelected()) {
            driver.findElement(By.id("youtube")).click();
        }
        clickSubmitButton(driver);
        return this;
    }

    // ************** REPUBLISH **************

    /**
     * Clicks the checkbox to select the recording on designated row, chooses the republish option on the drop-down, and then hits "Apply" button.
     *
     * @param driver existing driver instance
     * @return same page
     */
    public ArchivesPage selectRepublishRecording(WebDriver driver, int rowNumber) throws Exception {
        logger.info("Republishing recording on row " + String.valueOf(rowNumber));
        waitForPageLoad(driver).until(ExpectedConditions.elementToBeClickable(By.xpath(rowCheckboxXpath(rowNumber))));
        for (int attempts = 1; attempts <= 5; attempts++) {
            try {
                driver.findElement(By.xpath(rowCheckboxXpath(rowNumber))).click();
                if (driver.findElement(By.xpath(rowCheckboxXpath(rowNumber))).isSelected()) {
                    break;
                }
            } catch (Exception e) { logger.debug("Tried to click the checkbox, didn't work."); }
            Thread.sleep(1000);
        }
        waitForElementAndSelect(driver, driver.findElement(By.id(workflowSelectId())), "Lecture Processing and Distribution (UCB standard)");
        driver.findElement(By.id("awf-start")).click();
        return this;
    }

    /**
     * Enters the processing instructions for the republish in the pop-up.
     *
     * @param driver  existing driver instance
     * @param youTube publish to YouTube?  yes if true
     * @return same page
     */
    public ArchivesPage inputRepublishProcessingInstructions(WebDriver driver, boolean youTube) throws IOException {
        logger.info("Selecting processing instructions for republish");
        waitForPageUpdate(driver).until(ExpectedConditions.visibilityOfElementLocated(By.id("ui-dialog-title-awf-window")));
        if ((youTube && !driver.findElement(By.id("youtube")).isSelected()) || (!youTube && driver.findElement(By.id("youtube")).isSelected())) {
            driver.findElement(By.id("youtube")).click();
        }
        return this;
    }

    public ArchivesPage republishWithTrim(WebDriver driver, int rowNumber) throws Exception {
        selectRepublishRecording(driver, rowNumber);
        inputRepublishProcessingInstructions(driver, true);
        logger.info("Checking the review / trim box");
        waitForElementAndClick(driver, driver.findElement(By.id(trimHoldCheckboxId())));
        waitForPageUpdate(driver).until(ExpectedConditions.elementToBeSelected(By.id(trimHoldCheckboxId())));
        clickSubmitButton(driver);
        return this;
    }

    public ArchivesPage republishWithoutTrim(WebDriver driver, int rowNumber) throws Exception {
        selectRepublishRecording(driver, rowNumber);
        inputRepublishProcessingInstructions(driver, true);
        clickSubmitButton(driver);
        return this;
    }

    // ************** EDIT / REPUBLISH METADATA **************

    /**
     * Clicks the "Edit" link on row one
     *
     * @param driver existing driver instance
     * @return same page
     */
    public ArchivesPage clickEditLinkRowOne(WebDriver driver) throws Exception {
        logger.info("Clicking edit link on row one");
        waitForPageLoad(driver).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@id='episodesTable']/tbody/tr[1]/td[8]/a")));
        Thread.sleep(TimeoutUtils.getAjaxWaitMs());
        driver.findElement(By.xpath("//table[@id='episodesTable']/tbody/tr[1]/td[8]/a")).click();
        return this;
    }

    /**
     * Inputs new metadata for the recording.  Series is NOT part of metadata updates.
     *
     * @param driver    existing driver instance
     * @param recording the recording
     * @return same page
     */
    public ArchivesPage inputNewMetadata(WebDriver driver, Recording recording) throws Exception {
        logger.info("Inputting new metadata");
        clearAndType(driver, driver.findElement(By.id("titleField")), recording.getTitle());
        clearAndType(driver, driver.findElement(By.id("creator")), StringUtils.join(recording.getInstructors(), ", "));
        clearAndType(driver, driver.findElement(By.id("recordDate")), DateFormatUtils.getDateYYYY_MM_DD(recording.getRecordingDate()));
        Thread.sleep(1000);
        return this;
    }

    /**
     * Submits the new metadata in the pop-up
     *
     * @param driver existing driver instance
     * @return same page
     */
    public ArchivesPage submitNewMetadata(WebDriver driver) throws IOException {
        logger.info("Saving new metadata");
        driver.findElement(By.id("mpe-submit")).click();
        waitForMetadataUpdate(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.id("ui-dialog-title-mpe-window")));
        return this;
    }

    /**
     * Clicks the checkbox to select the recording on row one, chooses the republish metadata option in the drop-down, and then
     * hits "Apply" button.
     *
     * @param driver existing driver instance
     * @return same page
     */
    public ArchivesPage selectRepublishMetadataRowOne(WebDriver driver) throws Exception {
        logger.info("Republishing metadata on row one");
        waitForElementXpathAndClick(driver, "//table[@id='episodesTable']/tbody//input");
        waitForElementAndSelect(driver, driver.findElement(By.id(workflowSelectId())), "Republish metadata");
        waitForElementAndClick(driver, driver.findElement(By.id(workflowStartButtonId())));
        return this;
    }

    public ArchivesPage editAndRepublishMetadata(WebDriver driver, Recording recording) throws Exception {
        searchForRecording(driver, recording, 1);
        clickEditLinkRowOne(driver);
        inputNewMetadata(driver, recording);
        submitNewMetadata(driver);
        selectRepublishMetadataRowOne(driver);
        clickSubmitButton(driver);
        return this;
    }

}
