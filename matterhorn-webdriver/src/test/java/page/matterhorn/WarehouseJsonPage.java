package page.matterhorn;

import domain.Series;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.Pages;
import util.AuthenticationUtils;
import util.SalesforceUtils;
import util.TimeoutUtils;

import static org.junit.Assert.fail;

/**
 * @author Paul Farestveit
 */
public class WarehouseJsonPage extends Pages {

    final Logger logger = LoggerFactory.getLogger(this.getClass());

    public WarehouseJsonPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Hits the warehouse JSON page in the browser and parses the JSON content
     *
     * @param driver the current browser
     * @return the parsed JSON
     * @throws Exception
     */
    private JSONArray getCourses(WebDriver driver) throws Exception {
        driver.get(AuthenticationUtils.getWebcastWarehouseJson());
        waitForElementPresent(driver.findElement(By.xpath("//pre")), TimeoutUtils.getPageLoadWaitSec());
        String body = driver.findElement(By.xpath("//pre")).getText();
        JSONObject fullJson = new JSONObject(body);
        return fullJson.getJSONArray("courses");
    }

    /**
     * Searches for a given course in the JSON
     *
     * @param courses the parsed JSON for all courses
     * @param series the searched course
     * @return the JSON object for the searched course
     * @throws Exception
     */
    private JSONObject searchForCourseInArray(JSONArray courses, Series series) throws Exception {
        JSONObject desiredCourse = null;
        for (int i = 0; i < courses.length(); i++) {
            JSONObject course = (JSONObject) courses.get(i);
            if (course.getInt("year") == Integer.parseInt(SalesforceUtils.getCurrentYear())
                    && course.getString("semester").equals(SalesforceUtils.getCurrentTerm())
                    && course.getInt("ccn") == Integer.parseInt(series.getCcn())) {
                desiredCourse = course;
                break;
            }
        }
        return desiredCourse;
    }

    /**
     * Waits for a given course to appear in the parsed JSON of all courses.  Waits a configured amount of time between checks
     * and checks a configured maximum number of times.  Fails if the course never appears.
     *
     * @param driver the current browser
     * @param series the searched course
     * @return the JSON object for the searched course
     * @throws Exception
     */
    public JSONObject waitForCourse(WebDriver driver, Series series) throws Exception {
        JSONObject desiredCourse = null;
        int attempts = TimeoutUtils.getMaxJsonRetries();
        for (int i = 1; i <= attempts; i++) {
            if (i == attempts) fail("Gave up searching for the course after " + attempts + " attempts");
            try {
                logger.info("Checking for the course in the warehouse JSON");
                JSONArray courses = getCourses(driver);
                desiredCourse = searchForCourseInArray(courses, series);
                if (desiredCourse != null) {
                    logger.info("Found the course in the warehouse JSON");
                    break;
                }
            } catch (NullPointerException e) {
                logger.info("No course yet");
            }
            Thread.sleep(TimeoutUtils.getJsonRetryIntervalMs());
        }
        return desiredCourse;
    }

    /**
     * Finds the set of recordings associated with a given course
     *
     * @param driver the current browser
     * @param series the searched course
     * @return the JSON array of recordings
     * @throws Exception
     */
    private JSONArray getCourseRecordings(WebDriver driver, Series series) throws Exception {
        JSONArray recordings = null;
        try {
            JSONObject course = waitForCourse(driver, series);
            recordings = course.getJSONArray("recordings");
        } catch (JSONException e) {}
        return recordings;
    }

    /**
     * Searches for a course recording using a known YouTube video ID.  This is used to verify a JSON update has occurred
     * when the recording title is unchanged but a republish has produced a new YouTube video ID.
     *
     * @param recordings the JSON array of course recordings
     * @param recordingVideoId the YouTube video ID
     * @return the JSON object for the searched recording
     * @throws Exception
     */
    private JSONObject searchForRecordingByVideoId(JSONArray recordings, String recordingVideoId) throws Exception {
        JSONObject desiredRecording = null;
        for (int i = 0; i < recordings.length(); i++) {
            JSONObject recording = (JSONObject) recordings.get(i);
            if (recording.getString("youTubeId").equals(recordingVideoId)) {
                desiredRecording = recording;
                break;
            }
        }
        return desiredRecording;
    }

    /**
     * Searches for a course recording using a known title.  This is used to verify a JSON update has occurred when a new
     * recording is published or when an existing recording's title has been altered (but the YouTube video ID is unaltered).
     *
     * @param recordings the JSON array of course recordings
     * @param recordingTitle the recording's expected title
     * @return the JSON object for the serached recording
     * @throws Exception
     */
    private JSONObject searchForRecordingByTitle(JSONArray recordings, String recordingTitle) throws Exception {
        JSONObject desiredRecording = null;
        for (int i = 0; i < recordings.length(); i++) {
            JSONObject recording = (JSONObject) recordings.get(i);
            if (recording.getString("lecture").equals(recordingTitle)) {
                desiredRecording = recording;
                break;
            }
        }
        return desiredRecording;
    }

    /**
     * Waits for a recording with a given YouTube video ID to appear in the JSON
     *
     * @param driver the current browser
     * @param series the searched course
     * @param recordingVideoId the YouTube video ID
     * @return the JSON object for the searched recording
     * @throws Exception
     */
    public JSONObject waitForCourseRecordingVideoId(WebDriver driver, Series series, String recordingVideoId) throws Exception {
        JSONObject desiredRecording = null;
        int attempts = TimeoutUtils.getMaxJsonRetries();
        for (int i = 1; i <= attempts; i++) {
            if (i == attempts) fail("Gave up searching for the recording after " + attempts + " attempts");
            try {
                JSONArray recordings = getCourseRecordings(driver, series);
                desiredRecording = searchForRecordingByVideoId(recordings, recordingVideoId);
                if (desiredRecording != null) {
                    logger.info("Found the recording in the warehouse JSON using the YouTube video ID");
                    break;
                }
                else {
                    logger.info("The recording has not been updated yet");
                }
            } catch (Exception e) {
                logger.info("No recording yet");
            }
            Thread.sleep(TimeoutUtils.getJsonRetryIntervalMs());
        }
        return desiredRecording;
    }

    /**
     * Waits for a recording with a given title to appear in the JSON.
     *
     * @param driver the current browser
     * @param series the searched course
     * @param recordingTitle the recording's expected title
     * @return the JSON object for the searched recording
     * @throws Exception
     */
    public JSONObject waitForCourseRecordingTitle(WebDriver driver, Series series, String recordingTitle) throws Exception {
        JSONObject desiredRecording = null;
        int attempts = TimeoutUtils.getMaxJsonRetries();
        for (int i = 1; i <= attempts; i++) {
            if (i == attempts) fail("Gave up searching for the recording after " + attempts + " attempts");
            try {
                JSONArray recordings = getCourseRecordings(driver, series);
                desiredRecording = searchForRecordingByTitle(recordings, recordingTitle);
                if (desiredRecording != null) {
                    logger.info("Found the recording in the warehouse JSON using the recording title");
                    break;
                }
            } catch (Exception e) {}
            logger.info("No recording yet");
            Thread.sleep(TimeoutUtils.getJsonRetryIntervalMs());
        }
        return desiredRecording;
    }

    /**
     * Waits for a recording with a given YouTube video ID to be removed from the JSON.  Used for retraction or for republishing
     * where the video ID changes but the recording title does not.
     *
     * @param driver the current browser
     * @param series the searched course
     * @param recordingVideoId the YouTube video ID
     * @throws Exception
     */
    public void waitForRecordingRetraction(WebDriver driver, Series series, String recordingVideoId) throws Exception {
        int attempts = TimeoutUtils.getMaxJsonRetries();
        for (int i = 1; i <= attempts; i++) {
            if (i == attempts) fail("After " + attempts + " attempts, the recording is still in the JSON");
            try {
                JSONArray recordings = getCourseRecordings(driver, series);
                searchForRecordingByVideoId(recordings, recordingVideoId);
            } catch (NullPointerException e) {
                logger.info("The recording is gone");
                break;
            }
            logger.info("Still waiting for the recording to be removed from the warehouse JSON");
            Thread.sleep(TimeoutUtils.getJsonRetryIntervalMs());
        }
    }

    public boolean getCoursePublicFlag(JSONObject course) throws JSONException { return course.getBoolean("public"); }

    public String getRecordingTitle(JSONObject recording) throws JSONException { return recording.getString("lecture"); }

    public String getRecordingYouTubeVideoId(JSONObject recording) throws JSONException { return recording.getString("youTubeId"); }

    public String getRecordingStartUTC(JSONObject recording) { return recording.getString("recordingStartUTC"); }

}
