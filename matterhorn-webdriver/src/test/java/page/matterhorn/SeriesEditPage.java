package page.matterhorn;

import domain.Availability;
import domain.Series;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.Pages;
import util.AuthenticationUtils;
import util.TimeoutUtils;

import java.io.IOException;

/**
 * @author Paul Farestveit
 */
public class SeriesEditPage extends Pages {

    final Logger logger = LoggerFactory.getLogger(getClass());

    public SeriesEditPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    private String submitButtonId() { return "submitButton"; }

    public SeriesEditPage loadPage(WebDriver driver, Series series) throws IOException {
        driver.get(AuthenticationUtils.getMatterhornAdminTool() + "/admin/index.html#/series?seriesId=" + series.getSeriesId() + "&edit=true");
        return this;
    }

    /**
     * Inputs metadata about the series
     *
     * @param driver WebDriver instance
     * @param series existing series object
     * @return same page with series metadata input
     */
    public SeriesEditPage inputSeriesMetadata(WebDriver driver, boolean editTitle, Series series) throws Exception {
        waitForPageLoad(driver).until(ExpectedConditions.presenceOfElementLocated(By.id("title")));
        if (editTitle) {
            logger.info("Entering series name " + series.getSeriesName());
            clearAndType(driver, driver.findElement(By.id("title")), series.getSeriesName());
        }
        waitForElementPresent(driver.findElement(By.id("creator")), TimeoutUtils.getAjaxWaitSec());
        if (series.getInstructors() != null) {
            logger.info("Series creator is " + StringUtils.join(series.getInstructors(), ", "));
            clearAndType(driver, driver.findElement(By.id("creator")), StringUtils.join(series.getInstructors(), ", "));
        }
        if (series.getAvailability() == Availability.COPY_NON_PUBLIC && driver.findElement(By.xpath("//input[@id='accessRights'][1]")).isSelected()) {
            logger.info("The series availability is 'unlisted', but 'public' is selected.  Setting series availability to 'unlisted'");
            driver.findElement(By.xpath("//input[@id='accessRights'][2]")).click();
        }
        return this;
    }

    /**
     * Clicks the "save" button on the series editing page
     *
     * @param driver WebDriver instance
     * @return page listing all existing series
     */
    public SeriesPage clickSaveButton(WebDriver driver) throws Exception {
        logger.info("Clicking the save button");
        waitForElementAndClick(driver, driver.findElement(By.id(submitButtonId())));
        return new SeriesPage(driver);
    }

}
