package page;

import domain.*;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import util.*;

import java.io.IOException;

import static org.junit.Assert.fail;

/**
 * @author Paul Farestveit
 */

public class SalesforcePage extends Pages {

    final Logger logger = WebDriverUtils.getLogger(this.getClass());

    @FindBy(id = "username") public WebElement usernameInput;

    @FindBy(id = "password") public WebElement passwordInput;

    @FindBy(id = "Login") public WebElement loginButton;

    @FindBy(linkText = "Projects") public WebElement projectsLink;

    @FindBy(id = "phSearchInput") public WebElement searchInput;

    @FindBy(id = "phSearchButton") public WebElement searchButton;

    @FindBy(xpath = "//input[@value=' New ']") public WebElement newButton;

    @FindBy(name = "edit") public WebElement editButton;

    @FindBy(id = "p3") WebElement projectTypeSelect;

    @FindBy(xpath = "//input[@value='Continue']") public WebElement continueButton;

    // EDIT FIELDS

    @FindBy(id = "opp3") public WebElement courseNameInput;

    @FindBy(id = "opp11") WebElement stageSelect;

    @FindBy(id = "00N3000000ASkrb") WebElement approvalStatusSelect;

    @FindBy(id = "CF00N30000007YT0n") public WebElement parentProjectInput;

    @FindBy(id = "opp9") public WebElement projectEndDateInput;

    @FindBy(id = "CF00N3000000ASeuH") public WebElement instructorOneInput;

    @FindBy(id = "CF00N3000000ASkq9") public WebElement instructorTwoInput;

    @FindBy(id = "00N3000000ASeo4_unselected") WebElement unChosenDaysSelect;

    @FindBy(id = "00N3000000ASeo4_right_arrow") public WebElement addDaysButton;

    @FindBy(id = "00N3000000ASeo9") WebElement startTimeSelect;

    @FindBy(id = "00N3000000ASeoE") WebElement endTimeSelect;

    @FindBy(id = "00N3000000ASepM") WebElement recordingTypeSelect;

    @FindBy(id = "00N3000000ATJbD") WebElement availabilitySelect;

    @FindBy(id = "00N3000000ATJel") WebElement publishDelaySelect;

    @FindBy(id = "CF00N3000000ASeoO") public WebElement roomInput;

    @FindBy(id = "00N3000000ASenk") public WebElement ccnInput;

    @FindBy(id = "00N3000000ASenp") WebElement sectionSelect;

    @FindBy(id = "00N3000000ASenz") WebElement semesterTermSelect;

    @FindBy(id = "00N3000000ASenu") WebElement semesterYearSelect;

    @FindBy(id = "00N3000000ASkpp") public WebElement semesterStartDateInput;

    @FindBy(id = "00N3000000ASkpu") public WebElement semesterEndDateInput;

    @FindBy(id = "00N3000000ATcD9") public WebElement courseTitleInput;

    @FindBy(name = "save") public WebElement saveButton;

    @FindBy(id = "opp4") public WebElement departmentInput;

    // DISPLAY FIELDS

    @FindBy(id = "opp3_ileinner") public WebElement courseName;

    @FindBy(id = "RecordType_ileinner") public WebElement projectRecordType;

    @FindBy(id = "opp11_ileinner") public WebElement stage;

    @FindBy(id = "00N3000000ASkrb_ileinner") public WebElement approvalStatus;

    @FindBy(id = "00N3000000ASkrW_chkbox") public WebElement recordingsScheduledFlag;

    @FindBy(id = "00N3000000ASesz_ileinner") public WebElement courseOfferingId;

    @FindBy(id = "opp9_ileinner") public WebElement projectEndDate;

    @FindBy(id = "00N3000000ASiv7_chkbox") public WebElement instructorOneApproval;

    @FindBy(id = "CF00N3000000ASkq9_ileinner") public WebElement instructorTwo;

    @FindBy(id = "00N3000000ASkqE_chkbox") public WebElement instructorTwoApproval;

    @FindBy(id = "00N3000000ASeo4_ileinner") public WebElement scheduleDays;

    @FindBy(id = "00N3000000ASeo9_ileinner") public WebElement startTime;

    @FindBy(id = "00N3000000ASeoE_ileinner") public WebElement endTime;

    @FindBy(id = "00N3000000ATJel_ileinner") public WebElement publishDelayDays;

    @FindBy(id = "00N3000000ASenk_ileinner") public WebElement ccn;

    @FindBy(id = "00N3000000ASkq4_ileinner") public WebElement semester;

    @FindBy(id = "00N3000000ATcD9_ilecell") public WebElement courseTitle;

    public SalesforcePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public SalesforcePage loadPage(WebDriver driver) {
        driver.get("https://test.salesforce.com");
        return this;
    }

    public SalesforcePage login(WebDriver driver) throws Exception {
        logger.info("Logging into Salesforce");
        loadPage(driver);
        clearAndType(driver, usernameInput, AuthenticationUtils.getSalesforceUsername());
        clearAndType(driver, passwordInput, AuthenticationUtils.getSalesforcePassword());
        loginButton.click();
        return this;
    }

    public SalesforcePage clickProjectsLink(WebDriver driver) {
        logger.info("Clicking Projects link");
        if (!isElementPresent(projectsLink)) {
            String location =  driver.getCurrentUrl();
            driver.get(location);
        }
        projectsLink.click();
        return this;
    }

    public SalesforcePage searchForProject(WebDriver driver, Series series, int totalAttempts) throws Exception {
        logger.info("Searching for series");
        for (int attempts = 0; attempts <= totalAttempts; attempts++) {
            if (attempts == totalAttempts) fail("Timed out waiting");
            try {
                clearAndType(driver, searchInput, series.getSeriesTestId());
                searchButton.click();
                if (isLinkPresentByText(driver, series.getSeriesName())) {
                    break;
                }
            } catch (StaleElementReferenceException e) {
                continue;
            }
            Thread.sleep(1000);
        }
        return this;
    }

    public SalesforcePage clickProjectsLink() {
        logger.debug("Clicking Projects link");
        projectsLink.click();
        return this;
    }

    public SalesforcePage retrieveProject(WebDriver driver, Series series) throws Exception {
        clickProjectsLink(driver);
        searchForProject(driver, series, 5);
        clickLinkToProject(driver, series);
        return this;
    }

    public SalesforcePage startNewCourseSemesterProject(WebDriver driver) throws Exception {
        logger.debug("Clicking new button");
        newButton.click();
        waitForElementAndSelect(driver, projectTypeSelect, "Course Capture Semester");
        continueButton.click();
        return this;
    }

    public SalesforcePage startNewCourseProject(WebDriver driver) throws Exception {
        logger.debug("Clicking new button");
        newButton.click();
        waitForElementAndSelect(driver, projectTypeSelect, "Course Capture");
        continueButton.click();
        return this;
    }

    public SalesforcePage clickEditButton(WebDriver driver, Series series) throws IOException {
        logger.debug("Clicking edit button");
        editButton.click();
        WebDriverWait waitForEditPage = new WebDriverWait(driver, TimeoutUtils.getPageLoadWaitSec());
        waitForEditPage.until(ExpectedConditions.textToBePresentInElementValue(By.id("opp3"), series.getSeriesName()));
        return this;
    }

    public SalesforcePage inputNewCourseSemesterProjectMetadata(WebDriver driver, SalesforceSemesterProject salesforceSemesterProject) throws Exception {
        logger.info("Entering semester metadata for Salesforce semester project.");
        logger.debug("Semester starts " + DateFormatUtils.getDateMDYYYY(salesforceSemesterProject.getSemesterStartDate()));
        clearAndType(driver, semesterStartDateInput, DateFormatUtils.getDateMDYYYY(salesforceSemesterProject.getSemesterStartDate()));

        logger.debug("Semester is " + salesforceSemesterProject.getSemesterTerm() + " " + salesforceSemesterProject.getSemesterYear());
        waitForElementAndSelect(driver, semesterYearSelect, salesforceSemesterProject.getSemesterYear());
        waitForElementAndSelect(driver, semesterTermSelect, salesforceSemesterProject.getSemesterTerm());

        logger.debug("Semester course project name is " + salesforceSemesterProject.getSemesterProjectName());
        clearAndType(driver, courseNameInput, salesforceSemesterProject.getSemesterProjectName());

        logger.debug("Project stage is 'Cancelled'");
        waitForElementAndSelect(driver, stageSelect, "Cancelled");

        logger.debug("Project end date is " + DateFormatUtils.getDateMDYYYY(salesforceSemesterProject.getSemesterEndDate()));
        clearAndType(driver, projectEndDateInput, DateFormatUtils.getDateMDYYYY(salesforceSemesterProject.getSemesterEndDate()));

        logger.debug("Semester ends " + DateFormatUtils.getDateMDYYYY(salesforceSemesterProject.getSemesterEndDate()));
        clearAndType(driver, semesterEndDateInput, DateFormatUtils.getDateMDYYYY(salesforceSemesterProject.getSemesterEndDate()));

        saveButton.click();
        waitForPageLoad(driver).until(ExpectedConditions.textToBePresentInElement(courseName, salesforceSemesterProject.getSemesterProjectName()));
        return this;
    }

    public SalesforcePage inputNewCourseProjectMetadata(WebDriver driver, Series series, SalesforceSemesterProject
            salesforceSemesterProject) throws Exception {
        logger.info("Entering course metadata for Salesforce course project.");
        logger.debug("Course project name is " + series.getSeriesName());
        clearAndType(driver, courseNameInput, series.getSeriesName());

        logger.debug("Project stage is 'Waiting for client\'s response'");
        waitForElementAndSelect(driver, stageSelect, "Waiting for client's response");

        logger.debug("Approval status is 'No Approvals'");
        waitForElementAndSelect(driver, approvalStatusSelect, "No Approvals");

        logger.debug("Project end date is " + DateFormatUtils.getDateMDYYYY(series.getSeriesEndDate()));
        clearAndType(driver, projectEndDateInput, DateFormatUtils.getDateMDYYYY(series.getSeriesEndDate()));

        logger.debug("Parent project is " + salesforceSemesterProject.getSemesterProjectName());
        clearAndType(driver, parentProjectInput, salesforceSemesterProject.getSemesterProjectName());

        logger.debug("Instructor One is " + series.getInstructors()[0].getInstructorName());
        clearAndType(driver, instructorOneInput, series.getInstructors()[0].getInstructorName());
        if (series.getInstructors().length == 2) {
            logger.debug("Instructor Two is " + series.getInstructors()[1].getInstructorName());
            // The following line is a hack to get around the "Element is not clickable at point" error in Chrome
            roomInput.click();
            Thread.sleep(2000);
            clearAndType(driver, instructorTwoInput, series.getInstructors()[1].getInstructorName());
        }

        logger.debug("Recording day is " + DateFormatUtils.getDayOfWeek(RecordingUtils.getFirstRecordingDate(series)));
        waitForElementAndSelect(driver, unChosenDaysSelect, DateFormatUtils.getDayOfWeek(RecordingUtils.getFirstRecordingDate(series)));
        waitForElementAndClick(driver, addDaysButton);

        logger.info("Recording start time is " + SalesforceUtils.getRecordingStartTime());
        waitForElementAndSelect(driver, startTimeSelect, SalesforceUtils.getRecordingStartTime());

        logger.info("Recording end time is " + SalesforceUtils.getRecordingEndTime());
        waitForElementAndSelect(driver, endTimeSelect, SalesforceUtils.getRecordingEndTime());

        logger.debug("Room is capture agent " + series.getCaptureAgent().getRoom());
        clearAndType(driver, roomInput, series.getCaptureAgent().getRoom());
        waitForElementAndSelect(driver, recordingTypeSelect, "--None--");
        waitForElementAndSelect(driver, availabilitySelect, "--None--");
        waitForElementAndSelect(driver, publishDelaySelect, "0");

        logger.debug("CCN is " + series.getCcn());
        clearAndType(driver, ccnInput, series.getCcn());
        waitForElementAndSelect(driver, sectionSelect, "001");

        logger.debug("Course title is " + series.getCourseTitle());
        clearAndType(driver, courseTitleInput, series.getCourseTitle());
        clearAndType(driver, departmentInput, "ETS");

        saveButton.click();
        waitForPageLoad(driver).until(ExpectedConditions.textToBePresentInElement(courseName, series.getSeriesName()));
        return this;
    }

    public SalesforcePage saveProject() {
        logger.info("Saving project");
        saveButton.click();
        return this;
    }

    public SalesforcePage clickLinkToProject(WebDriver driver, Series series) {
        logger.debug("Clicking link to open project");
        driver.findElement(By.linkText(series.getSeriesName())).click();
        return this;
    }

    public SalesforcePage setUpSalesforceProjects(WebDriver driver, SalesforceSemesterProject semesterProject, Series series) throws Exception {
        login(driver);
        clickProjectsLink(driver);
        startNewCourseSemesterProject(driver);
        inputNewCourseSemesterProjectMetadata(driver, semesterProject);
        clickProjectsLink();
        startNewCourseProject(driver);
        inputNewCourseProjectMetadata(driver, series, semesterProject);
        return this;
    }

    public void verifyProjectStaticData(StringBuffer verificationErrors, WebDriver driver, Series series) throws IOException {
        TestUtils.verifyEquals(verificationErrors, series.getSeriesName(), courseName.getText());
        TestUtils.verifyEquals(verificationErrors, "Course Capture [Change]", projectRecordType.getText());
        TestUtils.verifyEquals(verificationErrors, SalesforceUtils.getCourseOfferingId(series.getCcn()), courseOfferingId.getText());
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getDateMDYYYY(series.getSeriesEndDate()), projectEndDate.getText());
        TestUtils.verifyEquals(verificationErrors, "06:30p", startTime.getText());
        TestUtils.verifyEquals(verificationErrors, "07:00p", endTime.getText());
        TestUtils.verifyTrue(verificationErrors, isLinkPresentByText(driver, series.getCaptureAgent().getRoom()));
        TestUtils.verifyEquals(verificationErrors, series.getPublishDelay().getDelayDaysString(), publishDelayDays.getText());
        TestUtils.verifyEquals(verificationErrors, series.getCcn(), ccn.getText());
        TestUtils.verifyEquals(verificationErrors, series.getCourseTitle(), courseTitle.getText());
    }

}
