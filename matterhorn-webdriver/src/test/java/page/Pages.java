package page;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.DateFormatUtils;
import util.TimeoutUtils;

import java.io.IOException;
import java.util.Date;

import static org.junit.Assert.fail;

/**
 * @author Paul Farestveit
 */
public class Pages {

    public Pages(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public WebDriverWait waitForPageLoad(WebDriver driver) throws IOException {
        return new WebDriverWait(driver, TimeoutUtils.getPageLoadWaitSec());
    }

    public WebDriverWait waitForPageUpdate(WebDriver driver) throws IOException {
        return new WebDriverWait(driver, TimeoutUtils.getAjaxWaitSec());
    }

    public WebDriverWait waitForMetadataUpdate(WebDriver driver) throws IOException {
        return new WebDriverWait(driver, TimeoutUtils.getMetadataEditWaitSec());
    }

    public WebDriverWait waitForFileUpload(WebDriver driver) throws IOException {
        return new WebDriverWait(driver, TimeoutUtils.getFileUploadTimeoutSec());
    }

    /**
     * Checks for an element's existence by looking for its size
     *
     * @param webElement the element in question
     * @return true or false
     */
    public boolean isElementPresent(WebElement webElement) {
        try {
            webElement.getSize();
            return true;
        } catch (NoSuchElementException ignored) {
            return false;
        }
    }

    /**
     * Uses JavaScript to scroll to an element
     *
     * @param driver     the browser driver
     * @param webElement the Web Element that should contain text
     */
    public void scrollToElement(WebDriver driver, WebElement webElement) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", webElement);
    }

    /**
     * Indicates whether or not a link is present based on the link's text.
     *
     * @param driver   WebDriver instance
     * @param linkText the text of the link that should be present
     * @return true if the link exists, false if not
     */
    public boolean isLinkPresentByText(WebDriver driver, String linkText) {
        try {
            driver.findElement(By.linkText(linkText));
            logger.info("Found link with text " + linkText);
            return true;
        } catch (NoSuchElementException ignored) {
            logger.info("Can't find a link with text " + linkText);
            return false;
        }
    }

    public void waitForElementPresent(WebElement webElement, int wait) throws InterruptedException {
        for (int second = 0; second <= wait; second++) {
            Thread.sleep(1000);
            if (second == wait)
                fail("Timed out after waiting for an element to appear");
            try {
                webElement.getSize();
                break;
            } catch (Exception ignored) {
            }
        }
    }

    public void waitForTitleNotContains(WebDriver driver, String title, int wait) throws InterruptedException {
        for (int second = 0; second <= wait; second++) {
            Thread.sleep(1000);
            if (second == wait)
                fail("Timed out waiting for an element to go away");
            if (!driver.getTitle().contains(title))
                break;
        }
    }

    /**
     * Waits the designated seconds for text to appear in the specified Web Element
     *
     * @param webElement the Web Element that will contain the text
     * @param text       the text that will appear in the Web Element
     * @param wait       the time to wait for the text to appear
     * @throws InterruptedException
     */
    public void waitForText(WebElement webElement, String text, int wait) throws InterruptedException {
        logger.info("Waiting for \"" + text + "\" to appear ");
        for (int second = 0; second <= wait; second++) {
            Thread.sleep(1000);
            if (second == wait)
                fail("Timed out after waiting for " + wait + " seconds.");
            try {
                if (webElement.getText().contains(text)) {
                    logger.debug("Found! at " + DateFormatUtils.getHourMinute(new Date()));
                    break;
                }
            } catch (StaleElementReferenceException ignored) {
            }
        }
    }

    /**
     * Waits the designated seconds for a link to appear with text matching the designated text
     *
     * @param webElement the link element that will contain the text
     * @param linkText   the text that will appear in the link
     * @param wait       the time to wait for the text to appear
     * @throws InterruptedException
     */
    public void waitForLink(WebElement webElement, String linkText, int wait) throws InterruptedException {
        logger.info("Waiting for link with text '" + linkText + "' to appear on the page");
        for (int second = 0; second <= wait; second++) {
            Thread.sleep(1000);
            if (second == wait) fail("Timed out waiting for link to appear");
            try {
                if (webElement.getText().contains(linkText)) {
                    logger.debug("Found!");
                    break;
                }
            } catch (NoSuchElementException ignored) {
            }
        }
    }

    /**
     * Waits for an element to become visible
     *
     * @param driver  the current browser
     * @param element the element that should be visible
     * @param timeout the time to wait for the element to become visible
     * @throws Exception
     */
    public void waitForVisible(WebDriver driver, WebElement element, int timeout) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    /**
     * Waits the designated seconds for an element to appear with value attribute matching the expectation
     *
     * @param webElement the element that will contain the value
     * @param value      the value attribute
     * @param wait       the time to wait for  the element to appear with the designated value
     * @throws InterruptedException
     */
    public void waitForValue(WebElement webElement, String value, int wait) throws InterruptedException {
        logger.info("Looking for value of " + value);
        for (int second = 0; second <= wait; second++) {
            if (second == wait)
                fail("Timed out before element value matched expectation.  Found " + webElement.getAttribute("value") + " instead.");
            try {
                if (webElement.getAttribute("value").matches(value)) {
                    logger.debug("Found!");
                    break;
                }
            } catch (StaleElementReferenceException ignored) {
            }
            Thread.sleep(1000);
        }
    }

    /**
     * Waits for a given value to be selected on the YouTube privacy select.
     *
     * @param driver        the current browser
     * @param selectElement the Web Element containing the select menu
     * @param select        the Select derived from the Web Element
     * @param optionValue   the expected option
     * @throws Exception
     */
    public void waitForYouTubePrivacySelectValue(WebDriver driver, WebElement selectElement, Select select, String optionValue) throws Exception {
        logger.info("Waiting for selected option value " + optionValue);
        int wait = TimeoutUtils.getPageLoadWaitSec();
        for (int second = 0; second <= wait; second++) {
            if (second == wait) {
                logger.error("Timed out before right option was selected.  This was selected instead " + select.getFirstSelectedOption().getText());
            } else {
                Thread.sleep(1000);
                try {
                    WebDriverWait waitForSelect = new WebDriverWait(driver, TimeoutUtils.getAjaxWaitMs());
                    waitForSelect.until(ExpectedConditions.presenceOfElementLocated(By.name("privacy")));
                    selectElement.click();
                    if ((select.getFirstSelectedOption().getText().toLowerCase()).contains(optionValue.toLowerCase())) {
                        logger.debug("Found!");
                        break;
                    }
                } catch (Exception ignored) {
                    logger.info("Nope, not there.  I'm seeing this selected: " + select.getFirstSelectedOption().getText());
                }
            }
        }
    }

    /**
     * A shortcut to populate an input with text: click the element, clear the contents, and send the text
     *
     * @param field the web element in which to enter text
     * @param text  the text to enter
     */

    public void clearAndType(WebDriver driver, WebElement field, String text) throws Exception {
        waitForElementAndClick(driver, field);
        field.clear();
        field.sendKeys(text);
    }

    /**
     * Clicks an element once it is both present and clickable, falling back on JavaScript to perform the click if
     * click() fails.
     *
     * @param driver  the current browser
     * @param element the element to be clicked
     * @throws Exception
     */
    public void waitForElementAndClick(WebDriver driver, WebElement element) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver, TimeoutUtils.getPageLoadWaitSec());
        logger.debug("Waiting for element to be present");
        waitForElementPresent(element, TimeoutUtils.getPageLoadWaitSec());
        logger.debug("Element is present; waiting for it to be clickable");
        wait.until(ExpectedConditions.elementToBeClickable(element));
        scrollToElement(driver, element);
        logger.debug("Clicking element");
        try {
            element.click();
        } catch (WebDriverException error) {
            logger.error(error.getMessage());
            logger.info("Click failed, trying JavaScript");
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].click();", element);
        }
    }

    /**
     * Waits for an element with the specified XPath to appear and then clicks it.  The purpose of using an XPath String
     * instead of a WebElement is to avoid stale element references due to Matterhorn's constant DOM updates.  Between
     * the WebElement becoming present and then being clicked, the WebElement can vanish.
     *
     * @param driver the current browser
     * @param xpath  the XPath of the element that will be clicked
     * @throws Exception
     */
    public void waitForElementXpathAndClick(WebDriver driver, String xpath) throws Exception {
        int maxAttempts = 3;
        for (int i = 0; i <= maxAttempts; i++) {
            if (i == maxAttempts) fail("Unable to click a link using XPath '" + xpath + "'");
            try {
                WebDriverWait waitForElement = new WebDriverWait(driver, TimeoutUtils.getAjaxWaitSec());
                waitForElement.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
                driver.findElement(By.xpath(xpath)).click();
                // Swallow any alert that might pop up
                swallowAlert(driver);
                break;
            } catch (StaleElementReferenceException ignored) {
                logger.info("StaleElementReferenceException caught");
            }
            Thread.sleep(1000);
        }
    }

    /**
     * Selects an option on a dropdown once the dropdown and the option are visible
     *
     * @param driver        the current browser
     * @param selectElement the dropdown
     * @param option        the text of the option
     * @throws java.io.IOException
     */
    public void waitForElementAndSelect(WebDriver driver, WebElement selectElement, String option) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver, TimeoutUtils.getPageLoadWaitSec());
        waitForElementPresent(selectElement, TimeoutUtils.getPageLoadWaitSec());
        wait.until(ExpectedConditions.elementToBeClickable(selectElement));
        scrollToElement(driver, selectElement);
        Select select = new Select(selectElement);
        select.selectByVisibleText(option);
    }

    /**
     * Accepts an alert if one is present
     *
     * @param driver the current browser
     */
    public void swallowAlert(WebDriver driver) throws IOException {
        try {
            WebDriverWait wait = new WebDriverWait(driver, TimeoutUtils.getAjaxWaitSec());
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            alert.accept();
        } catch (Exception ignored) {
        }
    }

    /**
     * Waits for and switches focus to an iframe
     *
     * @param driver        the current browser
     * @param iframeElement the iframe element
     * @throws Exception
     */
    public void switchToIframe(WebDriver driver, WebElement iframeElement) throws Exception {
        waitForElementPresent(iframeElement, TimeoutUtils.getAjaxWaitSec());
        driver.switchTo().frame(iframeElement);

    }

}
