package page;

import domain.Recording;
import domain.Series;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.*;

import java.io.IOException;

import static org.junit.Assert.fail;

/**
 * @author Paul Farestveit
 */

public class YouTubePage extends Pages {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @FindBy(xpath = "//a[contains(.,'Sign in')]") public WebElement signInButton;

    @FindBy(id = "identifierId") public WebElement emailInput;

    @FindBy(id = "identifierNext") public WebElement nextButton;

    @FindBy(xpath = "//div[@role='button'][contains(.,'Continue')]") public WebElement continueButton;

    @FindBy(id = "avatar-btn") public WebElement accountPicker;

    @FindBy(name = "title") public WebElement videoTitle;

    @FindBy(xpath = "//textarea[@name='description']") public WebElement videoDesc;

    @FindBy(className = "metadata-privacy-input") public WebElement privacySelect;

    @FindBy(className = "metadata-share-text") public WebElement sharing;

    public YouTubePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Select getPrivacySelect() {
        return new Select(privacySelect);
    }

    public YouTubePage loadVideoManagerPage(WebDriver driver) throws IOException {
        logger.info("Loading YouTube video manager page");
        driver.get("https://www.youtube.com/my_videos?o=U");
        swallowAlert(driver);
        return this;
    }

    public YouTubePage logInViaSpa(WebDriver driver, CalNetAuthPage calNetAuthPage) throws Exception {
        logger.info("Logging in to YouTube");
        driver.get("https://www.youtube.com");
        waitForElementAndClick(driver, signInButton);
        clearAndType(driver, emailInput, AuthenticationUtils.getYouTubeUsername());
        waitForElementAndClick(driver, nextButton);
        Thread.sleep(5000);
        calNetAuthPage.clearAndType(driver, calNetAuthPage.username, AuthenticationUtils.getCalnetSpaUsername());
        logger.debug("Waiting for CAS page to go away");
        waitForTitleNotContains(driver, "Central Authentication Service", 240000);
        logger.debug("Waiting for continue button");
        Thread.sleep(15000);
        if (isElementPresent(continueButton)) {
            waitForElementAndClick(driver, continueButton);
        }
        logger.debug("Waiting for account picker");
        waitForPageLoad(driver).until(ExpectedConditions.visibilityOf(accountPicker));
        return this;
    }

    public YouTubePage loadVideo(WebDriver driver, Recording recording) throws Exception {
        String truncatedTitle = StringUtils.substring(recording.getTitle(), 0, 99);
        logger.info("Waiting for YouTube to process the new upload with video title '" + truncatedTitle + "'");
        for (int second = 0; second <= TimeoutUtils.getPageLoadWaitSec(); second++) {
            if (second == TimeoutUtils.getPageLoadWaitSec())
                fail("Timed out before YouTube finished processing the new upload");
            try {
                loadVideoManagerPage(driver);
                if (isLinkPresentByText(driver, truncatedTitle)) {
                    logger.info("Found YouTube video for " + truncatedTitle);
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
            Thread.sleep(1000);
        }
        logger.info("Clicking link to YouTube video");
        driver.findElement(By.linkText(truncatedTitle)).click();
        waitForPageLoad(driver).until(ExpectedConditions.visibilityOf(videoTitle));
        return this;
    }

    public YouTubePage loadVideoEditPage(WebDriver driver, String videoId) throws Exception {
        logger.info("Checking the YouTube video sharing settings");
        // Wait a few seconds before checking in case YouTube hasn't immediately updated the visible sharing setting
        Thread.sleep(TimeoutUtils.getAjaxWaitMs());
        driver.get("https://www.youtube.com/edit?o=U&video_id=" + videoId);
        waitForElementPresent(this.sharing, TimeoutUtils.getPageLoadWaitSec());
        return this;
    }

    public String getYouTubeVideoId(WebDriver driver) {
        String url = driver.getCurrentUrl();
        String videoId = url.replace("https://www.youtube.com/edit?o=U&video_id=", "");
        logger.info("Video ID is " + videoId);
        return videoId;
    }

    public void triggerAndAcceptYouTubeAlert(WebDriver driver) throws Exception {
        logger.info("Navigating to Google to trigger and accept the 'are you sure?' alert when leaving video edit page");
        driver.get("https://www.google.com");
        swallowAlert(driver);
        Thread.sleep(TimeoutUtils.getAjaxWaitMs());
    }

    public String verifyYouTubeVideo(WebDriver driver, Series series, Recording recording, StringBuffer stringBuffer) throws Exception {
        String youTubeVideoId = "";
        try {
            loadVideo(driver, recording);
            youTubeVideoId = getYouTubeVideoId(driver);
            waitForVisible(driver, videoTitle, TimeoutUtils.getAjaxWaitSec());
            TestUtils.verifyEquals(stringBuffer, recording.getTitle(), videoTitle.getAttribute("value"));
            TestUtils.verifyEquals(stringBuffer, series.getSeriesName() + "\n" + series.getCourseTitle() + " - " + StringUtils.join(recording.getInstructors(), ", ")
                    + "\n" + series.getAvailability().getLicense().getLicenseDescription(), videoDesc.getText());
            loadVideoEditPage(driver, youTubeVideoId);
            waitForYouTubePrivacySelectValue(driver, privacySelect, getPrivacySelect(),
                    series.getAvailability().getYouTubeAvailability());
            triggerAndAcceptYouTubeAlert(driver);
        } catch (Exception e) {
            logger.error("Error checking YouTube video");
            e.printStackTrace();
        }
        return youTubeVideoId;
    }

    public void verifyYouTubeSharing(WebDriver driver, String videoId, StringBuffer stringBuffer) throws Exception {
        loadVideoEditPage(driver, videoId);
        TestUtils.verifyEquals(stringBuffer, "Shared with berkeley.edu", sharing.getText());
    }

}
