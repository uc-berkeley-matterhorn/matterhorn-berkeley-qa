package util;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.safari.SafariDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * author Paul Farestveit
 *
 * Util for defining configuration of browser drivers and logger
 */

public class WebDriverUtils {

    private static Properties WEBDRIVER_CONFIG;

    static {
        try {
            final InputStream inputStream = WebDriverUtils.class.getClassLoader().getResourceAsStream("selenium2-webdriver.properties");
            WEBDRIVER_CONFIG = new Properties();
            WEBDRIVER_CONFIG.load(inputStream);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static WebDriver getWebDriver() throws Exception {
        WebDriver WEB_DRIVER;
        final String WEB_DRIVERType = WEBDRIVER_CONFIG.getProperty("web.driver");
        if (WEB_DRIVERType.equals("FIREFOX")) {
            final FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.addPreference("devtools.jsonview.enabled", false);
            WEB_DRIVER = new FirefoxDriver(firefoxOptions);
        }
        else if (WEB_DRIVERType.equals("CHROME")) {
            System.setProperty("webdriver.chrome.driver", (System.getenv().get("HOME") + "/webdriver-executables/chromedriver"));
            // Explicitly enable Flash so that the review/trim UI is usable
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--disable-features=EnableEphemeralFlashPermission");
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("profile.default_content_setting_values.plugins", 1);
            prefs.put("profile.content_settings.plugin_whitelist.adobe-flash-player", 1);
            prefs.put("profile.content_settings.exceptions.plugins.*,*.per_resource.adobe-flash-player", 1);
            prefs.put("profile.content_settings.exceptions.plugins.*,*.setting", 1);
            prefs.put("PluginsAllowedForUrls", AuthenticationUtils.getMatterhornAdminTool());
            options.setExperimentalOption("prefs", prefs);
            WEB_DRIVER = new ChromeDriver(options);
        }
        else if (WEB_DRIVERType.equals("SAFARI")) {
            WEB_DRIVER = new SafariDriver();
        }
        else {
            throw new UnsupportedOperationException("whatchoo talkin' 'bout willis");
        }
        WEB_DRIVER.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        return WEB_DRIVER;
    }

    public static Logger getLogger(Class className) { return LoggerFactory.getLogger(className); }

}
