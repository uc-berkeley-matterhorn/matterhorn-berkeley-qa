package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Paul Farestveit
 */
public class TimeoutUtils {

    private static Properties WEBDRIVER_CONFIG;

    static {
        try {
            final InputStream inputStream = WebDriverUtils.class.getClassLoader().getResourceAsStream("selenium2-webdriver.properties");
            WEBDRIVER_CONFIG = new Properties();
            WEBDRIVER_CONFIG.load(inputStream);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static long getPageLoadWaitMs() throws IOException {
        return Integer.valueOf(WEBDRIVER_CONFIG.getProperty("timeout.page.load"));
    }

    public static int getPageLoadWaitSec() throws IOException {
        return (int) getPageLoadWaitMs()/1000;
    }

    public static long getAjaxWaitMs() throws IOException {
        return Integer.valueOf(WEBDRIVER_CONFIG.getProperty("timeout.ajax.event"));
    }

    public static int getAjaxWaitSec() throws IOException {
        return (int) getAjaxWaitMs()/1000;
    }

    public static long getCaptureSchedWaitMs() throws IOException {
        return Integer.valueOf(WEBDRIVER_CONFIG.getProperty("timeout.capture.scheduling"));
    }

    public static int getCaptureSchedWaitSec() throws IOException {
        return (int) getCaptureSchedWaitMs()/1000;
    }

    public static long getMetadataEditWaitMs() throws IOException {
        return Integer.valueOf(WEBDRIVER_CONFIG.getProperty("timeout.metadata.edit"));
    }

    public static int getMetadataEditWaitSec() throws IOException {
        return (int) getMetadataEditWaitMs()/1000;
    }

    public static int getMaxJsonRetries() throws IOException {
        int maxWait = Integer.valueOf(WEBDRIVER_CONFIG.getProperty("warehouse.json.check.maximum"));
        int interval = Integer.valueOf(WEBDRIVER_CONFIG.getProperty("warehouse.json.check.interval"));
        return (maxWait/interval);
    }

    public static int getJsonRetryIntervalMs() throws IOException {
        return Integer.valueOf(WEBDRIVER_CONFIG.getProperty("warehouse.json.check.interval"))*1000;
    }

    public static int getMaxWorkflowRetries() throws IOException {
        int maxWait = Integer.valueOf(WEBDRIVER_CONFIG.getProperty("workflow.check.maximum"));
        int interval = Integer.valueOf(WEBDRIVER_CONFIG.getProperty("workflow.check.interval"));
        return (maxWait/interval);
    }

    public static int getWorkflowRetryIntervalMs() throws IOException {
        return (Integer.valueOf(WEBDRIVER_CONFIG.getProperty("workflow.check.interval"))*1000);
    }

    public static long getSalesforceWaitMs() throws IOException {
        return Integer.valueOf(WEBDRIVER_CONFIG.getProperty("timeout.salesforce"));
    }

    public static long getFileUploadTimeoutMs() throws IOException {
        return Integer.valueOf(WEBDRIVER_CONFIG.getProperty("timeout.file.upload"));
    }

    public static int getFileUploadTimeoutSec() throws IOException {
        return (int) getFileUploadTimeoutMs()/1000;
    }
}
