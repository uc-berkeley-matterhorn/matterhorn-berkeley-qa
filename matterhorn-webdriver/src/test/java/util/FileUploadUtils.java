package util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Paul Farestveit
 */

public class FileUploadUtils {

    private static Properties PROPERTIES;

    static {
        try {
            final InputStream inputStream = new FileInputStream(System.getenv().get("HOME") + "/.matterhorn-webdriver-config/authentication.properties");
            PROPERTIES = new Properties();
            PROPERTIES.load(inputStream);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getSampleFileBaseDir() throws IOException { return PROPERTIES.getProperty("source.file.dir"); }

    /**
     * Enters a filepath in a file upload input.  For uploading existing test recordings, the base directory is the resources
     * dir that contains test files
     *
     * @param fileInputField the input element for the file path in the UI
     * @param relativePath the relative path (after the base directory) that points to the file being uploaded
     * @throws Exception
     */
    public static void uploadFile(WebDriver driver, WebElement fileInputField, String relativePath) throws Exception {
        final String path = new File(getSampleFileBaseDir(), relativePath).getAbsolutePath();
        WebDriverWait wait = new WebDriverWait(driver, TimeoutUtils.getPageLoadWaitSec());
        wait.until(ExpectedConditions.visibilityOf(fileInputField));
        fileInputField.sendKeys(path);
    }

    public static String getAudioUploadFileName() throws IOException { return PROPERTIES.getProperty("source.file.audio"); }

    public static String getScreenUploadFileName() throws IOException { return PROPERTIES.getProperty("source.file.screen"); }

    public static String getVideoUploadFileName() throws IOException { return PROPERTIES.getProperty("source.file.video"); }

}
