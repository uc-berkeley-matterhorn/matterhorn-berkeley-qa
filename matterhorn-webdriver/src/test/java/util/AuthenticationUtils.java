package util;

import warehouse.db.Credentials;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author Paul Farestveit
 */
public class AuthenticationUtils {

    private static Properties CREDENTIALS;

    static {
        try {
            final FileInputStream inputStream = new FileInputStream(System.getenv().get("HOME") + "/.matterhorn-webdriver-config/authentication.properties");
            CREDENTIALS = new Properties();
            CREDENTIALS.load(inputStream);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getCalnetAdminUsername() throws IOException { return CREDENTIALS.getProperty("username"); }

    public static String getCalnetAdminPassword() throws IOException { return CREDENTIALS.getProperty("password"); }

    public static String getCalnetSpaUsername() throws IOException { return CREDENTIALS.getProperty("spa.account.username"); }

    public static String getYouTubeUsername() throws IOException { return CREDENTIALS.getProperty("youtube.username"); }

    public static String getCalnetTestUserPassword() throws IOException { return CREDENTIALS.getProperty("passwordTestUser"); }

    public static String getSalesforceUsername() throws IOException { return CREDENTIALS.getProperty("usernameSalesforce"); }

    public static String getSalesforcePassword() throws IOException { return CREDENTIALS.getProperty("passwordSalesforce"); }

    public static String getWebcastJsonUsername() throws IOException { return CREDENTIALS.getProperty("usernameJsonFeed"); }

    public static String getWebcastJsonPassword() throws IOException { return CREDENTIALS.getProperty("passwordJsonFeed"); }

    public static Credentials getWarehouseDatabaseCredentials() {
        final String jdbcURL = CREDENTIALS.getProperty("warehouse.db.jdbcURL");
        final String username = CREDENTIALS.getProperty("warehouse.db.username");
        final String password = CREDENTIALS.getProperty("warehouse.db.password");
        return new Credentials(jdbcURL, username, password);
    }

    public static String getMatterhornAdminTool() throws IOException { return  "http://" + AuthenticationUtils.CREDENTIALS.getProperty("test.environment"); }

    public static String getWebcastWarehouseJson() throws IOException {
        return "https://" + getWebcastJsonUsername() + ":" + getWebcastJsonPassword() +
                "@" + AuthenticationUtils.CREDENTIALS.getProperty("test.environment") + "/warehouse/webcast.json";
    }
}
