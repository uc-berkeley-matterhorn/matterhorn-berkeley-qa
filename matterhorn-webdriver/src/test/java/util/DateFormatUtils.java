package util;

import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author John Crossman
 */

public class DateFormatUtils {

    public static String getDateYYYY_MM_DD(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public static String getDateMDYYYY(Date date) {
        return new SimpleDateFormat("M/d/yyyy").format(date);
    }

    public static String getHour(Date hour) {
        return new SimpleDateFormat("HH").format(hour);
    }

    public static String getMinute(Date minute) {
        return new SimpleDateFormat("mm").format(minute);
    }

    public static String getHourMinute(Date date) {
        return new SimpleDateFormat("HH:mm").format(date);
    }

    public static String getDayOfWeek(Date date) {
        return new SimpleDateFormat("EEEE").format(date);
    }

    public static String getDayAndDate(Date date) {
        return new SimpleDateFormat("EEE, MMM dd yyyy").format(date);
    }

    public static String getJsonRecordingDate(Date date) {
        Date nearestMinute = DateUtils.truncate(date, Calendar.MINUTE);
        return org.apache.commons.lang3.time.DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.format(nearestMinute);
    }

}
