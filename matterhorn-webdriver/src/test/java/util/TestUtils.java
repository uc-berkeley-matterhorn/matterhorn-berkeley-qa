package util;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * @author Paul Farestveit
 */
public class TestUtils {

    final static Logger logger = LoggerFactory.getLogger(TestUtils.class);

    /**
     * Creates an output file to contain verification errors that might result from a test run
     *
     * @param testId the unique ID number assigned to the test run
     * @return the output file
     * @throws IOException
     */
    static File createTestOutputFile(String testId) throws IOException {
        File resultsDir = new File(System.getenv().get("HOME") + "/MatterhornTestResults");
        if (!resultsDir.exists()) {
            resultsDir.mkdir();
        }
        File testOutput = new File(System.getenv().get("HOME") + "/MatterhornTestResults/" + testId + ".log");
        testOutput.createNewFile();
        return testOutput;
    }

    /**
     * Writes the stack traces of all the verification errors to the output file
     *
     * @param testOutputFileName the unique ID number assigned to the test run
     * @throws java.io.IOException
     */
    static void logErrors(String testOutputFileName, StringBuffer errors) throws IOException {
        logger.debug("Writing errors");
        File testOutput = createTestOutputFile(testOutputFileName);
        BufferedWriter writer = new BufferedWriter(new FileWriter(testOutput));
        writer.write(errors.toString());
        writer.flush();
        writer.close();
    }

    /**
     * Writes a 'no error' message to the output file if all verifications succeeded
     *
     * @param testOutputFileName the unique ID number assigned to the test run
     * @throws IOException
     */
    static void logSuccess(String testOutputFileName) throws IOException {
        logger.info("No verification errors occurred");
        File testOutput = createTestOutputFile(testOutputFileName);
        PrintWriter writer = new PrintWriter(new FileWriter(testOutput));
        writer.write("No verification errors occurred");
        writer.flush();
        writer.close();
    }

    /**
     * Creates a results file and writes any verification errors to the file, else a "no errors" message
     *
     * @param clazz  the test class
     * @param testId the unique ID number assigned to the test run
     * @param errors the list of errors that occurred during the test run
     * @throws IOException
     */
    public static void writeTestResults(Class clazz, String testId, StringBuffer errors) throws IOException {
        String testOutputFileName = clazz.getSimpleName() + "_" + testId;
        String verificationErrors = errors.toString();
        if (!"".equals(verificationErrors)) {
            logErrors(testOutputFileName, errors);
            fail(verificationErrors);
        } else {
            logSuccess(testOutputFileName);
        }
    }

    /**
     * Checks the truth of an assertion.  If it fails, then appends the failure to string buffer without
     * halting the test execution.
     *
     * @param buffer    string buffer containing all verification errors in the test
     * @param condition the condition subject to the assertion
     */
    public static void verifyTrue(StringBuffer buffer, boolean condition) throws AssertionError {
        try {
            assertTrue(condition);
        } catch (AssertionError e) {
            buffer.append(ExceptionUtils.getStackTrace(e));
        }
    }

    /**
     * Checks the falsehood of an assertion.  If it fails, then appends the failure to string buffer without
     * halting the test execution.
     *
     * @param buffer    string buffer containing all verification errors in the test
     * @param condition the condition subject to the assertion
     */
    public static void verifyFalse(StringBuffer buffer, boolean condition) throws AssertionError {
        try {
            assertFalse(condition);
        } catch (AssertionError e) {
            buffer.append(ExceptionUtils.getStackTrace(e));
        }
    }

    /**
     * Checks the equality of two objects.  If it fails, then appends the failure to string buffer without
     * halting the test execution.
     *
     * @param buffer   string buffer containing all verification errors in the test
     * @param expected the expected condition
     * @param actual   the actual condition
     */
    public static void verifyEquals(StringBuffer buffer, Object expected, Object actual) throws AssertionError {
        try {
            assertEquals(expected, actual);
        } catch (AssertionError e) {
            buffer.append(ExceptionUtils.getStackTrace(e));
        }
    }

    /**
     * Checks the inequality of two objects.  If it fails, then appends the failure to string buffer without
     * halting the test execution.
     *
     * @param buffer     string buffer containing all verification errors in the test
     * @param unexpected the unexpected condition
     * @param actual     the actual condition
     */
    public static void verifyNotEquals(StringBuffer buffer, Object unexpected, Object actual) throws AssertionError {
        try {
            assertNotEquals(unexpected, actual);
        } catch (AssertionError e) {
            buffer.append(ExceptionUtils.getStackTrace(e));
        }
    }

    public static String getTestId() { return (Long.toString(new Date().getTime())).substring(0, 9); }

}
