package util;

import domain.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Date;

/**
 * @author Paul Farestveit
 */
public class SeriesUtils {

    final static Logger logger = LoggerFactory.getLogger(SeriesUtils.class);

    /**
     * Reads the course control number for the current test from a text file
     *
     * @return the course control number as a string
     * @throws IOException
     */
    static String getNextCcn() throws IOException {
        FileReader reader = new FileReader(System.getenv().get("HOME") + "/.matterhorn-webdriver-config/ccn.txt");
        BufferedReader bufferedReader = new BufferedReader(reader);
        String ccn = bufferedReader.readLine();
        logger.debug("Current CCN is " + ccn);
        bufferedReader.close();
        return ccn;
    }

    /**
     * Writes the course control number for the next test to a text file by incrementing the existing number
     *
     * @throws IOException
     */
    static void setNextCcn() throws IOException {
        int current = Integer.parseInt(getNextCcn());
        int next = current + 1;
        String nextCcn = String.valueOf(next);
        logger.debug("Next CCN is " + nextCcn);
        FileWriter writer = new FileWriter(System.getenv().get("HOME") + "/.matterhorn-webdriver-config/ccn.txt", false);
        PrintWriter printWriter = new PrintWriter(writer);
        printWriter.print(nextCcn);
        printWriter.close();
    }

    /**
     * Reads the existing CCN and then increments the number for the next test
     *
     * @return the course control number as a string
     * @throws IOException
     */
    public static String getCcn() throws IOException {
        String ccn = getNextCcn();
        setNextCcn();
        return ccn;
    }

    public static String getSeriesTitle(Series series) throws IOException {
        return series.getSeriesName() + " - " + SalesforceUtils.getCurrentTerm() + " " + SalesforceUtils.getCurrentYear();
    }

    public static Date getSeriesStartDate() {
        return new Date();
    }

    public static Date getSeriesEndDate(Date seriesStartDate) {
        return DateUtils.addDays(seriesStartDate, 10);
    }

    /**
     * Generates a unique name for a series
     *
     * @param instructors the series instructors (instructor)
     * @return a new series object
     */
    public static Series generateUniqueSeries(Class clazz, String testId, Instructor[] instructors, CaptureAgent captureAgent,
                                              Inputs inputs, PublishDelay publishDelay) throws IOException {
        final String seriesName = StringUtils.substring(clazz.getSimpleName(), 0, 19) + " " + testId;
        final Date startDate = getSeriesStartDate();
        final Date endDate = getSeriesEndDate(startDate);
        final String ccn = getCcn();
        final String seriesId = SalesforceUtils.getCurrentYear() + SalesforceUtils.getCurrentTermCode() + ccn;
        return new Series(seriesName, testId, instructors, Availability.COPY_NON_PUBLIC, seriesId, startDate, endDate, ccn, captureAgent, inputs, publishDelay);
    }

}
