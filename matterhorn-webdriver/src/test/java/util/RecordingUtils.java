package util;

import domain.*;
import org.apache.commons.lang3.time.DateUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

/**
 * @author Paul Farestveit
 */
public class RecordingUtils {

    private static Properties RECORDING_CONFIG;

    static {
        try {
            final InputStream inputStream = WebDriverUtils.class.getClassLoader().getResourceAsStream("selenium2-webdriver.properties");
            RECORDING_CONFIG = new Properties();
            RECORDING_CONFIG.load(inputStream);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Generates a unique name for a new recording belonging to an existing series
     *
     * @param series the series
     * @return a new recording, which can be used to upload or schedule the recording
     */
    public static Recording generateSeriesRecording(Class clazz, Series series) throws IOException {
        final String recordingTitle = "QA " + clazz.getSimpleName() + " " + series.getSeriesTestId();
        return new Recording(recordingTitle, series.getSeriesTestId(), series.getInstructors(), series, series.getAvailability().getLicense(),
                DateUtils.addSeconds(series.getSeriesStartDate(), TimeoutUtils.getCaptureSchedWaitSec()), series.getPublishDelay());
    }

    /**
     * Determines the date for the first recording in a series scheduled via the sign-up form.  The workflow tests ensure that
     * the recordings scheduled via the sign-up form start the day after recordings added to the series via the admin UI.
     * Since Salesforce isn't configured for weekend recordings, this method prevents the start date from falling on a weekend.
     *
     * @param series the series
     * @return the recording date
     */
    public static Date getFirstRecordingDate(Series series) {
        if (DateFormatUtils.getDayOfWeek(series.getSeriesStartDate()).equals("Friday") ||
                DateFormatUtils.getDayOfWeek(series.getSeriesStartDate()).equals("Saturday")) {
            return DateUtils.addDays(series.getSeriesStartDate(), 3);
        }
        else {
            return DateUtils.addDays(series.getSeriesStartDate(), 1);
        }
    }

    public static String getDurationHours() throws IOException {
        return RECORDING_CONFIG.getProperty("duration.hours");
    }

    public static String getDurationMinutes() throws IOException {
        return RECORDING_CONFIG.getProperty("duration.minutes");
    }

    /**
     * Used for getting the expected recording title in the warehouse JSON when the recording has hyphens
     *
     * @param fullRecordingTitle the full, hyphenated recording title
     * @return the partial title, split at the first hyphen
     */
    public static String getRecordingTitleJson(String fullRecordingTitle) {
        return fullRecordingTitle.split(" - ", 2)[1];
    }
}
