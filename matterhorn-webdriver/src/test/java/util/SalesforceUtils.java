package util;

import domain.SalesforceSemesterProject;
import domain.Series;

import java.io.*;
import java.util.Properties;

/**
 * @author Paul Farestveit
 */
public class SalesforceUtils {

    private static Properties CREDENTIALS;

    static {
        try {
            final InputStream inputStream = new FileInputStream(System.getenv().get("HOME") + "/.matterhorn-webdriver-config/authentication.properties");
            CREDENTIALS = new Properties();
            CREDENTIALS.load(inputStream);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getCurrentTerm() throws IOException {
        return CREDENTIALS.getProperty("current.term");
    }

    public static String getCurrentTermCode() throws IOException {
        return CREDENTIALS.getProperty("current.term.code");
    }

    public static String getCurrentYear() throws IOException {
        return CREDENTIALS.getProperty("current.year");
    }

    public static String getCourseOfferingId(String ccn) throws IOException {
        return CREDENTIALS.getProperty("current.year") + CREDENTIALS.getProperty("current.term.code") + ccn;
    }

     public static String getSignUpFormUrl(Series series) throws IOException {
        return AuthenticationUtils.getMatterhornAdminTool() + "/signUp.html?id=" + series.getSeriesId();
    }

    public static String getSemesterProjectName(Series series) {
        return "Semester " + series.getSeriesName();
    }

    public static String getRecordingStartTime() throws IOException {
        return CREDENTIALS.getProperty("salesforce.start.time");
    }

    public static String getRecordingEndTime() throws IOException {
        return CREDENTIALS.getProperty("salesforce.end.time");
    }

    public static SalesforceSemesterProject generateSalesforceSemesterProject(Series series) throws IOException {
        return new SalesforceSemesterProject(getSemesterProjectName(series), getCurrentYear(), getCurrentTerm(), series.getSeriesStartDate(),
                series.getSeriesEndDate());
    }

}
