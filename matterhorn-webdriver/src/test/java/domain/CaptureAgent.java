package domain;

/**
 * @author Paul Farestveit
 */
public enum CaptureAgent {

    // At the moment, there is only one capture agent available for QA
    VIDEO_AGENT("pauley-ballroom", true, true, "Pauley Ballroom", "PauleyBallroom", null),
    SCREENCAST_AGENT("pauley-ballroom", true, true, "Pauley Ballroom", "PauleyBallroom", null);

    private String captureAgentName;
    private boolean hasVideo;
    private boolean hasScreen;
    private String room;
    private String warehouseBuilding;
    private String warehouseRoom;

    CaptureAgent(String captureAgentName, boolean hasVideo, boolean hasScreen, String room,
                         String warehouseBuilding, String warehouseRoom) {
        this.captureAgentName = captureAgentName;
        this.hasVideo = hasVideo;
        this.hasScreen = hasScreen;
        this.room = room;
        this.warehouseBuilding = warehouseBuilding;
        this.warehouseRoom = warehouseRoom;
    }

    public String getCaptureAgentName() { return captureAgentName; }

    public boolean agentHasVideo() { return hasVideo; }

    public boolean agentHasScreen() { return hasScreen; }

    public String getRoom() { return room; }

    public String getWarehouseBuilding() { return warehouseBuilding; }

    public String getWarehouseRoom() { return warehouseRoom; }

}
