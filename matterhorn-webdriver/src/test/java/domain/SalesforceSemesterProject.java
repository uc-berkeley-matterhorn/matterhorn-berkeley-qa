package domain;

import java.util.Date;

/**
 * @author Paul Farestveit
 */
public class SalesforceSemesterProject {

    private String semesterProjectName;
    private String semesterYear;
    private String semesterTerm;
    private Date semesterStartDate;
    private Date semesterEndDate;

    public SalesforceSemesterProject(String semesterProjectName, String semesterYear, String semesterTerm,
                                     Date semesterStartDate, Date semesterEndDate) {
        this.semesterProjectName = semesterProjectName;
        this.semesterYear = semesterYear;
        this.semesterTerm = semesterTerm;
        this.semesterStartDate = semesterStartDate;
        this.semesterEndDate = semesterEndDate;
    }

    public String getSemesterProjectName() {
        return semesterProjectName;
    }

    public String getSemesterYear() {
        return semesterYear;
    }

    public String getSemesterTerm() {
        return semesterTerm;
    }

    public Date getSemesterStartDate() {
        return semesterStartDate;
    }

    public Date getSemesterEndDate() {
        return semesterEndDate;
    }
}
