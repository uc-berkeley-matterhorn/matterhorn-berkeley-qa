package domain;

/**
 * @author Paul Farestveit
 */
public enum PublishDelay {

    ZERO_DAYS("As soon after capture as possible", 0),
    ONE_DAY("1 day", 1),
    TWO_DAYS("2 days", 2),
    THREE_DAYS("3 days", 3),
    FOUR_DAYS("4 days", 4),
    FIVE_DAYS("5 days", 5),
    SIX_DAYS("6 days", 6),
    SEVEN_DAYS("7 days", 7),
    EIGHT_DAYS("8 days", 8),
    NINE_DAYS("9 days", 9),
    TEN_DAYS("10 days", 10);

    private String delayDesc;
    private int delayDaysNumber;

    PublishDelay(String delayDesc, int delayDaysNumber) {
        this.delayDesc = delayDesc;
        this.delayDaysNumber = delayDaysNumber;
    }

    public String getDelayDesc() {
        return delayDesc;
    }

    public int getDelayDaysNumber() {
        return delayDaysNumber;
    }

    public String getDelayDaysString() {
        return String.valueOf(delayDaysNumber);
    }

}
