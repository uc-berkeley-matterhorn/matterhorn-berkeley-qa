package domain;

/**
 * @author Paul Farestveit
 */
public enum  Availability {

    COPY_NON_PUBLIC(License.COPYRIGHT, "studentsOnly", "private", "course", false);

    private License license;
    private String matterhornAvailability;
    private String youTubeAvailability;
    private String warehouseAvailability;
    private boolean jsonPublicFlag;

    Availability(License license, String matterhornAvailability, String youTubeAvailability, String warehouseAvailability, boolean jsonPublicFlag) {
        this.license = license;
        this.matterhornAvailability = matterhornAvailability;
        this.youTubeAvailability = youTubeAvailability;
        this.warehouseAvailability = warehouseAvailability;
        this.jsonPublicFlag = jsonPublicFlag;
    }

    public License getLicense() { return license; }

    public String getMatterhornAvailability() { return matterhornAvailability; }

    public String getYouTubeAvailability() { return youTubeAvailability; }

    public String getWarehouseAvailability() { return warehouseAvailability; }

    public boolean isJsonPublicFlag() { return jsonPublicFlag; }

}
