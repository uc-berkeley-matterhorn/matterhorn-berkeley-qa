package domain;

import java.util.Date;

/**
 * @author Paul Farestveit
 */
public class Recording {

    private String title;
    private String recordingTestId;
    private Instructor[] instructors;
    private Series series;
    private License license;
    private Date recordingDate;
    private PublishDelay delay;

    public Recording(String title, String recordingTestId, Instructor[] instructors, Series series, License license,
                     Date recordingDate, PublishDelay delay) {
        this.title = title;
        this.recordingTestId = recordingTestId;
        this.instructors = instructors;
        this.series = series;
        this.license = license;
        this.recordingDate = recordingDate;
        this.delay = delay;
    }

    public String getTitle() {
        return title;
    }

    public Instructor[] getInstructors() {
        return instructors;
    }

    public Series getSeries() {
        return series;
    }

    public String getSeriesTitle() {
        return series.getSeriesName();
    }

    public License getLicense() {
        return license;
    }

    public String getLicenseDescription() {
        return license.getLicenseDescription();
    }

    public Date getRecordingDate() {
        return recordingDate;
    }

    public String getRecordingTestId() {
        return recordingTestId;
    }

    public PublishDelay getDelay() {
        return delay;
    }

    public void setTitle(String newTitle) {
        title = newTitle;
    }

    public void setInstructors(Instructor[] newInstructors) {
        instructors = newInstructors;
    }

    public void setSeries(Series newSeries) {
        series = newSeries;
    }

    public void setLicense(License newLicense) {
        license = newLicense;
    }

    public void setRecordingDate(Date newRecordingDate) {
        recordingDate = newRecordingDate;
    }

    public void setDelay(PublishDelay newDelay) {
        delay = newDelay;
    }

}
