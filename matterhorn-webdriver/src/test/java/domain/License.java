package domain;

/**
 * @author Paul Farestveit
 */
public enum License {

    COPYRIGHT("Copyright © 2019 UC Regents; all rights reserved", "all_rights_reserved");

    private String licenseDescription;
    private String warehouseLicense;

    License(String licenseDescription, String warehouseLicense) {
        this.licenseDescription = licenseDescription;
        this.warehouseLicense = warehouseLicense;
    }

    public String getLicenseDescription() {
        return licenseDescription;
    }

    public String getWarehouseLicense() { return warehouseLicense; }

}
