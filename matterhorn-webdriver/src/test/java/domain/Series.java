package domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * @author Paul Farestveit
 */
public class Series {

    final Logger logger = LoggerFactory.getLogger(this.getClass());

    private String seriesName;
    private String seriesTestId;
    private Instructor[] instructors;
    private Availability availability;
    private String seriesId;
    private Date seriesStartDate;
    private Date seriesEndDate;
    private String ccn;
    private CaptureAgent captureAgent;
    private Inputs inputs;
    private PublishDelay publishDelay;

    public Series(String seriesName, String seriesTestId, Instructor[] instructors, Availability availability,
                  String seriesId, Date seriesStartDate, Date seriesEndDate, String ccn, CaptureAgent captureAgent, Inputs inputs,
                  PublishDelay publishDelay) {
        this.seriesName = seriesName;
        this.seriesTestId = seriesTestId;
        this.instructors = instructors;
        this.availability = availability;
        this.seriesId = seriesId;
        this.seriesStartDate = seriesStartDate;
        this.seriesEndDate = seriesEndDate;
        this.ccn = ccn;
        this.captureAgent = captureAgent;
        this.inputs = inputs;
        this.publishDelay = publishDelay;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public String getSeriesTestId() {
        return seriesTestId;
    }

    public Instructor[] getInstructors() {
        return instructors;
    }

    public Availability getAvailability() {
        return availability;
    }

    public String getSeriesId() {
        return seriesId;
    }

    public Date getSeriesStartDate() {
        return seriesStartDate;
    }

    public Date getSeriesEndDate() {
        return seriesEndDate;
    }

    public String getCcn() {
        return ccn;
    }

    public String getCourseTitle() {
        return "This is (the) course's t-itle";
    }

    public CaptureAgent getCaptureAgent() {
        return captureAgent;
    }

    public Inputs getInputs() {
        return inputs;
    }

    public PublishDelay getPublishDelay() {
        return publishDelay;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public void setInstructors(Instructor[] instructors) {
        this.instructors = instructors;
    }

    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    public void setSeriesId(String seriesId) {
        logger.info("Series ID is " + seriesId);
        this.seriesId = seriesId;
    }

    public void setCcn(String newCcn) {
        logger.info("Series CCN is now " + newCcn);
        this.ccn = newCcn;
    }

    public void setInputs(Inputs inputs) {
        this.inputs = inputs;
    }

    public void setPublishDelay(PublishDelay publishDelay) {
        this.publishDelay = publishDelay;
    }

}
