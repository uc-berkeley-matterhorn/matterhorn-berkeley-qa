package domain;

/**
 * @author Paul Farestveit
 */
public enum Inputs {
    
    AUDIO_ONLY("Audio only", "audio"),
    SCREENCAST("Computer Screen with Audio", "screencast"),
    VIDEO_ONLY("Video with Audio", "video"),
    VIDEO_SCREEN("Video, Computer Screen, and Audio", "video_and_screencast");

    private String description;
    private String warehouseDesc;

    Inputs(String description, String warehouseDesc) {
        this.description = description;
        this.warehouseDesc = warehouseDesc;
    }

    public String getDescription() {
        return description;
    }

    public String getWarehouseDesc() {
        return warehouseDesc;
    }
    
}
