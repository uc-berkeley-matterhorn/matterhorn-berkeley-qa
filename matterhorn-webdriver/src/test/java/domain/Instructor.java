package domain;

/**
 * @author Paul Farestveit
 */
public enum Instructor {

    TEST_INSTRUCTOR_1("Test Instructor", "test-322589"),
    TEST_INSTRUCTOR_2("Test Instructor Two", "test-322590"),
    TEST_GSI("Test Gsi", "test-212388");

    private String instructorName;
    private String instructorUsername;

    Instructor(String instructorName, String instructorUsername) {
        this.instructorName = instructorName;
        this.instructorUsername = instructorUsername;
    }

    public String getInstructorName() {
        return instructorName;
    }

    public String getInstructorUsername() {
        return instructorUsername;
    }

    public String toString() {
        return getInstructorName();
    }
}
