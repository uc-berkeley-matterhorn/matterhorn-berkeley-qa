/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package warehouse.domain;

/**
 * @author John Crossman
 */
public class WarehouseRecording {

    private final int id;
    private final String title;
    private final String youTubeVideoId;
    private final String trimmed_length;
    private final String matterhorn_episode_id;

    public WarehouseRecording(final int id, final String title, final String youTubeVideoId, final String trimmed_length,
                              final String matterhorn_episode_id) {
        this.id = id;
        this.title = title;
        this.youTubeVideoId = youTubeVideoId;
        this.trimmed_length = trimmed_length;
        this.matterhorn_episode_id = matterhorn_episode_id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getYouTubeVideoId() {
        return youTubeVideoId;
    }

    public String getTrimmed_length() {
        return trimmed_length;
    }

    public String getMatterhorn_episode_id() {
        return matterhorn_episode_id;
    }
}
