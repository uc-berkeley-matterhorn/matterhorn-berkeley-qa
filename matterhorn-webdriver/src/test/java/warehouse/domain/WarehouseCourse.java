/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package warehouse.domain;

import domain.Instructor;
import domain.Recording;
import domain.Series;
import org.apache.commons.lang3.StringUtils;
import util.TestUtils;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author John Crossman
 */
public class WarehouseCourse {

    private final CourseKey courseKey;
    private String catalogId;
    private String title;
    private String departmentName;
    private String description;
    private String audienceType;
    private String recordingType;
    private String licenseType;
    private String buildingName;
    private String roomNumber;
    private String[] instructor_names;
    private final List<WarehouseRecording> recordingList;

    public WarehouseCourse(final CourseKey courseKey, final List<WarehouseRecording> recordingList) {
        this.courseKey = courseKey;
        this.recordingList = recordingList;
    }

    public WarehouseCourse(final CourseKey courseKey) {
        this(courseKey, new LinkedList<WarehouseRecording>());
    }

    public void addRecording(final WarehouseRecording recording) {
        recordingList.add(recording);
    }

    public CourseKey getCourseKey() {
        return courseKey;
    }

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(final String catalogId) {
        this.catalogId = catalogId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(final String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getAudienceType() {
        return audienceType;
    }

    public void setAudienceType(final String audienceType) {
        this.audienceType = audienceType;
    }

    public String getRecordingType() {
        return recordingType;
    }

    public void setRecordingType(final String recordingType) {
        this.recordingType = recordingType;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(final String licenseType) {
        this.licenseType = licenseType;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(final String buildingName) {
        this.buildingName = buildingName;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(final String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String[] getInstructors() {
        return instructor_names;
    }

    public void setInstructors(final String[] instructor_names) {
        this.instructor_names = instructor_names;
    }

    public List<WarehouseRecording> getRecordingList() {
        return recordingList;
    }

    public void verifyCourseData(StringBuffer results, Series series, Instructor[] instructors) {
        TestUtils.verifyEquals(results, series.getSeriesName(), getTitle());
        TestUtils.verifyEquals(results, series.getCourseTitle() + " - " + StringUtils.join(instructors, ", "), getDescription());
        TestUtils.verifyEquals(results, series.getAvailability().getWarehouseAvailability(), getAudienceType());
        TestUtils.verifyEquals(results, series.getAvailability().getLicense().getWarehouseLicense(), getLicenseType());
        TestUtils.verifyEquals(results, series.getInputs().getWarehouseDesc(), getRecordingType());
        String[] instructorNames = {instructors[0].getInstructorName(), instructors[1].getInstructorName()};
        TestUtils.verifyTrue(results, Arrays.equals(instructorNames, getInstructors()));
        TestUtils.verifyEquals(results, series.getCaptureAgent().getWarehouseBuilding(), getBuildingName());
        TestUtils.verifyEquals(results, series.getCaptureAgent().getWarehouseRoom(), getRoomNumber());
    }
}
