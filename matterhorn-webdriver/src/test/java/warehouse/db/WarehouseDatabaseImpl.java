/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package warehouse.db;

import warehouse.domain.CourseKey;
import warehouse.domain.WarehouseCourse;

import java.util.Set;

/**
 * @author John Crossman
 */
public class WarehouseDatabaseImpl implements WarehouseDatabase {

  private static WarehouseDatabaseImpl database = null;
  private final WarehouseQuery warehouseQuery;

  public static void main(final String[] args) {
    final WarehouseDatabase db = getInstance();
    final WarehouseCourse course = db.getWarehouseCourse(new CourseKey(2014, 'D', 2609));
    System.out.println(course.getCourseKey().getYear());
    System.out.println(course.getCourseKey().getSemester());
    System.out.println(course.getCourseKey().getCcn());
    System.out.println(course.getCatalogId());
    System.out.println(course.getDepartmentName());
  }

  public static WarehouseDatabase getInstance() {
    if (database == null) {
      database = new WarehouseDatabaseImpl();
    }
    return database;
  }

  private WarehouseDatabaseImpl() {
    this.warehouseQuery = WarehouseQuery.getInstance();
  }

  @Override
  public WarehouseCourse getWarehouseCourse(final CourseKey courseKey) {
    final WarehouseCourse course;
    final String sql = getSelectCourseSQL(courseKey.getYear(), courseKey.getSemester(), courseKey.getCcn());
    final Set<WarehouseCourse> select = warehouseQuery.select(sql, new ResultConverterForWarehouse());
    if (select.isEmpty()) {
      course = null;
    } else if (select.size() == 1) {
      course = select.iterator().next();
    } else {
      throw new IllegalStateException("SQL query produced 2+ results: " + sql);
    }
    return course;
  }

  private String getSelectCourseSQL(final int year, final char semester, final Integer ccn) {
    final String sql = "SELECT\n"
            + "    c.year, c.semester_code, c.ccn, c.catalog_id, c.title, c.department_name, c.description, c.section,\n"
            + "    c.audience_type, c.recording_type, c.license_type, c.building_name, c.room_number, c.instructor_names,\n"
            + "    c.youtube_playlist_id, c.meeting_days, c.itunes_audio_collection_id, c.itunes_video_collection_id,\n"
            + "    c.rss_feed_audio, c.rss_feed_video, c.rss_feed_screen,\n"
            + "    r.recording_id, r.title as recording_title, r.description as recording_description, r.youtube_video_id,\n"
            + "    r.recording_start_utc,  r.recording_end_utc, r.trimmed_length, r.matterhorn_episode_id\n"
            + " FROM\n"
            + "  course as c\n"
            + " LEFT JOIN\n"
            + "    recording as r\n"
            + "    ON\n"
            + "        c.year = r.year\n"
            + "    AND\n"
            + "        c.semester_code = r.semester_code\n"
            + "    AND\n"
            + "        c.ccn = r.ccn\n"
            + "    WHERE\n"
            + "        c.year=" + year + '\n'
            + "        AND c.semester_code='" + Character.toUpperCase(semester) + "'\n";
    return ccn == null ? sql : sql + " AND c.ccn="  + ccn;
  }
}
