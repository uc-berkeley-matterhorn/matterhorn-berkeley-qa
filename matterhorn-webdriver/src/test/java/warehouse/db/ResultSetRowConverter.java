/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package warehouse.db;

import org.apache.commons.lang3.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * @author John Crossman
 */
abstract class ResultSetRowConverter<T> {

    /**
     * @author John Crossman
     */
    protected interface Column<T> {

        Class<T> getType();

        String name();

    }

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * @param resultSet never null
     * @return getInstance populated with relevant column data of resultSet
     * @throws java.sql.SQLException
     */
    public abstract T convert(ResultSet resultSet) throws SQLException;

    /**
     * @param r      one row of results from database query
     * @param column column we are looking for
     * @param <T>    data type of column
     * @return value, possibly null
     * @throws SQLException
     */
    protected <T> T get(final ResultSet r, final Column<T> column) throws SQLException {
        final String columnName = column.name();
        try {
            final T result;
            if (isAssignable(column, String.class)) {
                result = (T) r.getString(columnName);
            } else if (isAssignable(column, Integer.class)) {
                result = (T) new Integer(r.getInt(columnName));
            } else if (isAssignable(column, Date.class)) {
                result = (T) r.getDate(columnName);
            } else if (isAssignable(column, Array.class)) {
                result = (T) r.getArray(columnName);
            } else if (isAssignable(column, Object.class)) {
                result = (T) r.getArray(columnName);
            } else {
                throw new UnsupportedOperationException("Unexpected column type: " + column.getType());
            }
            return result;
        } catch (final SQLException e) {
            logger.error("FAILURE on column '" + columnName + "' where type = " + column.getType().getSimpleName(), e);
            throw e;
        }
    }

    private boolean isAssignable(final Column column, final Class clazz) {
        return ClassUtils.isAssignable(column.getType(), clazz);
    }

    protected enum ColInt implements Column<Integer> {

        ccn, year, recording_id, recordingId;

        @Override
        public Class<Integer> getType() {
            return Integer.class;
        }
    }

    protected enum ColStr implements Column<String> {

        audience_type, description, itunes_audio_collection_id, itunes_video_collection_id, room_number, rss_feed_audio,
        rss_feed_screen, rss_feed_video, section, semester_code, title, youtube_playlist_id, building_name, recording_type,
        license_type, catalog_id, department_name, youtube_video_id, trimmed_length, matterhorn_episode_id, recording,
        course, recording_title, recording_description, dateParsePattern;

        @Override
        public Class<String> getType() {
            return String.class;
        }

    }

    public enum ColArray implements Column<Array> {

        instructor_names, meeting_days;

        @Override
        public Class<Array> getType() {
            return Array.class;
        }
    }

}
