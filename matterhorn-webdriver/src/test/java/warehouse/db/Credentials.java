/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package warehouse.db;

/**
 * @author John Crossman
 */
public class Credentials {

  private final String jdbcURL;
  private final String username;
  private final String password;

  public Credentials(final String jdbcURL, final String username, final String password) {
    this.jdbcURL = jdbcURL;
    this.username = username;
    this.password = password;
  }

  public String getJdbcURL() {
    return jdbcURL;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }
}
