/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package warehouse.db;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import util.AuthenticationUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author John Crossman
 */
class WarehouseQuery {

  private static WarehouseQuery singleton;

  private ComboPooledDataSource pooledDataSource;

  static WarehouseQuery getInstance() {
    if (singleton == null) {
      singleton  = new WarehouseQuery();
    }
    return singleton;
  }

  private WarehouseQuery() {
    final Date beforeAttempt = new Date();
    try {
      final Credentials credentials = AuthenticationUtils.getWarehouseDatabaseCredentials();
      pooledDataSource = new ComboPooledDataSource();
      pooledDataSource.setDriverClass("org.postgresql.Driver");
      pooledDataSource.setJdbcUrl(credentials.getJdbcURL());
      pooledDataSource.setUser(credentials.getUsername());
      pooledDataSource.setPassword(credentials.getPassword());
      pooledDataSource.setMinPoolSize(2);
      pooledDataSource.setInitialPoolSize(0);
      pooledDataSource.setAcquireIncrement(2);
      pooledDataSource.setMaxPoolSize(20);
      pooledDataSource.setMaxIdleTime(600);
      pooledDataSource.setMaxIdleTimeExcessConnections(300);
    } catch (final Exception e) {
      final long seconds = (new Date().getTime() - beforeAttempt.getTime()) / 1000;
      final String message = seconds + " seconds after invoking DriverManager.getConnection(): " + e.getMessage();
      throw new IllegalStateException(message, e);
    }
  }

  <T> Set<T> select(final String sql, final ResultSetRowConverter<T> rowConverter) {
    Statement statement = null;
    Connection connection = null;

    try {
      connection = pooledDataSource.getConnection();
      statement = connection.createStatement();
      final ResultSet resultSet = statement.executeQuery(sql);
      final Set<T> set = new HashSet<T>();
      while (resultSet.next()) {
        set.add(rowConverter.convert(resultSet));
      }
      return set;
    } catch (final SQLException e) {
      throw new IllegalStateException("Failed to connect and/or run query:" + '\n' + sql, e);
    } finally {
      closeWithTolerance(statement);
      closeWithTolerance(connection);
    }
  }

  private void closeWithTolerance(final Object obj) {
    try {
      if (obj instanceof ResultSet) {
        ((ResultSet) obj).close();
      } else if (obj instanceof Statement) {
        ((Statement) obj).close();
      } else if (obj instanceof Connection) {
        ((Connection) obj).close();
      }
    } catch (final SQLException ignore) {
      // We swallow this exception so the connection.close() is still invoked.
      ignore.printStackTrace();
    }
  }

}
