/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package warehouse.db;

import warehouse.domain.CourseKey;
import warehouse.domain.WarehouseCourse;
import warehouse.domain.WarehouseRecording;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author John Crossman
 */
class ResultConverterForWarehouse extends ResultSetRowConverter<WarehouseCourse> {

    private final Map<String, WarehouseCourse> results;

    ResultConverterForWarehouse() {
        results = new HashMap<String, WarehouseCourse>();
    }

    public WarehouseCourse convert(final ResultSet r) throws SQLException {
        final Integer year = get(r, ColInt.year);
        final char semester = getSemester(r);
        final Integer ccn = get(r, ColInt.ccn);
        final String key = year.toString() + semester + ccn.toString();
        final WarehouseCourse w;
        if (results.containsKey(key)) {
            w = results.get(key);
        } else {
            w = new WarehouseCourse(new CourseKey(year, semester, ccn));
            w.setCatalogId(get(r, ColStr.catalog_id));
            w.setTitle(get(r, ColStr.title));
            w.setDepartmentName(get(r, ColStr.department_name));
            w.setDescription(get(r, ColStr.description));
            w.setAudienceType(get(r, ColStr.audience_type));
            w.setRecordingType(get(r, ColStr.recording_type));
            w.setLicenseType(get(r, ColStr.license_type));
            w.setBuildingName(get(r, ColStr.building_name));
            w.setRoomNumber(get(r, ColStr.room_number));
            w.setInstructors(getInstructorNames(r));
            results.put(key, w);
        }
        final Integer recordingId = get(r, ColInt.recording_id);
        if (recordingId != null && recordingId > 0) {
            final WarehouseRecording recording = new WarehouseRecording(get(r, ColInt.recording_id), get(r, ColStr.recording_title),
                    get(r, ColStr.youtube_video_id), get(r, ColStr.trimmed_length), get(r, ColStr.matterhorn_episode_id));
            w.addRecording(recording);
        }
        return w;
    }

    private String[] getInstructorNames(final ResultSet r) throws SQLException {
        final Array array = get(r, ColArray.instructor_names);
        final String[] values = array == null ? null : (String[]) array.getArray();
        return values == null || (values.length == 0) ? null : values;
    }

    private char getSemester(final ResultSet r) throws SQLException {
        return get(r, ColStr.semester_code).charAt(0);
    }

}