package test.matterhorn;

import domain.*;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import page.CalNetAuthPage;
import page.matterhorn.*;
import page.SalesforcePage;
import page.YouTubePage;
import util.*;
import warehouse.db.WarehouseDatabase;
import warehouse.db.WarehouseDatabaseImpl;
import warehouse.domain.CourseKey;
import warehouse.domain.WarehouseCourse;

/**
 * @author Paul Farestveit
 */

public class UpVidRepubMetaTest {

    /**
     * Tests the following workflow:
     * - Course:
     * -- uploaded video-only recording
     * - Initial Publish:
     * -- no publish delay
     * -- no review/trim
     * - Republish Metadata:
     * -- edit series title (which has no effect)
     * -- edit instructors
     */

    private WebDriver driver;
    private final String testId = TestUtils.getTestId();
    private final StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        this.driver = WebDriverUtils.getWebDriver();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        TestUtils.writeTestResults(this.getClass(), testId, verificationErrors);
    }

    @Test
    public void testUploadVideoRepubMeta() throws Exception {

        HomePage homePage = new HomePage(driver);
        CalNetAuthPage calNetPage = new CalNetAuthPage(driver);
        RecordingsPage recordingsPage = homePage.logIn(driver, calNetPage);
        recordingsPage.deleteUpcomingRecordings(driver);

        /*
            Create new Salesforce semester project and new course project.  Generate series via sign-up form.
         */

        int pageLoadTimeout = TimeoutUtils.getPageLoadWaitSec();

        Instructor[] instructors = {Instructor.TEST_INSTRUCTOR_1, Instructor.TEST_GSI};
        Series series = SeriesUtils.generateUniqueSeries(this.getClass(), testId, instructors, CaptureAgent.VIDEO_AGENT, Inputs.VIDEO_ONLY, PublishDelay.ZERO_DAYS);
        Recording recording = RecordingUtils.generateSeriesRecording(this.getClass(), series);
        SalesforceSemesterProject salesforceSemesterProject = SalesforceUtils.generateSalesforceSemesterProject(series);

        SalesforcePage salesforce = new SalesforcePage(driver);
        salesforce.setUpSalesforceProjects(driver, salesforceSemesterProject, series);
        series.setSeriesName(SeriesUtils.getSeriesTitle(series));

        SignUpPage signUpPage = new SignUpPage(driver);
        signUpPage.loadCompleteSubmit(driver, series);
        SeriesInfoPage seriesInfoPage = new SeriesInfoPage(driver);
        seriesInfoPage.loadPage(driver, series);
        seriesInfoPage.waitForText(seriesInfoPage.seriesAvailability, series.getAvailability().getMatterhornAvailability(), pageLoadTimeout);

        /*
            Upload recording for the series and wait for workflow to finish
         */

        recordingsPage.loadPage(driver);
        RecordingUploadPage recordingUploadPage = recordingsPage.clickUploadRecordingButton(driver);
        recordingUploadPage.inputRecordingMetadata(driver, recording, true);
        recordingUploadPage.inputMultipleFiles(driver, null, FileUploadUtils.getVideoUploadFileName(), FileUploadUtils.getAudioUploadFileName());
        recordingUploadPage.submitUpload(driver);
        recordingUploadPage.clickBackToRecordingsLink(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 3);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 3, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 3, "Finished");

        // Verify YouTube distribution
        YouTubePage youTube = new YouTubePage(driver);
        youTube.logInViaSpa(driver, calNetPage);
        String youTubeVideoId = youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);

        // Verify warehouse JSON
        WarehouseJsonPage json = new WarehouseJsonPage(driver);
        JSONObject course = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(course));
        JSONObject lecture = json.waitForCourseRecordingVideoId(driver, series, youTubeVideoId);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), json.getRecordingTitle(lecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(lecture));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, youTubeVideoId, verificationErrors);

        // Verify warehouse DB
        WarehouseDatabase db = WarehouseDatabaseImpl.getInstance();
        WarehouseCourse warehouseCourse = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourse.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourse.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, warehouseCourse.getRecordingList().get(0).getYouTubeVideoId());

        /*
            Edit series title
         */

        String oldSeriesName = series.getSeriesName();
        String newSeriesName = series.getSeriesName() + "-edited";
        series.setSeriesName(newSeriesName);
        SeriesEditPage seriesEditPage = new SeriesEditPage(driver);
        seriesEditPage.loadPage(driver, series);
        seriesEditPage.inputSeriesMetadata(driver, true, series);
        seriesEditPage.clickSaveButton(driver);

        /*
            Edit orig Instructors on archive tab, then republish metadata
         */

        recordingsPage.loadPage(driver);
        ArchivesPage archivesPage = recordingsPage.clickArchivesTab(driver);
        archivesPage.searchForRecording(driver, recording, 1);
        archivesPage.clickEditLinkRowOne(driver);
        Instructor[] newerInstructors = {Instructor.TEST_INSTRUCTOR_1, Instructor.TEST_GSI};
        recording.setInstructors(newerInstructors);
        archivesPage.inputNewMetadata(driver, recording);
        archivesPage.submitNewMetadata(driver);
        archivesPage.selectRepublishMetadataRowOne(driver);
        archivesPage.clickSubmitButton(driver);
        archivesPage.clickHomepageLink(driver);
        homePage.clickAdminLink(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 4);

        // Wait for recording to finish processing
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 4, "Finished");
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), recordingsPage.recordingTitle(driver, 3).getText());
        TestUtils.verifyEquals(verificationErrors, "", recordingsPage.recordingCaptureAgent(driver, 3).getText());

        // Verify YouTube distribution - no update to instructor metadata
        series.setSeriesName(oldSeriesName);
        youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);
        youTube.verifyYouTubeSharing(driver, youTubeVideoId, verificationErrors);

    }
}
