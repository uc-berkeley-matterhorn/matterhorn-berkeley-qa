package test.matterhorn;

import domain.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import page.CalNetAuthPage;
import page.matterhorn.*;
import page.SalesforcePage;
import page.YouTubePage;
import util.*;
import warehouse.db.WarehouseDatabase;
import warehouse.db.WarehouseDatabaseImpl;
import warehouse.domain.CourseKey;
import warehouse.domain.WarehouseCourse;

/**
 * @author Paul Farestveit
 */

public class SchdMultVidRetractRepubRetractTest {

    /**
     * Tests the following workflow:
     * - Course:
     * -- captured recording on video agent, video-only input
     * - Initial Publish:
     * -- pre-recording edit:
     * --- edit recording title
     * -- nine day publish delay
     * -- no review/trim
     * -- cancel publish delay
     * - Retract
     * - Republish:
     * -- review/trim:
     * --- trim recording
     * --- remove publish delay
     * - Retract
     */

    private WebDriver driver;
    private final String testId = TestUtils.getTestId();
    private final StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        this.driver = WebDriverUtils.getWebDriver();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        TestUtils.writeTestResults(this.getClass(), testId, verificationErrors);
    }

    @Test
    public void testSchedMultVidRepubRetract() throws Exception {

        HomePage homePage = new HomePage(driver);
        CalNetAuthPage calNetPage = new CalNetAuthPage(driver);
        RecordingsPage recordingsPage = homePage.logIn(driver, calNetPage);
        recordingsPage.deleteUpcomingRecordings(driver);

        /*
            Create new Salesforce semester project and new course project.  Generate series via sign-up form.
         */

        int pageLoadTimeout = TimeoutUtils.getPageLoadWaitSec();

        Instructor[] instructors = {Instructor.TEST_INSTRUCTOR_1, Instructor.TEST_GSI};
        Series series = SeriesUtils.generateUniqueSeries(this.getClass(), testId, instructors, CaptureAgent.VIDEO_AGENT, Inputs.VIDEO_ONLY, PublishDelay.NINE_DAYS);
        SalesforceSemesterProject salesforceSemesterProject = SalesforceUtils.generateSalesforceSemesterProject(series);

        SalesforcePage salesforce = new SalesforcePage(driver);
        salesforce.setUpSalesforceProjects(driver, salesforceSemesterProject, series);
        series.setSeriesName(SeriesUtils.getSeriesTitle(series));

        SignUpPage signUpPage = new SignUpPage(driver);
        signUpPage.loadCompleteSubmit(driver, series);
        SeriesInfoPage seriesInfoPage = new SeriesInfoPage(driver);
        seriesInfoPage.loadPage(driver, series);
        seriesInfoPage.waitForText(seriesInfoPage.seriesAvailability, series.getAvailability().getMatterhornAvailability(), pageLoadTimeout);

        /*
            Schedule two additional recordings for the series.  Capture one and wait for workflow to reach "On Hold" status.
         */

        Recording recording = RecordingUtils.generateSeriesRecording(this.getClass(), series);
        recordingsPage.loadPage(driver);
        RecordingsSchedulerPage schedulerPage = recordingsPage.clickScheduleRecordingButton(driver);
        schedulerPage.selectMultipleRecordings(driver);
        schedulerPage.inputRecordingMetadata(driver, recording);
        schedulerPage.scheduleRecurringCaptures(driver, recording);
        schedulerPage.addPublishDelayMultiRecordings(driver, series);
        schedulerPage.submitScheduledRecording(driver);
        recordingsPage.loadPage(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 4);
        String recordingOneTitle = recording.getTitle() + " - " + DateFormatUtils.getDateYYYY_MM_DD(series.getSeriesStartDate());
        String recordingTwoTitle = recording.getTitle() + " - " + DateFormatUtils.getDateYYYY_MM_DD(DateUtils.addDays(series.getSeriesStartDate(), 7));
        recordingsPage.waitForRecordingOnRow(driver, recordingTwoTitle, 2, pageLoadTimeout);
        recordingsPage.waitForRecordingOnRow(driver, recordingOneTitle, 4, pageLoadTimeout);

        // Edit recording title prior to capture, such that title exceeds max permitted characters at YouTube
        String recordingOneNewTitle = recordingOneTitle + " - Exceeds YouTube maximum character limitations for video by quite a lot of characters";
        recording.setTitle(recordingOneNewTitle);
        String recordingOneNewTitleTruncated = StringUtils.substring(recording.getTitle(), 0, 99);
        RecordingEditPage recordingEditPage = recordingsPage.clickEditLinkOnRow(driver, 4);
        recordingEditPage.editRecordingTitle(driver, recording);
        recordingEditPage.saveRecordingEdits(driver);
        recordingEditPage.waitForText(recordingEditPage.titleField, recording.getTitle(), pageLoadTimeout);
        recordingsPage.loadPage(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 4);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 4, TimeoutUtils.getAjaxWaitSec());
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 4, "Capturing");

        // Wait for workflow to reach "Delayed" status.  Cancel delay and wait for workflow to reach "Finished" status.
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 4, DateFormatUtils.getDayAndDate(DateUtils.addDays(recording.getRecordingDate(),
                recording.getDelay().getDelayDaysNumber())) + " - 00:00");
        recordingsPage.clickCancelPublishDelayLinkOnRow(driver, 4);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 4, "Finished");

        // Verify YouTube distribution for published recording
        YouTubePage youTube = new YouTubePage(driver);
        youTube.logInViaSpa(driver, calNetPage);
        youTube.loadVideo(driver, recording);
        String youTubeVideoId = youTube.getYouTubeVideoId(driver);
        TestUtils.verifyEquals(verificationErrors, recordingOneNewTitleTruncated, youTube.videoTitle.getText());
        TestUtils.verifyEquals(verificationErrors, series.getSeriesName() + "\n" + series.getCourseTitle() + " - " + StringUtils.join(recording.getInstructors(), ", ")
                + "\n" + series.getAvailability().getLicense().getLicenseDescription(), youTube.videoDesc.getText());
        youTube.loadVideoEditPage(driver, youTubeVideoId);
        youTube.waitForYouTubePrivacySelectValue(driver, youTube.privacySelect, youTube.getPrivacySelect(),
                series.getAvailability().getYouTubeAvailability());
        youTube.triggerAndAcceptYouTubeAlert(driver);

        // Verify warehouse JSON
        WarehouseJsonPage json = new WarehouseJsonPage(driver);
        JSONObject course = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(course));
        JSONObject lecture = json.waitForCourseRecordingVideoId(driver, series, youTubeVideoId);
        TestUtils.verifyEquals(verificationErrors, RecordingUtils.getRecordingTitleJson(recording.getTitle()), json.getRecordingTitle(lecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(lecture));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, youTubeVideoId, verificationErrors);

        // Verify warehouse DB
        WarehouseDatabase db = WarehouseDatabaseImpl.getInstance();
        WarehouseCourse warehouseCourse = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourse.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourse.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, warehouseCourse.getRecordingList().get(0).getYouTubeVideoId());
        int originalRecordingId = warehouseCourse.getRecordingList().get(0).getId();

        /*
            Retract recording
         */

        recordingsPage.loadPage(driver);
        ArchivesPage archivesPage = recordingsPage.clickArchivesTab(driver);
        archivesPage.searchForRecording(driver, recording, 1);
        archivesPage.retractRecording(driver, 1);
        archivesPage.clickRecordingsTab(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 5);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 5, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 5, "Finished");

        // Verify no YouTube distribution
        youTube.loadVideoManagerPage(driver);
        TestUtils.verifyFalse(verificationErrors, youTube.isLinkPresentByText(driver, recordingOneNewTitleTruncated));

        // Verify warehouse JSON - recording is gone
        json.waitForRecordingRetraction(driver, series, youTubeVideoId);
        JSONObject courseWithRetraction = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRetraction));

        // Verify warehouse DB
        WarehouseCourse warehouseCourseRetracted = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRetracted.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyTrue(verificationErrors, warehouseCourseRetracted.getRecordingList().isEmpty());

        /*
            Republish recording
         */

        recordingsPage.loadPage(driver);
        recordingsPage.clickArchivesTab(driver);
        archivesPage.searchForRecording(driver, recording, 1);
        archivesPage.republishWithTrim(driver, 1);
        archivesPage.clickRecordingsTab(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 6);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 6, TimeoutUtils.getAjaxWaitSec());
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 6, "On Hold");

        // Trim recording
        ReviewTrimPage reviewTrimPage = recordingsPage.clickReviewTrimLinkOnRow(driver, 6);
        recording.setDelay(PublishDelay.ZERO_DAYS);
        reviewTrimPage.editPublishDelay(driver, recording);
        reviewTrimPage.clickContinue(driver);

        // Wait for workflow to reach "Finished" status
        recordingsPage.clickHomepageLink(driver);
        homePage.clickAdminLink(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 6);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 6, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 6, "Finished");

        // Verify YouTube distribution
        youTube.loadVideo(driver, recording);
        String republishedYouTubeVideoId = youTube.getYouTubeVideoId(driver);
        TestUtils.verifyEquals(verificationErrors, recordingOneNewTitleTruncated, youTube.videoTitle.getText());
        TestUtils.verifyEquals(verificationErrors, series.getSeriesName() + "\n" + series.getCourseTitle() + " - " + StringUtils.join(recording.getInstructors(), ", ")
                + "\n" + series.getAvailability().getLicense().getLicenseDescription(), youTube.videoDesc.getText());
        youTube.loadVideoEditPage(driver, republishedYouTubeVideoId);
        youTube.waitForYouTubePrivacySelectValue(driver, youTube.privacySelect, youTube.getPrivacySelect(),
                series.getAvailability().getYouTubeAvailability());
        youTube.triggerAndAcceptYouTubeAlert(driver);

        // Verify warehouse JSON
        JSONObject republishedLecture = json.waitForCourseRecordingVideoId(driver, series, republishedYouTubeVideoId);
        TestUtils.verifyEquals(verificationErrors, RecordingUtils.getRecordingTitleJson(recording.getTitle()), json.getRecordingTitle(republishedLecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(republishedLecture));
        JSONObject courseWithRepublish = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRepublish));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, republishedYouTubeVideoId, verificationErrors);

        // Verify warehouse DB
        WarehouseCourse warehouseCourseRepublished = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRepublished.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourseRepublished.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, republishedYouTubeVideoId, warehouseCourseRepublished.getRecordingList().get(0).getYouTubeVideoId());
        int republishRecordingId = warehouseCourseRepublished.getRecordingList().get(0).getId();
        TestUtils.verifyNotEquals(verificationErrors, originalRecordingId, republishRecordingId);

        /*
            Retract recording again
         */

        recordingsPage.loadPage(driver);
        recordingsPage.clickArchivesTab(driver);
        archivesPage.searchForRecording(driver, recording, 1);
        archivesPage.retractRecording(driver, 1);
        archivesPage.clickRecordingsTab(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 7);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 7, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 7, "Finished");

        // Verify no YouTube distribution
        youTube.loadVideoManagerPage(driver);
        TestUtils.verifyFalse(verificationErrors, youTube.isLinkPresentByText(driver, recordingOneNewTitleTruncated));

        // Verify warehouse JSON - neither recording is present
        json.waitForRecordingRetraction(driver, series, youTubeVideoId);
        json.waitForRecordingRetraction(driver, series, republishedYouTubeVideoId);
        JSONObject courseWithRetractionRedux = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRetractionRedux));

        // Verify warehouse DB
        WarehouseCourse warehouseCourseRetractedAgain = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRetractedAgain.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyTrue(verificationErrors, warehouseCourseRetractedAgain.getRecordingList().isEmpty());
    }
}
