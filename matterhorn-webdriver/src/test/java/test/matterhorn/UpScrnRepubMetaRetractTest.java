package test.matterhorn;

import domain.*;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import page.CalNetAuthPage;
import page.matterhorn.*;
import page.SalesforcePage;
import page.YouTubePage;
import util.*;
import warehouse.db.WarehouseDatabase;
import warehouse.db.WarehouseDatabaseImpl;
import warehouse.domain.CourseKey;
import warehouse.domain.WarehouseCourse;

/**
 * @author Paul Farestveit
 */
public class UpScrnRepubMetaRetractTest {

    /**
     * Tests the following workflow:
     * - Course:
     * -- uploaded screencast recording
     * -- copyright license, students-only availability
     * - Initial Publish:
     * -- no publish delay
     * -- no review/trim
     * - Republish Metadata:
     * -- edit recording title
     * - Retract
     */

    private WebDriver driver;
    private final String testId = TestUtils.getTestId();
    private final StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        this.driver = WebDriverUtils.getWebDriver();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        TestUtils.writeTestResults(this.getClass(), testId, verificationErrors);
    }

    @Test
    public void testUploadScreenUnlistRepubMetaRetract() throws Exception {

        HomePage homePage = new HomePage(driver);
        CalNetAuthPage calNetPage = new CalNetAuthPage(driver);
        RecordingsPage recordingsPage = homePage.logIn(driver, calNetPage);
        recordingsPage.deleteUpcomingRecordings(driver);

        /*
            Create new Salesforce semester project and new course project.  Generate series via sign-up form.
         */

        int pageLoadTimeout = TimeoutUtils.getPageLoadWaitSec();

        Instructor[] instructors = {Instructor.TEST_INSTRUCTOR_1, Instructor.TEST_GSI};
        Series series = SeriesUtils.generateUniqueSeries(this.getClass(), testId, instructors, CaptureAgent.SCREENCAST_AGENT, Inputs.SCREENCAST, PublishDelay.ZERO_DAYS);
        Recording recording = RecordingUtils.generateSeriesRecording(this.getClass(), series);
        SalesforceSemesterProject salesforceSemesterProject = SalesforceUtils.generateSalesforceSemesterProject(series);

        SalesforcePage salesforce = new SalesforcePage(driver);
        salesforce.setUpSalesforceProjects(driver, salesforceSemesterProject, series);
        series.setSeriesName(SeriesUtils.getSeriesTitle(series));

        SignUpPage signUpPage = new SignUpPage(driver);
        signUpPage.loadCompleteSubmit(driver, series);
        SeriesInfoPage seriesInfoPage = new SeriesInfoPage(driver);
        seriesInfoPage.loadPage(driver, series);
        seriesInfoPage.waitForText(seriesInfoPage.seriesAvailability, series.getAvailability().getMatterhornAvailability(), pageLoadTimeout);

        /*
            Upload recording for the series and wait for workflow to reach "On Hold" status
         */

        recordingsPage.loadPage(driver);
        RecordingUploadPage recordingUploadPage = recordingsPage.clickUploadRecordingButton(driver);
        recordingUploadPage.inputRecordingMetadata(driver, recording, true);
        // In the following upload step, the audio goes where the video usually does due to WCT-5114
        recordingUploadPage.inputMultipleFiles(driver, FileUploadUtils.getScreenUploadFileName(), FileUploadUtils.getAudioUploadFileName(), null);
        recordingUploadPage.submitUpload(driver);
        recordingUploadPage.clickBackToRecordingsLink(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 3);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 3, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 3, "Finished");

        // Verify YouTube distribution
        YouTubePage youTube = new YouTubePage(driver);
        youTube.logInViaSpa(driver, calNetPage);
        String youTubeVideoId = youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);

        // Verify warehouse JSON
        WarehouseJsonPage json = new WarehouseJsonPage(driver);
        JSONObject course = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(course));
        JSONObject lecture = json.waitForCourseRecordingVideoId(driver, series, youTubeVideoId);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), json.getRecordingTitle(lecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(lecture));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, youTubeVideoId, verificationErrors);

        // Verify warehouse DB
        WarehouseDatabase db = WarehouseDatabaseImpl.getInstance();
        WarehouseCourse warehouseCourse = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourse.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourse.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, warehouseCourse.getRecordingList().get(0).getYouTubeVideoId());
        int originalRecordingId = warehouseCourse.getRecordingList().get(0).getId();

        /*
            Edit captured recording title on archive tab, then republish metadata
         */

        String oldTitle = recording.getTitle();
        String newTitle = recording.getTitle() + "-edited";
        recording.setTitle(newTitle);
        recordingsPage.loadPage(driver);
        ArchivesPage archivesPage = recordingsPage.clickArchivesTab(driver);
        archivesPage.editAndRepublishMetadata(driver, recording);
        archivesPage.clickHomepageLink(driver);
        homePage.clickAdminLink(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 4);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 4, pageLoadTimeout);

        // Wait for workflow to reach "Finished" status
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 4, "Finished");
        TestUtils.verifyEquals(verificationErrors, newTitle, recordingsPage.recordingTitle(driver, 4).getText());
        TestUtils.verifyEquals(verificationErrors, StringUtils.join(recording.getInstructors(), ", "), recordingsPage.recordingPresenter(driver, 4).getText());
        TestUtils.verifyEquals(verificationErrors, recording.getSeriesTitle(), recordingsPage.recordingSeries(driver, 4).getText());
        TestUtils.verifyEquals(verificationErrors, "", recordingsPage.recordingCaptureAgent(driver, 4).getText());

        // Verify YouTube distribution - that is, verify that the title metadata edit IS reflected at YouTube
        String newTitleVideoId = youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, newTitleVideoId);

        // Verify warehouse JSON - new metadata is present
        JSONObject republishMetaLecture = json.waitForCourseRecordingTitle(driver, series, recording.getTitle());
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, json.getRecordingYouTubeVideoId(republishMetaLecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(republishMetaLecture));
        JSONObject courseWithRepublishedMetadata = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRepublishedMetadata));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, youTubeVideoId, verificationErrors);

        // Verify warehouse DB
        WarehouseCourse warehouseCourseRepubMeta = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRepubMeta.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourseRepubMeta.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, warehouseCourseRepubMeta.getRecordingList().get(0).getYouTubeVideoId());
        int repubMetaRecordingId = warehouseCourseRepubMeta.getRecordingList().get(0).getId();
        TestUtils.verifyEquals(verificationErrors, originalRecordingId, repubMetaRecordingId);

        /*
            Retract recording
         */

        recordingsPage.loadPage(driver);
        recordingsPage.clickArchivesTab(driver);
        archivesPage.searchForRecording(driver, recording, 1);
        archivesPage.retractRecording(driver, 1);
        archivesPage.clickRecordingsTab(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 5);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 5, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 5, "Finished");

        // Verify no YouTube distribution of anything except the playlist
        youTube.loadVideoManagerPage(driver);
        TestUtils.verifyFalse(verificationErrors, youTube.isLinkPresentByText(driver, newTitle));
        TestUtils.verifyFalse(verificationErrors, youTube.isLinkPresentByText(driver, oldTitle));

        // Verify warehouse JSON
        json.waitForRecordingRetraction(driver, series, youTubeVideoId);
        JSONObject courseWithRetraction = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRetraction));

        // Verify warehouse DB
        WarehouseCourse warehouseCourseRetracted = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRetracted.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyTrue(verificationErrors, warehouseCourseRetracted.getRecordingList().isEmpty());
    }
}
