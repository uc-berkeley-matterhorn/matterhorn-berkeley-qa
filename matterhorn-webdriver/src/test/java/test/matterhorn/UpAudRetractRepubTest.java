package test.matterhorn;

import domain.*;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import page.CalNetAuthPage;
import page.matterhorn.*;
import page.SalesforcePage;
import page.YouTubePage;
import util.*;
import warehouse.db.WarehouseDatabase;
import warehouse.db.WarehouseDatabaseImpl;
import warehouse.domain.CourseKey;
import warehouse.domain.WarehouseCourse;

/**
 * @author Paul Farestveit
 */

public class UpAudRetractRepubTest {

    /**
     * Tests the following workflow:
     * - Course:
     * -- uploaded audio-only recording
     * - Initial Publish:
     * -- no publish delay
     * -- no review/trim
     * - Retract
     * - Republish:
     * -- no review/trim
     */

    private WebDriver driver;
    private final String testId = TestUtils.getTestId();
    private final StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        this.driver = WebDriverUtils.getWebDriver();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        TestUtils.writeTestResults(this.getClass(), testId, verificationErrors);
    }

    @Test
    public void testUpAudRetractRepub() throws Exception {

        HomePage homePage = new HomePage(driver);
        CalNetAuthPage calNetPage = new CalNetAuthPage(driver);
        RecordingsPage recordingsPage = homePage.logIn(driver, calNetPage);
        recordingsPage.deleteUpcomingRecordings(driver);

        /*
            Create new Salesforce semester project and new course project.  Generate series via sign-up form.
         */

        int pageLoadTimeout = TimeoutUtils.getPageLoadWaitSec();

        Instructor[] instructors = {Instructor.TEST_INSTRUCTOR_1, Instructor.TEST_GSI};
        Series series = SeriesUtils.generateUniqueSeries(this.getClass(), testId, instructors, CaptureAgent.VIDEO_AGENT, Inputs.AUDIO_ONLY, PublishDelay.ONE_DAY);
        Recording recording = RecordingUtils.generateSeriesRecording(this.getClass(), series);
        SalesforceSemesterProject salesforceSemesterProject = SalesforceUtils.generateSalesforceSemesterProject(series);

        SalesforcePage salesforce = new SalesforcePage(driver);
        salesforce.setUpSalesforceProjects(driver, salesforceSemesterProject, series);
        series.setSeriesName(SeriesUtils.getSeriesTitle(series));

        SignUpPage signUpPage = new SignUpPage(driver);
        signUpPage.loadCompleteSubmit(driver, series);
        SeriesInfoPage seriesInfoPage = new SeriesInfoPage(driver);
        seriesInfoPage.loadPage(driver, series);
        seriesInfoPage.waitForText(seriesInfoPage.seriesAvailability, series.getAvailability().getMatterhornAvailability(), pageLoadTimeout);

        /*
            Upload recording for the series
         */

        recordingsPage.loadPage(driver);
        RecordingUploadPage recordingUploadPage = recordingsPage.clickUploadRecordingButton(driver);
        recordingUploadPage.inputRecordingMetadata(driver, recording, true);
        recordingUploadPage.inputSingleFile(driver, FileUploadUtils.getAudioUploadFileName(), false);
        recordingUploadPage.submitUpload(driver);
        recordingUploadPage.clickBackToRecordingsLink(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 3);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 3, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 3, "Finished");

        // Verify YouTube distribution
        YouTubePage youTube = new YouTubePage(driver);
        youTube.logInViaSpa(driver, calNetPage);
        String youTubeVideoId = youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);

        // Verify warehouse JSON
        WarehouseJsonPage json = new WarehouseJsonPage(driver);
        JSONObject course = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(course));
        JSONObject lecture = json.waitForCourseRecordingVideoId(driver, series, youTubeVideoId);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), json.getRecordingTitle(lecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(lecture));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, youTubeVideoId, verificationErrors);

        // Verify warehouse DB
        WarehouseDatabase db = WarehouseDatabaseImpl.getInstance();
        WarehouseCourse warehouseCourse = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourse.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourse.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, warehouseCourse.getRecordingList().get(0).getYouTubeVideoId());
        int originalRecordingId = warehouseCourse.getRecordingList().get(0).getId();

        /*
            Retract recording
         */

        recordingsPage.loadPage(driver);
        ArchivesPage archivesPage = recordingsPage.clickArchivesTab(driver);
        archivesPage.searchForRecording(driver, recording, 1);
        archivesPage.retractRecording(driver, 1);
        archivesPage.clickRecordingsTab(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 4);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 4, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 4, "Finished");

        // Verify no YouTube distribution
        youTube.loadVideoManagerPage(driver);
        TestUtils.verifyFalse(verificationErrors, youTube.isLinkPresentByText(driver, recording.getTitle()));

        // Verify recording is removed from warehouse JSON
        json.waitForRecordingRetraction(driver, series, youTubeVideoId);
        JSONObject courseWithRetraction = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRetraction));

        // Verify warehouse DB - no recording
        WarehouseCourse warehouseCourseRetracted = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRetracted.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyTrue(verificationErrors, warehouseCourseRetracted.getRecordingList().isEmpty());

        /*
            Republish recording
         */

        recordingsPage.loadPage(driver);
        recordingsPage.clickArchivesTab(driver);
        archivesPage.searchForRecording(driver, recording, 1);
        archivesPage.republishWithoutTrim(driver, 1);
        archivesPage.clickRecordingsTab(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 5);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 5, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 5, "Finished");

        // Verify YouTube distribution - only the new recording is present
        youTube.loadVideo(driver, recording);
        String  newYouTubeVideoId = youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);
        TestUtils.verifyNotEquals(verificationErrors, youTubeVideoId, newYouTubeVideoId);

        // Verify recording is back in the warehouse JSON
        JSONObject republishedLecture = json.waitForCourseRecordingVideoId(driver, series, newYouTubeVideoId);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), json.getRecordingTitle(republishedLecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(republishedLecture));
        JSONObject courseWithRepublish = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRepublish));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, newYouTubeVideoId, verificationErrors);

        // Verify warehouse DB - new version is present
        WarehouseCourse warehouseCourseRepublished = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRepublished.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyEquals(verificationErrors, 1, warehouseCourseRepublished.getRecordingList().size());
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourseRepublished.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, newYouTubeVideoId, warehouseCourseRepublished.getRecordingList().get(0).getYouTubeVideoId());
        int republishRecordingId = warehouseCourseRepublished.getRecordingList().get(0).getId();
        TestUtils.verifyNotEquals(verificationErrors, originalRecordingId, republishRecordingId);
    }

}
