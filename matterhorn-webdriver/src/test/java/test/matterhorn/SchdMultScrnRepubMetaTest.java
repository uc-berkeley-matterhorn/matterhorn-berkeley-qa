package test.matterhorn;

import domain.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import page.CalNetAuthPage;
import page.matterhorn.*;
import page.SalesforcePage;
import page.YouTubePage;
import util.*;
import warehouse.db.WarehouseDatabase;
import warehouse.db.WarehouseDatabaseImpl;
import warehouse.domain.CourseKey;
import warehouse.domain.WarehouseCourse;

/**
 * @author Paul Farestveit
 */

public class SchdMultScrnRepubMetaTest {

    /**
     * Tests the following workflow:
     * - Course:
     * -- captured recording on screencast agent, screencast input
     * - Initial Publish:
     * -- no publish delay, no trim
     * - Republish Metadata:
     * -- edit recording title
     * - Republish:
     * -- no trim
     */

    private WebDriver driver;
    private final String testId = TestUtils.getTestId();
    private final StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        this.driver = WebDriverUtils.getWebDriver();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        TestUtils.writeTestResults(this.getClass(), testId, verificationErrors);
    }

    @Test
    public void testSchedMultScreenRepubMeta() throws Exception {

        HomePage homePage = new HomePage(driver);
        CalNetAuthPage calNetPage = new CalNetAuthPage(driver);
        RecordingsPage recordingsPage = homePage.logIn(driver, calNetPage);
        recordingsPage.deleteUpcomingRecordings(driver);

        /*
          Create new Salesforce semester project and new course project.  Generate series via sign-up form.
         */

        int pageLoadTimeout = TimeoutUtils.getPageLoadWaitSec();

        Instructor[] instructors = {Instructor.TEST_INSTRUCTOR_1, Instructor.TEST_GSI};
        Series series = SeriesUtils.generateUniqueSeries(this.getClass(), testId, instructors, CaptureAgent.SCREENCAST_AGENT, Inputs.SCREENCAST, PublishDelay.ZERO_DAYS);
        SalesforceSemesterProject salesforceSemesterProject = SalesforceUtils.generateSalesforceSemesterProject(series);

        SalesforcePage salesforce = new SalesforcePage(driver);
        salesforce.setUpSalesforceProjects(driver, salesforceSemesterProject, series);
        series.setSeriesName(SeriesUtils.getSeriesTitle(series));

        SignUpPage signUpPage = new SignUpPage(driver);
        signUpPage.loadCompleteSubmit(driver, series);
        SeriesInfoPage seriesInfoPage = new SeriesInfoPage(driver);
        seriesInfoPage.loadPage(driver, series);
        seriesInfoPage.waitForText(seriesInfoPage.seriesAvailability, series.getAvailability().getMatterhornAvailability(), pageLoadTimeout);

        /*
            Schedule two additional recordings for the series.  Capture one and wait for workflow to reach "Finished" status.
         */

        Recording recording = RecordingUtils.generateSeriesRecording(this.getClass(), series);
        recordingsPage.loadPage(driver);
        RecordingsSchedulerPage schedulerPage = recordingsPage.clickScheduleRecordingButton(driver);
        schedulerPage.selectMultipleRecordings(driver);
        schedulerPage.inputRecordingMetadata(driver, recording);
        schedulerPage.scheduleRecurringCaptures(driver, recording);
        schedulerPage.addPublishDelayMultiRecordings(driver, series);
        schedulerPage.submitScheduledRecording(driver);
        recordingsPage.loadPage(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 4);
        String recordingOneTitle = recording.getTitle() + " - " + DateFormatUtils.getDateYYYY_MM_DD(series.getSeriesStartDate());
        String recordingTwoTitle = recording.getTitle() + " - " + DateFormatUtils.getDateYYYY_MM_DD(DateUtils.addDays(series.getSeriesStartDate(), 7));
        recordingsPage.waitForRecordingOnRow(driver, recordingTwoTitle, 2, pageLoadTimeout);
        recordingsPage.waitForRecordingOnRow(driver, recordingOneTitle, 4, pageLoadTimeout);
        recording.setTitle(recordingOneTitle);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 4, "Capturing");
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 4, "Finished");

        // Verify YouTube distribution for published recording
        YouTubePage youTube = new YouTubePage(driver);
        youTube.logInViaSpa(driver, calNetPage);
        String youTubeVideoId = youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);

        // Verify warehouse JSON
        WarehouseJsonPage json = new WarehouseJsonPage(driver);
        JSONObject course = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(course));
        JSONObject lecture = json.waitForCourseRecordingVideoId(driver, series, youTubeVideoId);
        TestUtils.verifyEquals(verificationErrors, RecordingUtils.getRecordingTitleJson(recording.getTitle()), json.getRecordingTitle(lecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(lecture));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, youTubeVideoId, verificationErrors);

        // Verify warehouse DB
        WarehouseDatabase db = WarehouseDatabaseImpl.getInstance();
        WarehouseCourse warehouseCourse = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourse.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourse.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, warehouseCourse.getRecordingList().get(0).getYouTubeVideoId());
        int originalRecordingId = warehouseCourse.getRecordingList().get(0).getId();

        /*
            Change recording title on archive tab, then republish metadata
         */

        String recordingOneNewTitle = recording.getTitle() + " - edited";
        recording.setTitle(recordingOneNewTitle);
        recordingsPage.loadPage(driver);
        ArchivesPage archivesPage = recordingsPage.clickArchivesTab(driver);
        archivesPage.editAndRepublishMetadata(driver, recording);
        archivesPage.clickHomepageLink(driver);
        homePage.clickAdminLink(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 5);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 5, pageLoadTimeout);
        TestUtils.verifyEquals(verificationErrors, StringUtils.join(recording.getInstructors(), ", "), recordingsPage.recordingPresenter(driver, 4).getText());
        TestUtils.verifyEquals(verificationErrors, recording.getSeriesTitle(), recordingsPage.recordingSeries(driver, 4).getText());
        TestUtils.verifyEquals(verificationErrors, series.getCaptureAgent().getCaptureAgentName(), recordingsPage.recordingCaptureAgent(driver, 4).getText());

        // Wait for workflow to reach "Finished" status
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 5, "Finished");

        // Verify YouTube distribution, showing new title
        youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);

        // Verify warehouse JSON - same YouTube video ID but new title
        JSONObject republishMetaLecture = json.waitForCourseRecordingTitle(driver, series, RecordingUtils.getRecordingTitleJson(recording.getTitle()));
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, json.getRecordingYouTubeVideoId(republishMetaLecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(republishMetaLecture));
        JSONObject courseWithRepublishedMetadata = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRepublishedMetadata));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, youTubeVideoId, verificationErrors);

        // Verify warehouse DB
        WarehouseCourse warehouseCourseRepubMeta = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRepubMeta.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourseRepubMeta.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, warehouseCourseRepubMeta.getRecordingList().get(0).getYouTubeVideoId());
        int repubMetaRecordingId = warehouseCourseRepubMeta.getRecordingList().get(0).getId();
        TestUtils.verifyEquals(verificationErrors, originalRecordingId, repubMetaRecordingId);

    }
}
