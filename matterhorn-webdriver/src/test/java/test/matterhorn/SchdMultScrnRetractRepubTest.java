package test.matterhorn;

import domain.*;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import page.CalNetAuthPage;
import page.matterhorn.*;
import page.SalesforcePage;
import page.YouTubePage;
import util.*;
import warehouse.db.WarehouseDatabase;
import warehouse.db.WarehouseDatabaseImpl;
import warehouse.domain.CourseKey;
import warehouse.domain.WarehouseCourse;

/**
 * @author Paul Farestveit
 */
public class SchdMultScrnRetractRepubTest {

    /**
     * Tests the following workflow:
     * - Course:
     * -- captured recording on screencast agent, screencast input
     * - Initial Publish:
     * -- no publish delay
     * -- review/trim:
     * --- trim recording
     * --- make no metadata edits
     * - Republish:
     * -- edit series availability prior to republish, which should have no impact on recording availability
     * -- review/trim:
     * --- trim recording
     * --- edit recording title
     * - Retract
     */

    private WebDriver driver;
    private final String testId = TestUtils.getTestId();
    private final StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        this.driver = WebDriverUtils.getWebDriver();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        TestUtils.writeTestResults(this.getClass(), testId, verificationErrors);
    }

    @Test
    public void testSchedMultScreenRetractRepub() throws Exception {

        HomePage homePage = new HomePage(driver);
        CalNetAuthPage calNetPage = new CalNetAuthPage(driver);
        RecordingsPage recordingsPage = homePage.logIn(driver, calNetPage);
        recordingsPage.deleteUpcomingRecordings(driver);

        /*
          Create new Salesforce semester project and new course project.  Generate series via sign-up form.
         */

        int pageLoadTimeout = TimeoutUtils.getPageLoadWaitSec();

        Instructor[] instructors = {Instructor.TEST_INSTRUCTOR_1, Instructor.TEST_GSI};
        Series series = SeriesUtils.generateUniqueSeries(this.getClass(), testId, instructors, CaptureAgent.SCREENCAST_AGENT, Inputs.SCREENCAST, PublishDelay.ZERO_DAYS);
        SalesforceSemesterProject salesforceSemesterProject = SalesforceUtils.generateSalesforceSemesterProject(series);

        SalesforcePage salesforce = new SalesforcePage(driver);
        salesforce.setUpSalesforceProjects(driver, salesforceSemesterProject, series);
        series.setSeriesName(SeriesUtils.getSeriesTitle(series));

        SignUpPage signUpPage = new SignUpPage(driver);
        signUpPage.loadCompleteSubmit(driver, series);
        SeriesInfoPage seriesInfoPage = new SeriesInfoPage(driver);
        seriesInfoPage.loadPage(driver, series);
        seriesInfoPage.waitForText(seriesInfoPage.seriesAvailability, series.getAvailability().getMatterhornAvailability(), pageLoadTimeout);

        /*
          Schedule two additional recordings for the series.  Capture one and wait for workflow to reach "On Hold" status.
         */

        Recording recording = RecordingUtils.generateSeriesRecording(this.getClass(), series);
        recordingsPage.loadPage(driver);
        RecordingsSchedulerPage schedulerPage = recordingsPage.clickScheduleRecordingButton(driver);
        schedulerPage.selectMultipleRecordings(driver);
        schedulerPage.inputRecordingMetadata(driver, recording);
        schedulerPage.scheduleRecurringCaptures(driver, recording);
        schedulerPage.chooseReviewTrim(driver);
        schedulerPage.addPublishDelayMultiRecordings(driver, series);
        schedulerPage.submitScheduledRecording(driver);
        recordingsPage.loadPage(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 4);
        String recordingOneTitle = recording.getTitle() + " - " + DateFormatUtils.getDateYYYY_MM_DD(series.getSeriesStartDate());
        String recordingTwoTitle = recording.getTitle() + " - " + DateFormatUtils.getDateYYYY_MM_DD(DateUtils.addDays(series.getSeriesStartDate(), 7));
        recordingsPage.waitForRecordingOnRow(driver, recordingTwoTitle, 2, pageLoadTimeout);
        recordingsPage.waitForRecordingOnRow(driver, recordingOneTitle, 4, pageLoadTimeout);
        recording.setTitle(recordingOneTitle);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 4, "Capturing");
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 4, "On Hold");

        // Review recording, trim, but make no metadata edits
        ReviewTrimPage reviewTrimPage = recordingsPage.clickReviewTrimLinkOnRow(driver, 4);
        reviewTrimPage.moveInpointForward(driver, 3);
        reviewTrimPage.moveOutpointBack(driver, 3);
        reviewTrimPage.clickContinue(driver);

        // Wait for workflow to reach "Finished" status
        recordingsPage.loadPage(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 4);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 4, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 4, "Finished");

        // Verify YouTube distribution for published recording
        YouTubePage youTube = new YouTubePage(driver);
        youTube.logInViaSpa(driver, calNetPage);
        String youTubeVideoId = youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);

        // Verify warehouse JSON
        WarehouseJsonPage json = new WarehouseJsonPage(driver);
        JSONObject course = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(course));
        JSONObject lecture = json.waitForCourseRecordingVideoId(driver, series, youTubeVideoId);
        TestUtils.verifyEquals(verificationErrors, RecordingUtils.getRecordingTitleJson(recording.getTitle()), json.getRecordingTitle(lecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(lecture));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, youTubeVideoId, verificationErrors);

        // Verify warehouse DB
        WarehouseDatabase db = WarehouseDatabaseImpl.getInstance();
        WarehouseCourse warehouseCourse = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourse.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourse.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, warehouseCourse.getRecordingList().get(0).getYouTubeVideoId());
        int originalRecordingId = warehouseCourse.getRecordingList().get(0).getId();

        /*
            Retract recording
         */

        recordingsPage.loadPage(driver);
        ArchivesPage archivesPage = recordingsPage.clickArchivesTab(driver);
        archivesPage.searchForRecording(driver, recording, 1);
        archivesPage.retractRecording(driver, 1);
        archivesPage.clickRecordingsTab(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 5);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 5, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 5, "Finished");

        // Verify no YouTube distribution
        youTube.loadVideoManagerPage(driver);
        TestUtils.verifyFalse(verificationErrors, youTube.isLinkPresentByText(driver, recording.getTitle()));

        // Verify warehouse JSON - recording is gone
        json.waitForRecordingRetraction(driver, series, youTubeVideoId);
        JSONObject courseWithRetraction = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRetraction));

        // Verify warehouse DB
        WarehouseCourse warehouseCourseRetracted = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRetracted.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyTrue(verificationErrors, warehouseCourseRetracted.getRecordingList().isEmpty());

        /*
            Republish
         */

        recordingsPage.loadPage(driver);
        recordingsPage.clickArchivesTab(driver);
        archivesPage.searchForRecording(driver, recording, 1);
        archivesPage.republishWithoutTrim(driver, 1);
        archivesPage.clickRecordingsTab(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 6);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 6, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 6, "Finished");

        // Verify YouTube distribution
        String republishedYouTubeVideoId = youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);

        // Verify warehouse JSON
        JSONObject republishedLecture = json.waitForCourseRecordingVideoId(driver, series, republishedYouTubeVideoId);
        TestUtils.verifyEquals(verificationErrors, RecordingUtils.getRecordingTitleJson(recording.getTitle()), json.getRecordingTitle(republishedLecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(republishedLecture));
        JSONObject courseWithRepublish = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRepublish));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, republishedYouTubeVideoId, verificationErrors);

        // Verify warehouse DB
        WarehouseCourse warehouseCourseRepublished = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRepublished.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyEquals(verificationErrors, 1, warehouseCourse.getRecordingList().size());
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourseRepublished.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, republishedYouTubeVideoId, warehouseCourseRepublished.getRecordingList().get(0).getYouTubeVideoId());
        int republishRecordingId = warehouseCourseRepublished.getRecordingList().get(0).getId();
        TestUtils.verifyNotEquals(verificationErrors, originalRecordingId, republishRecordingId);

    }
}
