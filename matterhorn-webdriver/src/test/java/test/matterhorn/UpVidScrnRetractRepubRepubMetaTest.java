package test.matterhorn;

import domain.*;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import page.CalNetAuthPage;
import page.matterhorn.*;
import page.SalesforcePage;
import page.YouTubePage;
import util.*;
import warehouse.db.WarehouseDatabase;
import warehouse.db.WarehouseDatabaseImpl;
import warehouse.domain.CourseKey;
import warehouse.domain.WarehouseCourse;
import java.util.Date;

/**
 * @author Paul Farestveit
 */
public class UpVidScrnRetractRepubRepubMetaTest {

    /**
     * Tests the following workflow:
     * - Course:
     * -- uploaded video + screencast recording
     * -- copyright license, students-only availability
     * - Initial Publish:
     * -- no publish delay
     * -- no review/trim
     * - Retract
     * - Republish Metadata:
     * -- edit instructors (which has no effect)
     * - Republish:
     * -- review/trim:
     * --- trim recording
     * --- no metadata edits
     * - Republish Metadata:
     * -- edit recording title
     */

    private WebDriver driver;
    private final String testId = TestUtils.getTestId();
    private final StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        this.driver = WebDriverUtils.getWebDriver();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        TestUtils.writeTestResults(this.getClass(), testId, verificationErrors);
    }

    @Test
    public void testUploadVideoScreenUnlistRetractRepubRepubMeta() throws Exception {

        HomePage homePage = new HomePage(driver);
        CalNetAuthPage calNetPage = new CalNetAuthPage(driver);
        RecordingsPage recordingsPage = homePage.logIn(driver, calNetPage);
        recordingsPage.deleteUpcomingRecordings(driver);

        /*
            Create new Salesforce semester project and new course project.  Generate series via sign-up form.
         */

        int pageLoadTimeout = TimeoutUtils.getPageLoadWaitSec();

        Instructor[] origInstructors = {Instructor.TEST_INSTRUCTOR_1, Instructor.TEST_GSI};
        Series series = SeriesUtils.generateUniqueSeries(this.getClass(), testId, origInstructors, CaptureAgent.VIDEO_AGENT, Inputs.VIDEO_SCREEN, PublishDelay.ZERO_DAYS);
        Recording recording = RecordingUtils.generateSeriesRecording(this.getClass(), series);
        SalesforceSemesterProject salesforceSemesterProject = SalesforceUtils.generateSalesforceSemesterProject(series);

        SalesforcePage salesforce = new SalesforcePage(driver);
        salesforce.setUpSalesforceProjects(driver, salesforceSemesterProject, series);
        series.setSeriesName(SeriesUtils.getSeriesTitle(series));

        SignUpPage signUpPage = new SignUpPage(driver);
        signUpPage.loadCompleteSubmit(driver, series);
        SeriesInfoPage seriesInfoPage = new SeriesInfoPage(driver);
        seriesInfoPage.loadPage(driver, series);
        seriesInfoPage.waitForText(seriesInfoPage.seriesAvailability, series.getAvailability().getMatterhornAvailability(), pageLoadTimeout);

        /*
            Upload recording for the series and wait for the workflow to reach "Finished" status
         */

        recordingsPage.loadPage(driver);
        RecordingUploadPage recordingUploadPage = recordingsPage.clickUploadRecordingButton(driver);
        recordingUploadPage.inputRecordingMetadata(driver, recording, true);
        recordingUploadPage.inputMultipleFiles(driver, FileUploadUtils.getScreenUploadFileName(), FileUploadUtils.getVideoUploadFileName(),
                FileUploadUtils.getAudioUploadFileName());
        recordingUploadPage.submitUpload(driver);
        recordingUploadPage.clickBackToRecordingsLink(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 3);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 3, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 3, "Finished");

        // Verify YouTube distribution
        YouTubePage youTube = new YouTubePage(driver);
        youTube.logInViaSpa(driver, calNetPage);
        String youTubeVideoId = youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);

        // Verify warehouse JSON
        WarehouseJsonPage json = new WarehouseJsonPage(driver);
        JSONObject course = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(course));
        JSONObject lecture = json.waitForCourseRecordingVideoId(driver, series, youTubeVideoId);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), json.getRecordingTitle(lecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(lecture));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, youTubeVideoId, verificationErrors);

        // Verify warehouse DB
        WarehouseDatabase db = WarehouseDatabaseImpl.getInstance();
        WarehouseCourse warehouseCourse = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourse.verifyCourseData(verificationErrors, series, origInstructors);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourse.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, warehouseCourse.getRecordingList().get(0).getYouTubeVideoId());
        int originalRecordingId = warehouseCourse.getRecordingList().get(0).getId();

        /*
            Retract recording
         */

        recordingsPage.loadPage(driver);
        ArchivesPage archivesPage = recordingsPage.clickArchivesTab(driver);
        archivesPage.searchForRecording(driver, recording, 1);
        archivesPage.retractRecording(driver, 1);
        archivesPage.clickRecordingsTab(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 4);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 4, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 4, "Finished");

        // Verify no YouTube distribution
        youTube.loadVideoManagerPage(driver);
        TestUtils.verifyFalse(verificationErrors, youTube.isLinkPresentByText(driver, recording.getTitle()));

        // Verify warehouse JSON - no recording
        json.waitForRecordingRetraction(driver, series, youTubeVideoId);
        JSONObject courseWithRetraction = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRetraction));

        // Verify warehouse DB - no recording
        WarehouseCourse warehouseCourseRetracted = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRetracted.verifyCourseData(verificationErrors, series, origInstructors);
        TestUtils.verifyTrue(verificationErrors, warehouseCourseRetracted.getRecordingList().isEmpty());

        /*
            Edit captured recording instructors on archive tab and republish metadata
         */

        Instructor[] newInstructors = {Instructor.TEST_INSTRUCTOR_2, Instructor.TEST_GSI};
        recording.setInstructors(newInstructors);
        recordingsPage.loadPage(driver);
        recordingsPage.clickArchivesTab(driver);
        archivesPage.searchForRecording(driver, recording, 1);
        archivesPage.editAndRepublishMetadata(driver, recording);
        archivesPage.clickHomepageLink(driver);
        homePage.clickAdminLink(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 5);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 5, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 5, "Finished");
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), recordingsPage.recordingTitle(driver, 4).getText());
        TestUtils.verifyEquals(verificationErrors, recording.getSeriesTitle(), recordingsPage.recordingSeries(driver, 4).getText());
        TestUtils.verifyEquals(verificationErrors, "", recordingsPage.recordingCaptureAgent(driver, 4).getText());

        /*
            Republish recording and wait for workflow to reach "On Hold" status
         */

        recordingsPage.clickArchivesTab(driver);
        archivesPage.searchForRecording(driver, recording, 1);
        archivesPage.republishWithoutTrim(driver, 1);
        archivesPage.clickRecordingsTab(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 6);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 6, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 6, "Finished");

        // Verify YouTube distribution
        String republishedYouTubeVideoId = youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);

        // Verify warehouse JSON
        JSONObject republishedLecture = json.waitForCourseRecordingVideoId(driver, series, republishedYouTubeVideoId);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), json.getRecordingTitle(republishedLecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(republishedLecture));
        JSONObject courseWithRepublish = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRepublish));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, republishedYouTubeVideoId, verificationErrors);

        // Verify warehouse DB
        WarehouseCourse warehouseCourseRepublished = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRepublished.verifyCourseData(verificationErrors, series, origInstructors);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourseRepublished.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, republishedYouTubeVideoId, warehouseCourseRepublished.getRecordingList().get(0).getYouTubeVideoId());
        int republishRecordingId = warehouseCourseRepublished.getRecordingList().get(0).getId();
        TestUtils.verifyNotEquals(verificationErrors, originalRecordingId, republishRecordingId);

        /*
            Edit captured recording title on archive tab, then republish metadata
         */

        String newTitle = recording.getTitle() + "-edited";
        recording.setTitle(newTitle);
        Date origRecordingDate = recording.getRecordingDate();
        Date yesterday = DateUtils.addDays(recording.getRecordingDate(), -1);
        recording.setRecordingDate(yesterday);
        recordingsPage.loadPage(driver);
        recordingsPage.clickArchivesTab(driver);
        archivesPage.editAndRepublishMetadata(driver, recording);
        archivesPage.clickHomepageLink(driver);
        homePage.clickAdminLink(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 7);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 7, pageLoadTimeout);

        // Wait for workflow to reach "Finished" status
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 7, "Finished");

        // Verify warehouse JSON - metadata updated but not recording date
        json.waitForCourseRecordingTitle(driver, series, recording.getTitle());
        TestUtils.verifyEquals(verificationErrors, republishedYouTubeVideoId, json.getRecordingYouTubeVideoId(republishedLecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(origRecordingDate), json.getRecordingStartUTC(republishedLecture));
        JSONObject courseWithRepublishedMetadata = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRepublishedMetadata));

        // Verify warehouse DB
        WarehouseCourse warehouseCourseRepubMeta = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRepubMeta.verifyCourseData(verificationErrors, series, origInstructors);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourseRepubMeta.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, republishedYouTubeVideoId, warehouseCourseRepubMeta.getRecordingList().get(0).getYouTubeVideoId());
        int repubMetaRecordingId = warehouseCourseRepubMeta.getRecordingList().get(0).getId();
        TestUtils.verifyEquals(verificationErrors, republishRecordingId, repubMetaRecordingId);

        // Verify YouTube distribution -  metadata updated
        youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);
        youTube.loadVideoEditPage(driver, youTubeVideoId);
        TestUtils.verifyEquals(verificationErrors, "Shared with berkeley.edu", youTube.sharing.getText());
    }
}
