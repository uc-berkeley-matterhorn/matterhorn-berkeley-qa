package test.matterhorn;

import domain.*;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import page.CalNetAuthPage;
import page.matterhorn.*;
import page.SalesforcePage;
import page.YouTubePage;
import util.*;
import warehouse.db.WarehouseDatabase;
import warehouse.db.WarehouseDatabaseImpl;
import warehouse.domain.CourseKey;
import warehouse.domain.WarehouseCourse;

/**
 * @author Paul Farestveit
 */
public class SchdOneVidScrnRepubMetaRetractTest {

    /**
     * Tests the following workflow:
     * - Course:
     * -- captured recording on video agent, video + screen input
     * - Initial Publish:
     * -- no publish delay
     * -- no review/trim
     * - Republish:
     * -- no review/trim
     * - Retract
     */

    private WebDriver driver;
    private final String testId = TestUtils.getTestId();
    private final StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        this.driver = WebDriverUtils.getWebDriver();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        TestUtils.writeTestResults(this.getClass(), testId, verificationErrors);
    }

    @Test
    public void testSchedOneVideoScreenRepubMetaRetract() throws Exception {

        HomePage homePage = new HomePage(driver);
        CalNetAuthPage calNetPage = new CalNetAuthPage(driver);
        RecordingsPage recordingsPage = homePage.logIn(driver, calNetPage);
        recordingsPage.deleteUpcomingRecordings(driver);

        /*
          Create new Salesforce semester project and new course project.  Generate series via sign-up form.
         */

        int pageLoadTimeout = TimeoutUtils.getPageLoadWaitSec();

        Instructor[] instructors = {Instructor.TEST_INSTRUCTOR_1, Instructor.TEST_GSI};
        Series series = SeriesUtils.generateUniqueSeries(this.getClass(), testId, instructors, CaptureAgent.VIDEO_AGENT, Inputs.VIDEO_SCREEN, PublishDelay.ZERO_DAYS);
        SalesforceSemesterProject salesforceSemesterProject = SalesforceUtils.generateSalesforceSemesterProject(series);

        SalesforcePage salesforce = new SalesforcePage(driver);
        salesforce.setUpSalesforceProjects(driver, salesforceSemesterProject, series);
        series.setSeriesName(SeriesUtils.getSeriesTitle(series));

        SignUpPage signUpPage = new SignUpPage(driver);
        signUpPage.loadCompleteSubmit(driver, series);
        SeriesInfoPage seriesInfoPage = new SeriesInfoPage(driver);
        seriesInfoPage.loadPage(driver, series);
        seriesInfoPage.waitForText(seriesInfoPage.seriesAvailability, series.getAvailability().getMatterhornAvailability(), pageLoadTimeout);

        /*
            Schedule one additional recording for the series
         */

        Recording recording = RecordingUtils.generateSeriesRecording(this.getClass(), series);
        recordingsPage.loadPage(driver);
        RecordingsSchedulerPage schedulerPage = recordingsPage.clickScheduleRecordingButton(driver);
        schedulerPage.selectSingleRecording(driver);
        schedulerPage.inputRecordingMetadata(driver, recording);
        schedulerPage.scheduleSingleCapture(driver, recording);
        schedulerPage.submitScheduledRecording(driver);
        recordingsPage.loadPage(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 3);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 3, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 3, "Capturing");
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 3, "Finished");

        // Verify YouTube distribution
        YouTubePage youTube = new YouTubePage(driver);
        youTube.logInViaSpa(driver, calNetPage);
        String youTubeVideoId = youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);

        // Verify warehouse JSON
        WarehouseJsonPage json = new WarehouseJsonPage(driver);
        JSONObject course = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(course));
        JSONObject lecture = json.waitForCourseRecordingVideoId(driver, series, youTubeVideoId);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), json.getRecordingTitle(lecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(lecture));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, youTubeVideoId, verificationErrors);

        // Verify warehouse DB
        WarehouseDatabase db = WarehouseDatabaseImpl.getInstance();
        WarehouseCourse warehouseCourse = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourse.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourse.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, warehouseCourse.getRecordingList().get(0).getYouTubeVideoId());
        int originalRecordingId = warehouseCourse.getRecordingList().get(0).getId();

       /*
            Change recording title on archive tab, then republish metadata
        */

        String recordingOneNewTitle = recording.getTitle() + " - edited";
        recording.setTitle(recordingOneNewTitle);
        recordingsPage.loadPage(driver);
        ArchivesPage archivesPage = recordingsPage.clickArchivesTab(driver);
        archivesPage.editAndRepublishMetadata(driver, recording);
        archivesPage.clickHomepageLink(driver);
        homePage.clickAdminLink(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 4);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 4, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 5, "Finished");

        // Verify YouTube distribution, showing new title
        youTube.verifyYouTubeVideo(driver, series, recording, verificationErrors);

        // Verify warehouse JSON - same YouTube video ID but new title
        JSONObject republishMetaLecture = json.waitForCourseRecordingTitle(driver, series, RecordingUtils.getRecordingTitleJson(recording.getTitle()));
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, json.getRecordingYouTubeVideoId(republishMetaLecture));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getJsonRecordingDate(recording.getRecordingDate()), json.getRecordingStartUTC(republishMetaLecture));
        JSONObject courseWithRepublishedMetadata = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRepublishedMetadata));

        // Verify YouTube sharing
        youTube.verifyYouTubeSharing(driver, youTubeVideoId, verificationErrors);

        // Verify warehouse DB
        WarehouseCourse warehouseCourseRepubMeta = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRepubMeta.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyEquals(verificationErrors, recording.getTitle(), warehouseCourseRepubMeta.getRecordingList().get(0).getTitle());
        TestUtils.verifyEquals(verificationErrors, youTubeVideoId, warehouseCourseRepubMeta.getRecordingList().get(0).getYouTubeVideoId());
        int repubMetaRecordingId = warehouseCourseRepubMeta.getRecordingList().get(0).getId();
        TestUtils.verifyEquals(verificationErrors, originalRecordingId, repubMetaRecordingId);

        /*
            Retract recording
         */

        recordingsPage.loadPage(driver);
        recordingsPage.clickArchivesTab(driver);
        archivesPage.searchForRecording(driver, recording, 1);
        archivesPage.retractRecording(driver, 1);
        archivesPage.clickRecordingsTab(driver);
        recordingsPage.searchForSeriesRecordings(driver, series, 5);
        recordingsPage.waitForRecordingOnRow(driver, recording.getTitle(), 5, pageLoadTimeout);
        recordingsPage.waitForRecordingStatusOnRow(driver, series, 5, "Finished");

        // Verify YouTube distribution - no recording
        youTube.loadVideoManagerPage(driver);
        TestUtils.verifyFalse(verificationErrors, youTube.isLinkPresentByText(driver, recording.getTitle()));

        // Verify warehouse JSON - recording is gone
        json.waitForRecordingRetraction(driver, series, youTubeVideoId);
        JSONObject courseWithRetraction = json.waitForCourse(driver, series);
        TestUtils.verifyEquals(verificationErrors, series.getAvailability().isJsonPublicFlag(), json.getCoursePublicFlag(courseWithRetraction));

        // Verify warehouse DB
        WarehouseCourse warehouseCourseRetracted = db.getWarehouseCourse(new CourseKey(Integer.valueOf(SalesforceUtils.getCurrentYear()),
                SalesforceUtils.getCurrentTermCode().charAt(0), Integer.valueOf(series.getCcn())));
        warehouseCourseRetracted.verifyCourseData(verificationErrors, series, instructors);
        TestUtils.verifyTrue(verificationErrors, warehouseCourseRetracted.getRecordingList().isEmpty());
    }
}
