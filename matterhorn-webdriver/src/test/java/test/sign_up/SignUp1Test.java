package test.sign_up;

import domain.*;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import page.*;
import page.matterhorn.HomePage;
import page.matterhorn.RecordingsPage;
import page.matterhorn.SeriesInfoPage;
import page.matterhorn.SignUpPage;
import page.SalesforcePage;
import util.*;

import java.util.Date;

/**
 * @author Paul Farestveit
 */
public class SignUp1Test {

    /**
     * Tests the following workflow:
     * - Single instructor signs up
     * - Inputs selected are video+screen+audio
     * - Publish delay is 5 days
     */

    private WebDriver driver;
    private final String testId = TestUtils.getTestId();
    private final StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        this.driver = WebDriverUtils.getWebDriver();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        TestUtils.writeTestResults(this.getClass(), testId, verificationErrors);
    }

    @Test
    public void testSignUp01() throws Exception {

        HomePage homePage = new HomePage(driver);
        CalNetAuthPage calNetPage = new CalNetAuthPage(driver);
        RecordingsPage recordingsPage = homePage.logIn(driver, calNetPage);
        recordingsPage.deleteUpcomingRecordings(driver);

        Instructor[] instructors = {Instructor.TEST_INSTRUCTOR_1};
        Series series = SeriesUtils.generateUniqueSeries(this.getClass(), testId, instructors, CaptureAgent.VIDEO_AGENT, Inputs.VIDEO_SCREEN, PublishDelay.FIVE_DAYS);
        SalesforceSemesterProject salesforceSemesterProject = SalesforceUtils.generateSalesforceSemesterProject(series);
        Date recordingOneDate = RecordingUtils.getFirstRecordingDate(series);
        Date recordingTwoDate = DateUtils.addDays(recordingOneDate, 7);

        /*
            ADMIN: Create new Salesforce semester project and new course project
         */
        SalesforcePage salesforce = new SalesforcePage(driver);
        salesforce.setUpSalesforceProjects(driver, salesforceSemesterProject, series);

        /*
            INSTRUCTOR ONE: Authenticate via CAS; visit sign-up form; verify default settings; select new settings; submit
         */

        SignUpPage signUpPage = new SignUpPage(driver);
        signUpPage.loadPage(driver, series);
        signUpPage.clickLogOut(driver);
        signUpPage.logIn(driver, calNetPage, series, series.getInstructors()[0].getInstructorUsername(), AuthenticationUtils.getCalnetTestUserPassword());
        TestUtils.verifyEquals(verificationErrors, signUpPage.headingText, signUpPage.pageHeading.getText());
        TestUtils.verifyEquals(verificationErrors, series.getSeriesName(), signUpPage.courseTitle.getText());
        TestUtils.verifyEquals(verificationErrors, series.getCaptureAgent().getRoom() + "  |  " + DateFormatUtils.getDayOfWeek(recordingOneDate) + " 6:30pm-7:00pm",
                signUpPage.locationAndTime.getText());
        TestUtils.verifyEquals(verificationErrors, "Instructor: " + series.getInstructors()[0].getInstructorName() + " (not yet signed up)",
                signUpPage.instructorListText.getText());
        TestUtils.verifyFalse(verificationErrors, signUpPage.audioOnly.isSelected());
        TestUtils.verifyFalse(verificationErrors, signUpPage.screencast.isSelected());
        TestUtils.verifyFalse(verificationErrors, signUpPage.videoOnly.isSelected());
        TestUtils.verifyFalse(verificationErrors, signUpPage.videoAndScreencast.isSelected());
        TestUtils.verifyFalse(verificationErrors, signUpPage.recordingAvailability.isDisplayed());
        TestUtils.verifyFalse(verificationErrors, signUpPage.noPublishDelayRadio.isSelected());
        TestUtils.verifyFalse(verificationErrors, signUpPage.publishDelayRadio.isSelected());
        TestUtils.verifyFalse(verificationErrors, signUpPage.agreementConfirmationCbx.isSelected());
        TestUtils.verifyFalse(verificationErrors, signUpPage.signUpButton.isEnabled());
        signUpPage.chooseInputs(series);
        TestUtils.verifyFalse(verificationErrors, signUpPage.signUpButton.isEnabled());
        signUpPage.setPublishDelay(series);
        TestUtils.verifyFalse(verificationErrors, signUpPage.signUpButton.isEnabled());
        signUpPage.agreeToTerms();
        signUpPage.submitForm();

        /*
            INSTRUCTOR ONE: Verify confirmation page messaging; revisit sign-up page later and verify messaging
         */

        // Confirmation page
        signUpPage.waitForText(signUpPage.instructionalText, signUpPage.confText, TimeoutUtils.getPageLoadWaitSec());
        TestUtils.verifyEquals(verificationErrors, "Instructor: " + series.getInstructors()[0].getInstructorName() + " (signed up)", signUpPage.instructorListText.getText());
        signUpPage.verifyStaticConfirmMsg(verificationErrors, series, recordingOneDate);

        // Revisit page
        signUpPage.loadPage(driver, series);
        signUpPage.waitForText(signUpPage.instructionalText, signUpPage.reconfText, TimeoutUtils.getPageLoadWaitSec());
        TestUtils.verifyEquals(verificationErrors, "Instructor: " + series.getInstructors()[0].getInstructorName() + " (signed up)", signUpPage.instructorListText.getText());
        signUpPage.verifyStaticConfirmMsg(verificationErrors, series, recordingOneDate);
        signUpPage.clickLogOut(driver);

        /*
            ADMIN: Verify Salesforce project metadata after sign-up
         */

        Thread.sleep(TimeoutUtils.getSalesforceWaitMs());
        salesforce.login(driver);
        salesforce.retrieveProject(driver, series);
        TestUtils.verifyEquals(verificationErrors, "In Production", salesforce.stage.getText());
        TestUtils.verifyEquals(verificationErrors, "Approved", salesforce.approvalStatus.getText());
        TestUtils.verifyEquals(verificationErrors, "Checked", salesforce.recordingsScheduledFlag.getAttribute("title"));
        TestUtils.verifyEquals(verificationErrors, "Checked", salesforce.instructorOneApproval.getAttribute("title"));
        TestUtils.verifyEquals(verificationErrors, "Not Checked", salesforce.instructorTwoApproval.getAttribute("title"));
        TestUtils.verifyEquals(verificationErrors, DateFormatUtils.getDayOfWeek(recordingOneDate), salesforce.scheduleDays.getText());
        salesforce.verifyProjectStaticData(verificationErrors, driver, series);

        /*
            ADMIN: Authenticate via CAS; make sure recordings are scheduled in Matterhorn
         */

        homePage.logIn(driver, calNetPage);
        Date[] recordingDates = {recordingOneDate, recordingTwoDate};
        recordingsPage.searchForSeriesRecordings(driver, series, 2);
        recordingsPage.verifyTwoScheduledRecordings(driver, verificationErrors, series, recordingDates);

        // Verify series information for recordings
        SeriesInfoPage seriesInfoPage = new SeriesInfoPage(driver);
        seriesInfoPage.verifySeriesInfo(driver, verificationErrors, series);
    }
}
